<div class="row clearfix">
 <div class="col-lg-3 col-md-6 col-sm-12">
  <div class="widget bg-primary">
   <div class="widget-body">
    <div class="d-flex justify-content-between align-items-center">
     <div class="state">
      <h6>Users</h6>
      <h2><?php echo $total_user ?></h2>
     </div>
     <div class="icon">
      <i class="ik ik-user"></i>
     </div>
    </div>
    <small class="text-small mt-10 d-block">Total Pengguna</small>
   </div>
  </div>
 </div>
 <div class="col-lg-3 col-md-6 col-sm-12">
  <div class="widget bg-warning">
   <div class="widget-body">
    <div class="d-flex justify-content-between align-items-center">
     <div class="state">
      <h6>Kontrak Progress</h6>
      <h2 id="total_kontrak_progress"><?php echo $total_kontrak ?></h2>
     </div>
     <div class="icon">
      <i class="ik ik-file-text"></i>
     </div>
    </div>
    <small class="text-small mt-10 d-block">Total Kontrak</small>
   </div>
  </div>
 </div>
 <div class="col-lg-3 col-md-6 col-sm-12">
  <div class="widget bg-success">
   <div class="widget-body">
    <div class="d-flex justify-content-between align-items-center">
     <div class="state">
      <h6>Kontrak Selesai</h6>
      <h2 id="total_kontrak_selesai"><?php echo $total_kontrak_selesai ?></h2>
     </div>
     <div class="icon">
      <i class="ik ik-calendar"></i>
     </div>
    </div>
    <small class="text-small mt-10 d-block">Total Kontrak Selesai</small>
   </div>
  </div>
 </div>
 <div class="col-lg-3 col-md-6 col-sm-12">
  <div class="widget bg-info">
   <div class="widget-body">
    <div class="d-flex justify-content-between align-items-center">
     <div class="state">
      <h6>Vendor</h6>
      <h2><?php  echo $total_vendor?></h2>
     </div>
     <div class="icon">
      <i class="ik ik-users"></i>
     </div>
    </div>
    <small class="text-small mt-10 d-block">Total Vendor</small>
   </div>
  </div>
 </div>
</div>