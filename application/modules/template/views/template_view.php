<!DOCTYPE html>
<html>
 <?php echo $this->load->view('head_content'); ?>
 <body>    
  <div class="wrapper">   
   <div class="loader">

   </div>
   <?php echo $this->load->view('navbar_content') ?>

   <div class="page-wrap">
    <!-- Left side column. contains the logo and sidebar -->
    <?php echo $this->load->view('sidebar_content') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="main-content">   
     <div class="container-fluid">
      <?php echo $this->load->view($module . '/' . $view_file); ?>
     </div>      
     <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->
    <?php echo $this->load->view('footer_content') ?>
   </div>  

   <!-- Control Sidebar -->
   <?php // echo $this->load->view('right_content') ?>
   <!-- /.control-sidebar -->
   <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->   
  </div>
  <!-- ./wrapper -->

  <?php echo $this->load->view('script_content') ?>

  <?php
  if (isset($header_data)) {
   foreach ($header_data as $v_head) {
    echo $v_head;
   }
  }
  ?>
 </body>
</html>


<style>
 .color-pln{
  background-color: #00a2b9 !important;
 } 
</style>