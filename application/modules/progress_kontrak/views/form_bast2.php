<form class="forms-sample" method="post">  
 <input type="hidden" value="<?php echo isset($kontrak_b2) ? $kontrak_b2 : '' ?>" id="kontrak_b2" class="form-control" />
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Nomor</label>
  <div class="col-sm-9">
   <input type="text" class="form-control required text-right" 
          error="Nomor" id="nomor_bast2" 
          placeholder="Nomor" value="<?php echo isset($nomor_bast2) ? $nomor_bast2 : '0' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal</label>
  <div class="col-sm-9">
   <input  type="text" readonly="" class="form-control text-right" 
           error="Tanggal" id="tanggal_bast2" 
           placeholder="Tanggal" value="<?php echo isset($tanggal_bast2) ? $tanggal_bast2 : '' ?>">
  </div>
 </div>
</form>