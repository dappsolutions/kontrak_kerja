<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Nama Dokumen</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Nama Dokumen" id="nama" 
              placeholder="Nama Dokumen" value="<?php echo isset($nama) ? $nama : '' ?>">
      </div>
     </div>
     <div class="text-right">
      <button type="submit" class="btn btn-primary mr-2" onclick="NamaDokumen.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
      <a href="<?php echo base_url() . $module ?>">Batal</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
