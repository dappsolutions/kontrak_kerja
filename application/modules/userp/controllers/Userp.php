<?php

class Userp extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
//  echo '<pre>';
//  print_r($_SESSION);die;
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'userp';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/userp.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'user';
 }

 public function getRootModule() {
  return "User";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Pegawai";
  $data['title_content'] = 'Pegawai';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataUserp();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataUserp($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('t.password', $keyword),
       array('p.nip', $keyword),
       array('p.nama_pegawai', $keyword),
       array('p.posisi', $keyword),
   );
  }

  $where = "t.deleted = 0 and t.priveledge = '3' and p.upt = '".$this->upt."'";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0 and t.priveledge = '3'";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip', 'p.nama_pegawai', 'p.posisi'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip', 'p.nama_pegawai', 'p.posisi'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataUserp($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('t.password', $keyword),
       array('p.nip', $keyword),
       array('p.nama_pegawai', $keyword),
       array('p.posisi', $keyword),
   );
  }

  $where = "t.deleted = 0 and t.priveledge = '3' and p.upt = '".$this->upt."'";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0 and t.priveledge = '3'";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip', 'p.nama_pegawai', 'p.posisi'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip', 'p.nama_pegawai', 'p.posisi'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataUserp($keyword)
  );
 }

 public function getDetailDataUserp($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListPegawai() {
  $where = "p.deleted = 0 and p.upt = '".$this->upt."' and p.id != '".$this->pegawai_id."'";
  if($this->akses == 'superadmin'){
   $where = "p.deleted = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'where' => $where
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'User ';
  $data['title'] = "Tambah User Pegawai";
  $data['title_content'] = 'Tambah User Pegawai';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataUserp($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah User Pegawai";
  $data['root_module'] = 'User ';
  $data['title_content'] = 'Ubah User Pegawai';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataUserp($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail User Pegawai";
  $data['root_module'] = 'User';
  $data['title_content'] = "Detail User Pegawai";
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function getPostDataUserp($value) {
  $data['pegawai'] = $value->pegawai;
  $data['username'] = $value->username;
  $data['password'] = $value->password;
  $data['priveledge'] = 3;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataUserp($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Userp";
  $data['title_content'] = 'Data Userp';
  $content = $this->getDataUserp($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
