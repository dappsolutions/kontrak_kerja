<head>
 <meta charset="utf-8">
 <meta http-equiv="x-ua-compatible" content="ie=edge">
 <title>Aplikasi - Silakon</title>
 <meta name="description" content="">
 <meta name="keywords" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <link rel="icon" href="<?php echo base_url() ?>assets/images/logo-pln.png" type="image/x-icon" />

 <!--<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">-->

 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/bootstrap/dist/css/bootstrap.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/fontawesome-free/css/all.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/icon-kit/dist/css/iconkit.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/ionicons/dist/css/ionicons.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/jvectormap/jquery-jvectormap.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/weather-icons/css/weather-icons.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/c3/c3.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/owl.carousel/dist/assets/owl.carousel.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/owl.carousel/dist/assets/owl.theme.default.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/dist/css/theme.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/select2/dist/css/select2.min.css">
 <link href="<?php echo base_url() ?>assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" /> 
 <link href="<?php echo base_url() ?>assets/plugins/components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
 <script src="<?php echo base_url() ?>assets/themekit/src/js/vendor/modernizr-2.8.3.min.js"></script>
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/jquery-toast-plugin/dist/jquery.toast.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
</head>