<footer class="footer">
 <div class="w-100 clearfix">
  <span class="text-center text-sm-left d-md-inline-block">Copyright © 2019 Dappsolutions v1.0. All Rights Reserved.</span>
  <span class="float-none float-sm-right mt-1 mt-sm-0 text-center"><i class="fa fa-cube text-danger"></i> by <a href="https://solutionsdapps.com" class="text-dark" target="_blank">Dappsolutions</a></span>
 </div>
</footer>