<div class="table-responsive">
 <table class="table table-bordered" id="tb_plan">
  <thead>
   <tr>
    <th>Termin Ke</th>
    <th>Fisik (%)</th>
    <th>Bayar (%)</th>
    <th>Tanggal</th>
    <th>Jumlah</th>
    <th class="text-center">Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_plan)) { ?>
    <?php foreach ($list_plan as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input type="text" value="<?php echo $value['jumlah_termin'] ?>" placeholder="Termin Ke" id="termin_ke" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['fisik'] ?>" placeholder="Fisik (%)" id="fisik" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['bayar'] ?>" placeholder="Bayar (%)" id="bayar" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['tanggal'] ?>" placeholder="Tanggal" id="tanggal_disbursment_<?php echo $value['id'] ?>" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['jumlah'] ?>" placeholder="Jumlah" id="jumlah_bayar_plan_<?php echo $value['id'] ?>" class="form-control" />
      </td>
      <td class="text-center">
       <i class="ik ik-trash ik-2x" onclick="Kontrak.removeItem(this)"></i>
      </td>


    <script>
     $(function () {
      $('#tanggal_disbursment_<?php echo $value['id'] ?>').datepicker({
       format: 'yyyy-mm-dd',
       autoclose: true,
       todayHighlight: true,
       orientation: 'bottom left'
      });


      $('#jumlah_bayar_plan_<?php echo $value['id'] ?>').divide({
       delimiter: '.',
       divideThousand: true
      });
     });
    </script>
    </tr>
   <?php } ?>
  <?php } ?>
  <tr class="" data_id="">
   <td colspan="6"><a href="" onclick="Kontrak.addItemDisbursment(this, event)" class="btn btn-primary">Tambah Item</a></td>
  </tr>
  </tbody>
 </table>
</div>