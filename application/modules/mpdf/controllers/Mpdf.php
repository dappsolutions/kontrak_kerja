<?php

class Mpdf extends MX_Controller{
 public function getInitPdf() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();
  return $mpdf;
 }
}