var ProgressKontrak = {
 module: function () {
  return 'progress_kontrak';
 },

 add: function () {
  window.location.href = url.base_url(ProgressKontrak.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(ProgressKontrak.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(ProgressKontrak.module()) + "index";
 },

setSessionKeyword: function () {
  var keyWord = $('#keyword').val();
  $.ajax({
   type: 'POST',
   data: {
    keyWord: keyWord
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ProgressKontrak.module()) + "setSessionKeyword",
   error: function () {
    showDangerToast("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    window.location.href = url.base_url(ProgressKontrak.module()) + "search";
   }
  });
 },
 
 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    ProgressKontrak.setSessionKeyword();
//    window.location.href = url.base_url(Kontrak.setSessionKeyword();.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ProgressKontrak.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'form': {
    'no_kontrak': $('#no_kontrak').val(),
    'id_kontrak': $("select#no_kontrak").find('option[value="'+$('#no_kontrak').val()+'"]').attr('id_kontrak'),
    'diprek': $('input#diprek').val(),
    'lokasi': $('input#lokasi').val(),
    'dirlap': $('input#dirlap').val(),
    'korlap': $('input#korlap').val(),
    'kendala': $('input#kendala').val(),
    'keterangan': $('#keterangan').val(),
   },
   'form_amandemen': {
    'kontrak_amandemen_id': $('input#kontrak_amandemen_id').val(),
    'no_amandemen': $('input#no_amandemen').val(),
    'tgl_mulai': $('input#tgl_mulai').val(),
    'tgl_selesai': $('input#tgl_selesai').val(),
    'nilai_kontrak': $('#nilai_kontrak').val(),
    'uraian': $('#uraian').val(),
   },
   'form_progress': {
    'kontrak_ps_id': $('input#kontrak_ps_id').val(),
    'kurva_before': $('input#kurva_before').val(),
    'realisasi_before': $('input#realisasi_before').val(),
    'kurva_after': $('input#kurva_after').val(),
    'realisasi_after': $('input#realisasi_after').val(),
   },
   'form_cod': {
    'kontrak_cod': $('input#kontrak_cod').val(),
    'target': $('input#target').val(),
    'realisasi': $('input#realisasi').val(),
    'ket_cod': $('#ket_cod').val(),
   },
   'form_bayar': {
    'kontrak_bayar': $('input#kontrak_bayar').val(),
    'jumlah_termin': $('input#jumlah_termin').val(),
    'fisik': $('input#fisik').val(),
    'bayar': $('input#bayar').val(),
    'tanggal': $('input#tanggal').val(),
    'jumlah': $('input#jumlah').val(),
    'tanggal_ba': $('input#tanggal_ba').val(),
    'nilai': $('input#nilai').val(),
    'persentase_bayar': $('input#persentase_bayar').val(),
    'denda': $('input#denda').val(),
    'kontrak_plan': $('#kontrak_plan').val(),
   },
   'form_bast1': {
    'kontrak_b1': $('input#kontrak_b1').val(),
    'nomor_bast1': $('input#nomor_bast1').val(),
    'tanggal_bast1': $('input#tanggal_bast1').val(),
   },
   'form_bast2': {
    'kontrak_b2': $('input#kontrak_b2').val(),
    'nomor_bast2': $('input#nomor_bast2').val(),
    'tanggal_bast2': $('input#tanggal_bast2').val(),
   }
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = ProgressKontrak.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ProgressKontrak.module()) + "simpan",
    error: function () {
     showDangerToast("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ProgressKontrak.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      showDangerToast("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(ProgressKontrak.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(ProgressKontrak.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='ProgressKontrak.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ProgressKontrak.module()) + "delete/" + id,

   error: function () {
    showDangerToast("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ProgressKontrak.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     showDangerToast("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="ProgressKontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="ProgressKontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="ProgressKontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  ProgressKontrak.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     showDangerToast('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    showDangerToast('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   showDangerToast('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ProgressKontrak.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='ProgressKontrak.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="ProgressKontrak.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="ProgressKontrak.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tgl_mulai').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tgl_selesai').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tanggal_ba').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tanggal_bast1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tanggal_bast2').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#target').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#realisasi').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': false,
   'lengthChange': true,
   'searching': false,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },

 setSelect2: function () {
  $('select#upt').select2();
  $('select#vendor').select2();
  $('select#jenis').select2();
  $('select#no_kontrak').select2();
  $('select#kontrak_plan').select2();
 },

 clickTabInput: function (elm, e) {
  e.preventDefault();
  var action = $(elm).attr('action');

//  console.log(action);
  $('ul#nav_input').find('.nav-link').removeClass('active');

  $('div.form_input').removeClass('hidden');
  $('div.form_input').addClass('hidden');
  $('div#' + action).removeClass('hidden');
  $(elm).addClass('active');
 },

 getDetailProgress: function (elm) {
  var kontrak = $(elm).val();
  kontrak = $("select#no_kontrak").find('option[value="'+kontrak+'"]').attr('id_kontrak');
  $.ajax({
   type: 'POST',
   data: {
    kontrak: kontrak,
    progress_kontrak: $(elm).val()
   },
   dataType: 'json',
   async: false,
   url: url.base_url(ProgressKontrak.module()) + "getDetailProgress",
   error: function () {
    showDangerToast("Gagal");
   },

   success: function (resp) {
    $('#lokasi').val(resp.data.lokasi);
    $('#diprek').val(resp.data.diprek);
    $('#dirlap').val(resp.data.dirlap);
    $('#korlap').val(resp.data.korlap);
    $('#kendala').val(resp.data.kendala);
    $('#keterangan').val(resp.data.keterangan);
    
    console.log($('div#content_bayar'));
    $('div#content_bayar').html(resp.distburst);
   }
  });
 },

 setThousandSparator: function () {
  $('#nilai').divide({
   delimiter: '.',
   divideThousand: true
  });
  $('#nilai_kontrak').divide({
   delimiter: '.',
   divideThousand: true
  });
  $('#denda').divide({
   delimiter: '.',
   divideThousand: true
  });
 },
};

$(function () {
 ProgressKontrak.setDate();
 ProgressKontrak.setDataTable();
 ProgressKontrak.setSelect2();
 ProgressKontrak.setThousandSparator();
});