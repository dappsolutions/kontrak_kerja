<?php echo $this->load->view('header'); ?>


<div class="card">
 <div class="card-header row">
  <div class="col col-sm-3">
   <div class="card-options d-inline-block">
    <button type="button" class="btn btn-success" onclick="Usera.add()">Tambah</button>
   </div>
  </div>
  <div class="col col-sm-9">
   <div class="card-search with-adv-search dropdown">
    <input type="text" class="form-control" id="keyword" 
           placeholder="Pencarian" required="" onkeyup="Usera.search(this, event)">
    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
   </div>
  </div>

  <?php if (isset($keyword)) { ?>
   <br/>
   <br/>
   <br/>
   <br/>
   <div class="col-sm-12">
    Cari : <label class="bold" style="font-size: 14px;"><b><?php echo $keyword ?></b></label>
   </div>
  <?php } ?> 
 </div>
 <div class="card-body">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_content">
    <thead>
     <tr>
      <th class="text-center">No</th>
      <th>Username</th>
      <th>Password</th>
      <th>Nip</th>
      <th>Nama Pegawai</th>
      <th>Posisi</th>
      <th>Upt</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = $pagination['last_no'] + 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr>
        <td class="text-center"><?php echo $no++ ?></td>
        <td><?php echo $value['username'] ?></td>
        <td><?php echo $value['password'] ?></td>
        <td><?php echo $value['nip'] ?></td>
        <td><?php echo $value['nama_pegawai'] ?></td>
        <td><?php echo $value['posisi'] ?></td>
        <td><?php echo $value['nama_upt'] ?></td>
        <td class="text-center">
         <div class="list-actions">
          <a href="#" onclick="Usera.detail('<?php echo $value['id'] ?>')"><i class="ik ik-eye"></i></a>
          <a href="#" onclick="Usera.ubah('<?php echo $value['id'] ?>')"><i class="ik ik-edit-2"></i></a>
          <a href="#" class="list-delete" onclick="Usera.delete('<?php echo $value['id'] ?>')"><i class="ik ik-trash-2"></i></a>       </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?> 
    </tbody>
   </table>
  </div>

  <div class="text-right">
   <nav aria-label="Page navigation example">
    <ul class="pagination mb-0 pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </nav>
  </div>
 </div>
</div>
