<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Vendor</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Vendor" id="nama" 
              placeholder="Vendor" value="<?php echo isset($nama_vendor) ? $nama_vendor : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Email</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Email" id="email" 
              placeholder="Email" value="<?php echo isset($email) ? $email : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">No Hp</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="No Hp" id="no_hp" 
              placeholder="No Hp" value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Alamat</label>
      <div class="col-sm-9">
       <textarea class="form-control required" error="Alamat" id="alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
      </div>
     </div>
     <div class="text-right">
      <button type="submit" class="btn btn-primary mr-2" onclick="Vendor.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
      <a href="<?php echo base_url().$module ?>">Batal</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
