<div class="table-responsive">
 <table class="table table-bordered" id="tb_kendala">
  <thead>
   <tr>
    <th>Kendala</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_kendala)) { ?>
    <?php foreach ($list_kendala as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['kendala'] ?>" placeholder="Kendala" id="kendala" class="form-control" />
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>