<form class="forms-sample" method="post">  

 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">No Amandemen</label>
  <div class="col-sm-9">
   <input disabled type="text" class="form-control required" 
          error="No Amandemen" id="no_amandemen" 
          placeholder="No Amandemen" value="<?php echo isset($no_amandemen) ? $no_amandemen : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Mulai</label>
  <div class="col-sm-9">
   <input disabled readonly="" type="text" class="form-control required" 
          error="Tanggal Mulai" id="tgl_mulai" 
          placeholder="Tanggal Mulai" value="<?php echo isset($tanggal_mulai) ? $tanggal_mulai : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Selesai</label>
  <div class="col-sm-9">
   <input disabled readonly="" type="text" class="form-control required" 
          error="Tanggal Selesai" id="tgl_selesai" 
          placeholder="Tanggal Selesai" value="<?php echo isset($tanggal_selesai) ? $tanggal_selesai : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Nilai Kontrak</label>
  <div class="col-sm-9">
   <input disabled type="text" min="0" class="form-control required text-right" 
           error="Nilai Kontrak" id="nilai_kontrak" 
           placeholder="Nilai Kontrak" value="<?php echo isset($nilai_kontrak) ? $nilai_kontrak : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Uraian</label>
  <div class="col-sm-9">
   <textarea disabled class="form-control required" 
             error="Uraian" id="uraian" 
             placeholder="Uraian"><?php echo isset($uraian) ? $uraian : '' ?></textarea>
  </div>
 </div>
</form>