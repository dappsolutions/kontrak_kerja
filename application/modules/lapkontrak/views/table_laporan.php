<?php if (!empty($content)) { ?>
 <?php $no = 1; ?>
 <?php foreach ($content as $key => $v_head) { ?>
  <tr>
   <td colspan="33" class="bg-primary-light text-white" style="border:1px solid black;"><?php echo $key ?></td>
  </tr>

  <?php foreach ($v_head as $key_kontrak => $v_kontrak) { ?>
   <tr>
    <td style="vertical-align: top !important;border:1px solid black;" class="text-center sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $no++ ?></td>
    <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>">
     <table>
      <tbody>
       <tr>
        <td style="border:none"><?php echo $v_kontrak[0]['pekerjaan'] ?></td>
       </tr>
       <tr>
        <td style="border:none">&nbsp;</td>
       </tr>
       <tr>
        <td class="" style="font-weight: bold;border:none;">Administrasi</td>
        <td class="" style="border:none;">:</td>
       </tr>
       <tr>
        <td style="border:none;">- RAB </td>
        <td style="border:none;">:</td>
       </tr>
       <tr>
        <td style="border:none;">- RKS </td>
        <td style="border:none;">:</td>
       </tr>
       <tr>
        <td style="border:none;">- PRK </td>
        <td style="border:none;">:</td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Kontrak No</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['no_kontrak'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Tanggal Kontrak</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['tanggal_kontrak'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Tanggal Akhir Kontrak</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['tanggal_selesai_kontrak'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Nilai</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo number_format($v_kontrak[0]['harga_kontrak'], 0, ',', '.') ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Pelaksana</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['pelaksana'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Nama Direktur / Leader KSO</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['leader'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">Alamat</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['alamat'] ?></td>
       </tr>
       <tr>
        <td style="font-weight: bold;border:none;">No Telp</td>
        <td style="border:none;">:</td>
        <td style="border:none;"><?php echo $v_kontrak[0]['no_hp'] ?></td>
       </tr>
      </tbody>
     </table>           
    </td>
    <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['jenis'] ?></td>
    <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['lokasi_pekerjaan'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['no_amandemen'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['uraian'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_mulai'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_selesai'] ?></td>


    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['kurva_before'] . ' %' ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi_before'] . ' %' ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['kurva_after'] . ' %' ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi_after'] . ' %' ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['target'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi'] ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['termin_ke'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['fisik'] . ' %' ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['bayar'] . ' %' ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal'] ?></td>
    <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['jumlah'], 0, ',', '.') ?></td>


    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_ba'] ?></td>
    <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['nilai'], 0, ',', '.') ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['persentase_bayar'] . ' %' ?></td>
    <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['denda'], 0, ',', '.') ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['nomor_kb1'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_kb1'] ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['nomor_kb2'] ?></td>
    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_kb2'] ?></td>

    <td style="border:1px solid black;"><?php echo $v_kontrak[0]['diprek'] ?></td>
    <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['dirlap'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['korlap'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['kendala'] ?></td>
    <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"></td>
   </tr>
   <?php if (count($v_kontrak) > 1) { ?>
    <?php foreach ($v_kontrak as $key_detail => $value) { ?>
     <?php if ($key_detail != 0) { ?>
      <tr>
       <td style="border:1px solid black;"><?php echo $value['no_amandemen'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['uraian'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['tanggal_mulai'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['tanggal_selesai'] ?></td>


       <td style="border:1px solid black;"><?php echo $value['kurva_before'] . ' %' ?></td>
       <td style="border:1px solid black;"><?php echo $value['realisasi_before'] . ' %' ?></td>

       <td style="border:1px solid black;"><?php echo $value['kurva_after'] . ' %' ?></td>
       <td style="border:1px solid black;"><?php echo $value['realisasi_after'] . ' %' ?></td>

       <td style="border:1px solid black;"><?php echo $value['target'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['realisasi'] ?></td>

       <td style="border:1px solid black;"><?php echo $value['termin_ke'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['fisik'] . ' %' ?></td>
       <td style="border:1px solid black;"><?php echo $value['bayar'] . ' %' ?></td>
       <td style="border:1px solid black;"><?php echo $value['tanggal'] ?></td>
       <td style="border:1px solid black;"><?php echo number_format($value['jumlah'], 0, ',', '.') ?></td>


       <td style="border:1px solid black;"><?php echo $value['tanggal_ba'] ?></td>
       <td style="border:1px solid black;"><?php echo number_format($value['nilai'], 0, ',', '.') ?></td>
       <td style="border:1px solid black;"><?php echo $value['persentase_bayar'] . ' %' ?></td>
       <td style="border:1px solid black;"><?php echo number_format($value['denda'], 0, ',', '.') ?></td>

       <td style="border:1px solid black;"><?php echo $value['nomor_kb1'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['tanggal_kb1'] ?></td>

       <td style="border:1px solid black;"><?php echo $value['nomor_kb2'] ?></td>
       <td style="border:1px solid black;"><?php echo $value['tanggal_kb2'] ?></td>

       <td style="border:1px solid black;"><?php echo $value['diprek'] ?></td>
      </tr>
     <?php } ?>           
    <?php } ?>  
   <?php } ?>                
  <?php } ?>
 <?php } ?>       
<?php } else { ?>
 <tr>
  <td colspan="4" class="text-center">Tidak ada data ditemukan</td>
 </tr>
<?php } ?> 