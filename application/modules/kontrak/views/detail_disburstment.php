<div class="table-responsive">
 <table class="table table-bordered" id="tb_plan">
  <thead>
   <tr>
    <th>Termin Ke</th>
    <th>Fisik (%)</th>
    <th>Bayar (%)</th>
    <th>Tanggal</th>
    <th>Jumlah</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_plan)) { ?>
    <?php foreach ($list_plan as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['jumlah_termin'] ?>" placeholder="Termin Ke" id="termin_ke" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['fisik'] ?>" placeholder="Fisik (%)" id="fisik" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['bayar'] ?>" placeholder="Bayar (%)" id="bayar" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['tanggal'] ?>" placeholder="Tanggal" id="tanggal_disbursment_<?php echo $value['id'] ?>" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo number_format($value['jumlah']) ?>" placeholder="Jumlah" id="jumlah_bayar_plan_<?php echo $value['id'] ?>" class="form-control" />
      </td>
    </tr>
   <?php } ?>
  <?php } ?>
  </tbody>
 </table>
</div>