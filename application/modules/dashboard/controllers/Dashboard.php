<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();

  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
//      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/number-divider.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/speedochart/Chart.js"></script>',
      '<script src="' . base_url() . 'assets/js/speedochart/Gauge.js"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/chartist/dist/chartist.min.css"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/chartist/dist/chartist.min.js"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/flot-charts/jquery.flot.js"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/flot-charts/jquery.flot.categories.js"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/flot-charts/curvedLines.js"></script>',
      '<script src="' . base_url() . 'assets/themekit/plugins/flot-charts/jquery.flot.tooltip.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard_v-1.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/probis_v2.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kontrak_v1-3.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/progress_kontrak_v1-4.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['total_kontrak'] = $this->getTotalKontrak();
  $data['total_kontrak_selesai'] = $this->getTotalKontrakSelesai();
  $data['hak_akses'] = $this->session->userdata('hak_akses');
  $data['probis_data'] = Modules::run('probis/getDataProbis')['data'];
  $data['persentase_kontrak'] = $this->getPersentaseAllKontrak();
  $data['all_persentase_kontrak'] = $this->getDataPresentaseSemuaKontrak();

//  echo '<pre>';
//  print_r($data['all_persentase_kontrak']);
//  die;
  echo Modules::run('template', $data);
 }

 public function getTotalUser() {
  $total = Modules::run('database/count_all', array(
              'table' => 'user',
              'where' => "deleted = 0"
  ));
  return $total;
 }

 public function getTotalVendor() {
  $total = Modules::run('database/count_all', array(
              'table' => 'vendor',
              'where' => "deleted = 0"
  ));
  return $total;
 }

 public function getTotalKontrak() {
  $where = "k.deleted = 0 and (ks.status = 'DRAFT' or ks.status is null)";
  if ($this->hak_akses != 'superadmin') {
   $where = "k.deleted = 0 and (ks.status = 'DRAFT' or ks.status is null) and u.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'kontrak k',
              'field' => array('k.*', 'u.nama_upt', 'ks.status'),
              'join' => array(
                  array('upt u', 'k.upt = u.id'),
                  array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id', 'left'),
                  array('kontrak_status ks', 'ks.id = kss.id', 'left'),
              ),
              'where' => $where
  ));

//    echo "<pre>";
//    echo $this->db->last_query();
//    die;
  return $total;
 }

 public function getTotalKontrakSelesai() {
  $where = "k.deleted = 0 and ks.status = 'DONE'";
  if ($this->hak_akses != 'superadmin') {
   $where = "k.deleted = 0 and ks.status = 'DONE' and u.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'kontrak k',
              'field' => array('k.*', 'u.nama_upt', 'ks.status'),
              'join' => array(
                  array('upt u', 'k.upt = u.id'),
                  array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                  array('kontrak_status ks', 'ks.id = kss.id'),
              ),
              'where' => $where
  ));
  return $total;
 }

 public function getRememberDateKontrak() {
  $where = "where k.deleted = 0";
  if ($this->hak_akses != 'superadmin') {
   $where = "where (k.deleted = 0) and ut.id = '" . $this->upt . "'";
  }

  $sql = "select 
	k.no_kontrak
 , k.id
	, v.nama_vendor
	, ut.nama_upt
	, jp.jenis as jenis_pekerjaan	
 , ks.status
	from kontrak k
	join jenis_pekerjaan jp
		on jp.id = k.jenis_pekerjaan
 join upt ut
		on ut.id = k.upt
 left join vendor v
		on k.vendor = v.id
 join (select max(id) id, kontrak from kontrak_status group by kontrak) kss
  on kss.kontrak = k.id
 join kontrak_status ks
  on ks.id = kss.id
	" . $where . " order by ut.nama_upt";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detailProgress() {
  $progress = $_POST['progress'];

  $data = Modules::run('progress_kontrak/getDetailDataProgress', $progress);
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Progress Pekerjaan";
  $data['root_module'] = 'Progress';
  $data['title_content'] = "Detail Progress Pekerjaan";
  $data['list_kontrak'] = Modules::run('progress_kontrak/getListKontrak');

  echo $this->load->view('progress_kontrak/detail_view', $data, true);
 }

 public function getTotalRealisasi($kontrak) {
//   $sql = "select
// 	sum(kpf.realisasi_after) as total_realisasi
// from progress p
// join progress_kontrak pk
// 	on pk.id = p.progress_kontrak
// join kontrak k
// 	on k.id = pk.kontrak
// join kontrak_progress_fisik kpf
// 	on kpf.progress = p.id
// where k.id = '" . $kontrak . "' and p.deleted = 0";

  $sql = "select
	kpf.realisasi_after as total_realisasi
from progress p
join progress_kontrak pk
	on pk.id = p.progress_kontrak
join kontrak k
	on k.id = pk.kontrak
join kontrak_progress_fisik kpf
	on kpf.progress = p.id
where k.id = '" . $kontrak . "' and p.deleted = 0 order by p.id desc";

  $data = Modules::run('database/get_custom', $sql);
  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['total_realisasi'];
  }


  return $total;
 }

 public function showPersetaseKontrak() {
  $kontrak = $_POST['kontrak'];
  $total_realisasi = $this->getTotalRealisasi($kontrak);

  $data['kontrak'] = $kontrak;
  $data['total_realisasi'] = $total_realisasi;
  echo $this->load->view('grafik_persentase_kontrak', $data, true);
 }

 public function getTotalPekerjaan100() {
  $sql = "select
	count(*) as total_realisasi
from progress p
join progress_kontrak pk
	on pk.id = p.progress_kontrak
join kontrak k
	on k.id = pk.kontrak
join kontrak_progress_fisik kpf
	on kpf.progress = p.id
where kpf.realisasi_after >= 100";

  $data = Modules::run('database/get_custom', $sql);

  $total_realisasi = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total_realisasi = $data['total_realisasi'];
  }

  return $total_realisasi;
 }

 public function getTotalKontrakSelesaiall() {
  $where = "";
  if($this->hak_akses != 'superadmin'){
   $where = " and k.upt = '".$this->upt."'";
  }
  $sql = "select 
	count(*) as total_selesai
	from kontrak k
join (select max(id) id, kontrak from kontrak_status group by kontrak) kss
	on kss.kontrak = k.id
join kontrak_status ks
	on ks.id = kss.id
	where ks.status = 'DONE' and k.deleted = 0 ".$where;

  $data = Modules::run('database/get_custom', $sql);

  $total_selesai = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total_selesai = $data['total_selesai'];
  }

  return $total_selesai;
 }

 public function getTotalKontrakAll() {
  $where = "";
  if($this->hak_akses != 'superadmin'){
   $where = " and k.upt = '".$this->upt."'";
  }
  
  $sql = "select count(*) as jumlah_kontrak 
	from kontrak where deleted = 0 ".$where;

  $data = Modules::run('database/get_custom', $sql);

  $jumlah_kontrak = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $jumlah_kontrak = $data['jumlah_kontrak'];
  }

  return $jumlah_kontrak;
 }

 public function getPersentaseAllKontrak() {
  //  $total_realisasi = $this->getTotalPekerjaan100();
  $total_kontrak_selesai = $this->getTotalKontrakSelesaiall();
  $total_kontrak = $this->getTotalKontrakAll();
  $persentase = $total_kontrak_selesai / $total_kontrak;
  $persentase *= 100;
  return $persentase;
 }

 public function viewSurat() {
  echo $this->load->view('reminder_mail');
 }

 public function getDataPresentaseSemuaKontrak() {
  $where = "";
  if ($this->hak_akses != 'superadmin') {
   $where = " and k.upt = '".$this->upt."'";
  }
  
  $sql = "SELECT sum(kk.realisasi) as realisasi, k.no_kontrak 
	FROM `kontrak_kurva` kk
join kontrak k
	on k.id = kk.kontrak
where k.deleted = 0 and kk.deleted = 0 ".$where."
GROUP by k.id";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  $max_data = array();
  $cat_0_until_30 = 0;
  $cat_31_until_50 = 0;
  $cat_51_until_70 = 0;
  $cat_71_until_100 = 0;
  $max = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $realisasi = intval(trim($value['realisasi']));
    if($realisasi >= 0 && $realisasi <= 30){
     $cat_0_until_30 +=1;     
    }
    
    if($realisasi >= 31 && $realisasi <= 50){
     $cat_31_until_50 +=1;
    }
    
    if($realisasi >= 51 && $realisasi <= 70){
     $cat_51_until_70 +=1;
    }
    
    if($realisasi >= 71 && $realisasi <= 100){
     $cat_71_until_100 +=1;
    }
    
    $max_data[] = $realisasi;            
   }
   
   sort($max_data);
   $max = $max_data[count($max_data) -1];
  }


  return array(
      'max'=> $max,
      '0_30'=> $cat_0_until_30,
      '31_50'=> $cat_31_until_50,
      '51_70'=> $cat_51_until_70,
      '71_100'=> $cat_71_until_100,
  );
 }

}
