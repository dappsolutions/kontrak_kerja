<?php

class Library extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'library';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/library.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'library';
 }

 public function getRootModule() {
  return "Master";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Library";
  $data['title_content'] = 'Library';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataLibrary();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function getTotalDataLibrary($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
   );
  }


  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*'),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*'),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataLibrary($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataLibrary($keyword)
  );
 }

 public function getDetailDataLibrary($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListKontrak() {
  $join = array(
      array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
      array('kontrak_status ks', 'kss.id = ks.id'),
  );
  $where = "k.deleted = 0";
  if ($this->akses != 'superadmin') {
   $join = array(
       array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
       array('kontrak_status ks', 'kss.id = ks.id'),
       array('upt u', 'k.upt = u.id'),
   );
   $where = "k.deleted = 0 and u.id = '" . $this->upt . "'";
  }

  $data = Modules::run('database/get', array(
              'table' => 'kontrak k',
              'field' => array('k.*'),
              'join' => $join,
//              'where' => "k.deleted = 0 and ks.status != 'DONE'"
              'where' => $where
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'Library ';
  $data['title'] = "Tambah Library";
  $data['title_content'] = 'Tambah Library';
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataLibrary($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Library";
  $data['root_module'] = 'Library ';
  $data['title_content'] = 'Ubah Library';
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataLibrary($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Library";
  $data['root_module'] = 'Library ';
  $data['title_content'] = "Detail Library";
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function getPostDataLibrary($value) {
  $data['nama'] = $value->nama;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $file = $_FILES;
  $is_save = true;
  $is_valid = false;
//  echo '<pre>';
//  print_r($file);die;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataLibrary($data->form);
   if ($id == '') {
    if (!empty($file)) {
     $response_upload = $this->uploadData('file');
     if ($response_upload['is_valid']) {
      list($kategori, $tipe) = explode('/', $file['file']['type']);
      $post['file'] = $file['file']['name'];
     } else {
      $is_save = false;
      $message = $response_upload['response'];
     }
    }
    if ($is_save) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($file)) {
     if ($data->form->file_str != $file['file']['name']) {
      $response_upload = $this->uploadData('file');
      if ($response_upload['is_valid']) {
       list($kategori, $tipe) = explode('/', $file['file']['type']);
       $post['file'] = $file['file']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }
    if ($is_save) {
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Library";
  $data['title_content'] = 'Data Library';
  $content = $this->getDataLibrary($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $data['akses'] = $this->akses;
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/library/';
  $config['allowed_types'] = 'png|jpg|jpeg|pdf';
  $config['max_size'] = '13000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
