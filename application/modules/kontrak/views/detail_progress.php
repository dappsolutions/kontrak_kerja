<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Lokasi</label>
 <div class="col-sm-9">
  <input disabled type="text" class="form-control required" 
         error="Lokasi" id="lokasi" 
         placeholder="Lokasi" value="<?php echo isset($lokasi) ? $lokasi : '' ?>">
 </div>
</div>

<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Dirpek</label>
 <div class="col-sm-9">
  <input disabled type="text" class="form-control required" 
         error="Diprek" id="diprek" 
         placeholder="Diprek" value="<?php echo isset($diprek) ? $diprek : '' ?>">
 </div>
</div>

<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Dirlap</label>
 <div class="col-sm-9">
  <input disabled type="text" class="form-control required" 
         error="Dirlap" id="dirlap" 
         placeholder="Dirlap" value="<?php echo isset($dirlap) ? $dirlap : '' ?>">
 </div>
</div>

<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Korlap</label>
 <div class="col-sm-9">
  <input disabled type="text" class="form-control required" 
         error="Korlap" id="korlap" 
         placeholder="Korlap" value="<?php echo isset($korlap) ? $korlap : '' ?>">
 </div>
</div>

<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Kendala</label>
 <div class="col-sm-9">
  <input disabled type="text" class="form-control required" 
         error="Kendala" id="kendala" 
         placeholder="Kendala" value="<?php echo isset($kendala) ? $kendala : '' ?>">
 </div>
</div>
<!-- 
<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Keterangan</label>
 <div class="col-sm-9">
  <textarea disabled class="form-control required" 
            error="Keterangan" id="ket_progress" 
            placeholder="Keterangan"><?php echo isset($ket_progress) ? $ket_progress : '' ?></textarea>
 </div>
</div> -->
