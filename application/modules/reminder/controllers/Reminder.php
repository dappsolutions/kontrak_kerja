<?php

class Reminder extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function getModuleName()
	{
		return 'reminder';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/controllers/reminder.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Reminder";
		$data['title_content'] = 'Reminder';
		echo $this->load->view('index', $data, true);
	}

	public function checkReminder()
	{
		$date = date('Y-m-d');
		// $date = date('Y-m-d', strtotime($date. ' + 1 days'));
		// echo $date;die;
		// $date = "2020-02-27";
		// echo $date;die;
		$hi = date('Hi');
		$send_email = "belum jam 5";
		if(intval($hi) == 505){
			$sql = "select k.*
		, ks.status
		, vdr.nama_vendor
	 , vdr.email
		from kontrak k
	join (SELECT max(id) id, kontrak from kontrak_status GROUP by kontrak) kss
		on kss.kontrak = k.id
	join kontrak_status ks
		on ks.id = kss.id		
	join vendor vdr
		on vdr.id = k.vendor
	where k.deleted = 0 and k.reminder_date = '" . $date . "' and ks.status = 'DRAFT'";

			$data = Modules::run('database/get_custom', $sql);

			// echo '<pre>';
			// echo $this->db->last_query();die;
			// echo date('H::i');die;
			if (!empty($data)) {
		// 	echo '<pre>';
		// print_r($data->result_array());die;
				foreach ($data->result_array() as $key => $value) {
					$data_kontrak = Modules::run('kontrak/getDetailDataKontrak', $value['id']);				
			// 		echo '<pre>';
			// print_r($data_kontrak);die;
					$message = $this->load->view('dashboard/reminder_mail', $data_kontrak, true);													
						$send_email = Modules::run(
							'email/send_email',
							trim($data_kontrak['email_vendor']),
							$message
						);												
				}
			}else{
				Modules::run("email/insertSendEmail", "noemail", "noemail", "noemail");
			}
		}				
		echo $send_email;
		// echo '<pre>';
		// echo $this->db->last_query();die;
	}
}
