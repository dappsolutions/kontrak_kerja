<div class="row">
 <div class="col-md-12">
  <h4>Histori Harga Kontrak</h4>
  <hr/>
  <div class="table-responsive">
   <table class="table table-bordered table-striped">
    <thead>
     <tr>
      <th>No</th>
      <th>Amandemen</th>
      <th>Harga Kontrak</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($data as $value) { ?>
       <tr>
        <td><?php echo $no++ ?></td>
        <td><?php echo $value['no_amandemen'] ?></td>
        <td><?php echo number_format($value['nilai_kontrak']) ?></td>
       </tr>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>

  </div>
 </div>
</div>