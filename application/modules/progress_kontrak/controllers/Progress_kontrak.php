<?php

class Progress_kontrak extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'progress_kontrak';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/number-divider.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/progress_kontrak_v1-4.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'progress';
 }

 public function getRootModule() {
  return "Pekerjaan";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Progress";
  $data['title_content'] = 'Progress';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataProgress();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function getTotalDataProgress($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.no_kontrak', $keyword),
       array('u.nama_upt', $keyword),
       array('p.no_progress', $keyword),
   );
  }

  $where = "p.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'superadmin') {
   $where = "p.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
                'join' => array(
                    array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                    array('kontrak k', 'k.id = pk.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
                'join' => array(
                    array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                    array('kontrak k', 'k.id = pk.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataProgress($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.no_kontrak', $keyword),
       array('u.nama_upt', $keyword),
       array('p.no_progress', $keyword),
   );
  }

  $where = "p.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'superadmin') {
   $where = "p.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
                'join' => array(
                    array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                    array('kontrak k', 'k.id = pk.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where,
                'orderby' => 'u.nama_upt, p.id desc'
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
                'join' => array(
                    array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                    array('kontrak k', 'k.id = pk.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where,
                'orderby' => 'u.nama_upt, p.id desc'
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProgress($keyword)
  );
 }

 public function getDetailDataProgress($id) {
  $data = Modules::run('database/get', array(
              'table' => 'progress pg',
              'field' => array('pg.no_progress', 't.*',
                  'ka.tanggal_mulai', 'ka.tanggal_selesai',
                  'ka.no_amandemen', 'ka.uraian', 'kpf.kurva_before',
                  'ka.nilai_kontrak',
                  'kpf.realisasi_before', 'kpf.kurva_after',
                  'kpf.realisasi_after', 'kc.target',
                  'kc.realisasi', 'kb.tanggal_ba', 'kb.nilai',
                  'kb.persentase_bayar', 'kb.denda',
                  'kp.jumlah_termin',
                  'kb1.nomor as nomor_bast1', 'kb1.tanggal as tanggal_bast1',
                  'kb2.nomor as nomor_bast2', 'kb2.tanggal as tanggal_bast2',
                  'ka.id as kontrak_amandemen',
                  'kpf.id as kontrak_ps', 'kc.id as kontrak_cod',
                  'kb.id as kontrak_bayar', 'kb1.id as kontrak_b1',
                  'kb2.id as kontrak_b2',
                  'kc.keterangan as ket_cod', 'kb.kontrak_plan', 'kt.harga_kontrak'),
              'join' => array(
                  array('progress_kontrak t', 'pg.progress_kontrak = t.id'),
                  array('kontrak kt', 'kt.id = t.kontrak'),
                  array('kontrak_amandemen ka', 'ka.progress = pg.id', 'left'),
                  array('kontrak_progress_fisik kpf', 'kpf.progress = pg.id', 'left'),
                  array('kontrak_cod kc', 'kc.progress = pg.id', 'left'),
                  array('kontrak_bayar kb', 'kb.progress = pg.id', 'left'),
                  array('kontrak_plan kp', 'kp.id = kb.kontrak_plan', 'left'),
                  array('kontrak_bast1 kb1', 'kb1.progress = pg.id', 'left'),
                  array('kontrak_bast2 kb2', 'kb2.progress = pg.id', 'left'),
              ),
              'where' => "pg.id = '" . $id . "'"
  ));

  $data = $data->row_array();
//  echo '<pre>';
//  print_r($data);die;
  return $data;
 }

 public function getListKontrak() {
  $join = array(
      array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
      array('kontrak_status ks', 'kss.id = ks.id'),
      array('progress_kontrak pk', 'k.id = pk.kontrak'),
  );
  $where = "k.deleted = 0";
  if ($this->akses != 'superadmin') {
   $join = array(
       array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
       array('kontrak_status ks', 'kss.id = ks.id'),
       array('progress_kontrak pk', 'k.id = pk.kontrak'),
       array('upt u', 'k.upt = u.id'),
   );
   $where = "k.deleted = 0 and u.id = '" . $this->upt . "'";
  }

  $data = Modules::run('database/get', array(
              'table' => 'kontrak k',
              'field' => array('k.*', 'pk.id as pk_id'),
              'join' => $join,
//              'where' => "k.deleted = 0 and ks.status != 'DONE'"
              'where' => $where
  ));
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListVendor() {
  $data = Modules::run('database/get', array(
              'table' => 'vendor v',
              'where' => "v.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenis() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_pekerjaan jp',
              'where' => "jp.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListDisbursment($kontrak = '') {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_plan kp',
              'field' => array('kp.*'),
              'where' => "kp.deleted = 0 and kp.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_plan'] = $value['tanggal'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add($kontrak = "") {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'Progress ';
  $data['title'] = "Tambah Progress Pekerjaan";
  $data['title_content'] = 'Tambah Progress Pekerjaan';
  $data['list_kontrak'] = $this->getListKontrak();
  $data['kontrak_id'] = $kontrak;
  $data['kontrak'] = $kontrak;
  $data['no_amandemen'] = Modules::run('no_generator/generateNoAmandemen');
  $data['akses'] = $this->akses;
  $data['list_disbursment'] = $this->getListDisbursment($kontrak);
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProgress($id);
  $data['id_progress_kontrak'] = $id;
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Progress Pekerjaan";
  $data['root_module'] = 'Progress ';
  $data['title_content'] = 'Ubah Progress Pekerjaan';
  $data['list_kontrak'] = $this->getListKontrak();
  $data['kontrak_id'] = $data['kontrak'];
  $data['akses'] = $this->akses;
  $data['list_disbursment'] = $this->getListDisbursment($data['kontrak']);
//  echo '<pre>';
//  print_r($data['list_disbursment']);die;
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataProgress($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Progress Pekerjaan";
  $data['root_module'] = 'Progress';
  $data['title_content'] = "Detail Progress Pekerjaan";
  $data['list_kontrak'] = $this->getListKontrak();
  $data['list_disbursment'] = $this->getListDisbursment($data['kontrak']);
  echo Modules::run('template', $data);
 }

 public function getPostDataProgress($value) {
  $data['upt'] = $value->upt;
  $data['vendor'] = $value->vendor;
  $data['no_kontrak'] = $value->no_kontrak;
  $data['jenis_pekerjaan'] = $value->jenis;
  $data['tanggal_kontrak'] = $value->tgl_kontrak;
  $data['tanggal_selesai_kontrak'] = $value->tgl_selesai_kontrak;
  $data['keterangan'] = $value->keterangan;
  $data['reminder_date'] = $value->tgl_reminder;
  $data['leader'] = $value->leader;
  return $data;
 }

 public function getPostAmandemen($form_amandemen) {
  $post_amandemen['no_amandemen'] = $form_amandemen->no_amandemen;
  $post_amandemen['uraian'] = $form_amandemen->uraian;
  $post_amandemen['nilai_kontrak'] = $form_amandemen->nilai_kontrak;
  if ($form_amandemen->tgl_mulai != '') {
   $post_amandemen['tanggal_mulai'] = $form_amandemen->tgl_mulai;
  }
  if ($form_amandemen->tgl_selesai != '') {
   $post_amandemen['tanggal_selesai'] = $form_amandemen->tgl_selesai;
  }
  return $post_amandemen;
 }

 public function getPostProgress($form_progress) {
  $pos_progress['kurva_before'] = $form_progress->kurva_before;
  $pos_progress['realisasi_before'] = $form_progress->realisasi_before;
  $pos_progress['kurva_after'] = $form_progress->kurva_after;
  $pos_progress['realisasi_after'] = $form_progress->realisasi_after;
  return $pos_progress;
 }

 public function getPostCod($form_cod) {
  $post_cod = array();
  if ($form_cod->target != '') {
   $post_cod['target'] = $form_cod->target;
  }
  if ($form_cod->realisasi != '') {
   $post_cod['realisasi'] = $form_cod->realisasi;
  }
  if ($form_cod->ket_cod != '') {
   $post_cod['keterangan'] = $form_cod->ket_cod;
  }
  return $post_cod;
 }

 public function getPostBayar($form_bayar) {
//  $post_bayar['jumlah_termin'] = $form_bayar->jumlah_termin;
//  $post_bayar['fisik'] = $form_bayar->fisik;
//  $post_bayar['bayar'] = $form_bayar->bayar;
//  if ($form_bayar->tanggal != '') {
//   $post_bayar['tanggal'] = $form_bayar->tanggal;
//  }
//  $post_bayar['jumlah'] = $form_bayar->jumlah;
  if ($form_bayar->tanggal_ba != '') {
   $post_bayar['tanggal_ba'] = $form_bayar->tanggal_ba;
  }
  $post_bayar['nilai'] = $form_bayar->nilai;
  $post_bayar['persentase_bayar'] = $form_bayar->persentase_bayar;
  $post_bayar['denda'] = $form_bayar->denda;
  if ($form_bayar->kontrak_plan != '') {
   $post_bayar['kontrak_plan'] = $form_bayar->kontrak_plan;
  }
  return $post_bayar;
 }

 public function getPostBast1($form_bast1) {
  $post_bast1['nomor'] = $form_bast1->nomor_bast1;
  if ($form_bast1->tanggal_bast1 != '') {
   $post_bast1['tanggal'] = $form_bast1->tanggal_bast1;
  }
  return $post_bast1;
 }

 public function getPostBast2($form_bast2) {
  $post_bast2['nomor'] = $form_bast2->nomor_bast2;
  if ($form_bast2->tanggal_bast2 != '') {
   $post_bast2['tanggal'] = $form_bast2->tanggal_bast2;
  }
  return $post_bast2;
 }

 public function getPostForm($form) {
  $post_form['kontrak'] = $form->no_kontrak;
  $post_form['lokasi'] = $form->lokasi;
  $post_form['diprek'] = $form->diprek;
  $post_form['dirlap'] = $form->dirlap;
  $post_form['korlap'] = $form->korlap;
  $post_form['kendala'] = $form->kendala;
  $post_form['keterangan'] = $form->keterangan;
  return $post_form;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;


  $progress_kontrak_id = $data->form->no_kontrak;
  $this->db->trans_begin();
  try {
   if ($id == '') {
    $form = $data->form;
    $post_prog['progress_kontrak'] = $progress_kontrak_id;
    $post_prog['no_progress'] = Modules::run('no_generator/generateNoProgress');
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prog);

    $form_amandemen = $data->form_amandemen;
    $post_amandemen = $this->getPostAmandemen($form_amandemen);
    $post_amandemen['progress'] = $id;
    $amandemen = Modules::run('database/_insert', 'kontrak_amandemen', $post_amandemen);

    $form_progress = $data->form_progress;
    $pos_progress = $this->getPostProgress($form_progress);
    $pos_progress['progress'] = $id;
    $progress = Modules::run('database/_insert', 'kontrak_progress_fisik', $pos_progress);

    $form_cod = $data->form_cod;
    $post_cod = $this->getPostCod($form_cod);
    $post_cod['progress'] = $id;
    $cod = Modules::run('database/_insert', 'kontrak_cod', $post_cod);

    $form_bayar = $data->form_bayar;
    $post_bayar = $this->getPostBayar($form_bayar);
    $post_bayar['progress'] = $id;
    $bayar = Modules::run('database/_insert', 'kontrak_bayar', $post_bayar);

    $form_bast1 = $data->form_bast1;
    $post_bast1 = $this->getPostBast1($form_bast1);
    $post_bast1['progress'] = $id;
    $bast1 = Modules::run('database/_insert', 'kontrak_bast1', $post_bast1);

    $form_bast2 = $data->form_bast2;
    $post_bast2 = $this->getPostBast2($form_bast2);
    $post_bast2['progress'] = $id;
    $bast2 = Modules::run('database/_insert', 'kontrak_bast2', $post_bast2);
   } else {
    //update
//    echo $id;die;
    $form_amandemen = $data->form_amandemen;
    $post_amandemen = $this->getPostAmandemen($form_amandemen);
    $post_amandemen['progress'] = $id;
    if ($form_amandemen->kontrak_amandemen_id == '') {
     $amandemen = Modules::run('database/_insert', 'kontrak_amandemen', $post_amandemen);
    } else {
     $amandemen = Modules::run('database/_update', 'kontrak_amandemen',
                     $post_amandemen, array('id' => $form_amandemen->kontrak_amandemen_id));
    }

    $form_progress = $data->form_progress;
    $pos_progress = $this->getPostProgress($form_progress);
    $pos_progress['progress'] = $id;
    if ($form_progress->kontrak_ps_id == '') {
     $progress = Modules::run('database/_insert', 'kontrak_progress_fisik', $pos_progress);
    } else {
     $progress = Modules::run('database/_update', 'kontrak_progress_fisik',
                     $pos_progress, array('id' => $form_progress->kontrak_ps_id));
    }

    $form_cod = $data->form_cod;
    $post_cod = $this->getPostCod($form_cod);
    $post_cod['progress'] = $id;
    if ($form_cod->kontrak_cod == '') {
     $cod = Modules::run('database/_insert', 'kontrak_cod', $post_cod);
    } else {
     $cod = Modules::run('database/_update', 'kontrak_cod',
                     $post_cod, array('id' => $form_cod->kontrak_cod));
    }

    $form_bayar = $data->form_bayar;
    $post_bayar = $this->getPostBayar($form_bayar);
    $post_bayar['progress'] = $id;

    if ($form_bayar->kontrak_bayar == '') {
     $bayar = Modules::run('database/_insert', 'kontrak_bayar', $post_bayar);
    } else {
     $bayar = Modules::run('database/_update', 'kontrak_bayar',
                     $post_bayar, array('id' => $form_bayar->kontrak_bayar));
    }

    $form_bast1 = $data->form_bast1;
    $post_bast1 = $this->getPostBast1($form_bast1);
    $post_bast1['progress'] = $id;

    if ($form_bast1->kontrak_b1 == '') {
     $bast1 = Modules::run('database/_insert', 'kontrak_bast1', $post_bast1);
    } else {
     $bast1 = Modules::run('database/_update', 'kontrak_bast1',
                     $post_bast1, array('id' => $form_bast1->kontrak_b1));
    }

    $form_bast2 = $data->form_bast2;
    $post_bast2 = $this->getPostBast2($form_bast2);
    $post_bast2['progress'] = $id;

    if ($form_bast2->kontrak_b2 == '') {
     $bast2 = Modules::run('database/_insert', 'kontrak_bast2', $post_bast2);
    } else {
     $bast2 = Modules::run('database/_update', 'kontrak_bast2',
                     $post_bast2, array('id' => $form_bast2->kontrak_b2));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function setSessionKeyword() {
  $keyword = $_POST['keyWord'];

  $this->session->set_userdata(array(
      'keyword' => $keyword
  ));

  echo 1;
 }

 public function search() {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = $this->session->userdata('keyword');
//  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Progress";
  $data['title_content'] = 'Data Progress';
  $content = $this->getDataProgress($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function getDetailProgress() {
  $kontrak = $_POST['kontrak'];
  $progress_kontrak = $_POST['progress_kontrak'];
  $data = Modules::run('database/get', array(
              'table' => 'progress_kontrak pk',
              'field' => array('pk.*'),
              'where' => "pk.deleted = 0 and pk.id = '" . $progress_kontrak. "'"
          ))->row_array();
  
  $content['list_disbursment'] = $this->getListDisbursment($kontrak);
  $view_disbutst = $this->load->view('list_disbursment', $content, true);

  echo json_encode(array('data' => $data, 'distburst'=> $view_disbutst));
 }

}
