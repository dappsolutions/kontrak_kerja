<div class="wrapper">
	<!-- <table style="width:100%;">
		<tr>
			<td>Nomor</td>
			<td>:</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Surat Sdr.No.</td>
			<td>:</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sifat</td>
			<td>:</td>
			<td>Penting</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Lampiran</td>
			<td>:</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Peringatan Masa Pelaksanaan Pekerjaan</td>
			<td>:</td>
			<td>&nbsp;</td>
			<td style="padding-top:46px;">
				<p>Kepada : </p>
				<p>
					<?php echo $nama_vendor ?>
					<?php echo $alamat_vendor ?>
					<?php echo $no_vendor ?>
				</p>
			</td>
		</tr>
	</table> -->

	<br>
	<table style="width:100%;">
		<tr>
			<td><b>U.p. Yth. Direktur</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Menunjuk :</td>
		</tr>
		<tr>
			<td>
				<p>1. Surat Perjanjian Nomor <?php echo $no_kontrak ?> <?php echo $pekerjaan ?> tanggal <?php echo $tanggal_kontrak ?></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2. Amandemen <?php echo $no_amandemen ?> tanggal <?php echo $tgl_amandemen ?> perihal Perpanjangan Waktu Pelaksanaan Pekerjaan yang akan berakhir tanggal <?php echo $tgl_selesai_amandemen ?>,</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;Bersama ini kami sampaikan progres hingga saat ini adalah sebesar <?php echo $realisasi_after.' %' ?>, mohon agar saudara :</p>
			</td>
		</tr>
		<tr>
			<td>
				<p style="padding-left:16px;">1. Segera menyampaikan Dokumen yang belum di Approve maupun yang Revisi.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p style="padding-left:16px;">2. Mengambil langkah-langkah strategis untuk mempercepat proses pekerjaan.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p style="padding-left:16px;">3. Berkoordinasi dengan direksi lapangan selama melaksanakan pekerjaan.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p style="padding-left:16px;">4. Mengajukan Working Permit ke APP terkait.</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Demikian kami sampaikan, atas perhatiannya diucapkan terimakasih.</p>
			</td>
		</tr>
	</table>
	<br>
	<br>
	
	<table style="width:100%;">
		<tr>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>SENIOR MANAJER KONSTRUKSI</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>&nbsp;</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>&nbsp;</b></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="text-align: center;"><b>&nbsp;</b></td>
		</tr>
		<tr>
			<td><b>Tembusan : </b></td>
			<td style="text-align: center;"><b>TRANS TJBTB</b></td>
		</tr>
		<!-- <tr>
			<td>
				<p>1. PT PLN (Persero) UPT Malang</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>2. PT PLN (Persero) PUSMANPRO – UPMK II</p>
			</td>
		</tr>
		<tr>
			<td>
				<p>3. MSB DALKONS</p>
			</td>
		</tr> -->
	</table>
</div>
