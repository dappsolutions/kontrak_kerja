<form class="forms-sample" method="post">  
<input type="hidden" value="<?php echo isset($kontrak_ps) ? $kontrak_ps : '' ?>" id="kontrak_ps_id" class="form-control" />
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Kurva (%) Sebelum</label>
  <div class="col-sm-9">
   <input type="number" min="0" class="form-control required text-right" 
          error="Kurva (%) Sebelum" id="kurva_before" 
          placeholder="Kurva (%) Sebelum" value="<?php echo isset($kurva_before) ? $kurva_before : '0' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Realisasi (%) Sebelum</label>
  <div class="col-sm-9">
   <input  type="number" min="0" class="form-control required text-right" 
          error="Realisasi (%) Sebelum" id="realisasi_before" 
          placeholder="Realisasi (%) Sebelum" value="<?php echo isset($realisasi_before) ? $realisasi_before : '0' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Kurva (%) Sekarang</label>
  <div class="col-sm-9">
   <input  type="number" min="0" class="form-control required text-right" 
          error="Kurva (%) Sekarang" id="kurva_after" 
          placeholder="Kurva (%) Sekarang" value="<?php echo isset($kurva_after) ? $kurva_after : '0' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Realisasi (%) Sekarang</label>
  <div class="col-sm-9">
   <input  type="number" min="0" class="form-control required text-right" 
          error="Realisasi (%) Sekarang" id="realisasi_after" 
          placeholder="Realisasi (%) Sekarang" value="<?php echo isset($realisasi_after) ? $realisasi_after : '0' ?>">
  </div>
 </div>
</form>