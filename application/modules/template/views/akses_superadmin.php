<?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'grafik' ?>" aria-expanded="false"><i class="icon-pie-chart fa-fw"></i> <span class="hide-menu"> Grafik </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'path' ?>" aria-expanded="false"><i class="icon-compass fa-fw"></i> <span class="hide-menu"> Pohon Probis </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'recover_password/lupa_password' ?>" aria-expanded="false"><i class="icon-lock fa-fw"></i> <span class="hide-menu"> Lupa Password </span></a>
 </li>
<?php } ?>   

<?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-doc fa-fw"></i> <span class="hide-menu"> Master </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'upt' ?>"><?php echo 'Upt' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'pegawai' ?>"><?php echo 'Pegawai' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'probis' ?>"><?php echo 'Probis' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'info_probis' ?>"><?php echo 'Info Probis' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'rule_probis' ?>"><?php echo 'Peraturan Probis' ?></a> </li>
  </ul>
 </li> 
<?php } ?>   
