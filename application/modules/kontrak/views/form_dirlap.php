<div class="table-responsive">
 <table class="table table-bordered" id="tb_dirlap">
  <thead>
   <tr>
    <th>Dirlap</th>
    <th class="text-center">Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_dirlap)) { ?>
    <?php foreach ($list_dirlap as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input type="text" value="<?php echo $value['dirlap'] ?>" placeholder="Dirlap" id="dirlap" class="form-control" />
      </td>
      <td class="text-center">
       <i class="ik ik-trash ik-2x" onclick="Kontrak.removeItem(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
   <tr class="" data_id="">
    <td colspan="2"><a href="" onclick="Kontrak.addItemDirlap(this, event)" class="btn btn-primary">Tambah Item</a></td>
   </tr>
  </tbody>
 </table>
</div>