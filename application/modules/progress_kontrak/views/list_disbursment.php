<select class="form-control" id="kontrak_plan" error="Disbursment">
 <option value="">Pilih Disbursment</option>
 <?php if (!empty($list_disbursment)) { ?>
  <?php foreach ($list_disbursment as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($kontrak_plan)) { ?>
    <?php $selected = $kontrak_plan == $value['id'] ? 'selected' : '' ?>
   <?php } ?>
   <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jumlah_termin'] ?></option>
  <?php } ?>
 <?php } ?>
</select>