<form class="forms-sample" method="post">  
 <input type="hidden" value="<?php echo isset($kontrak_bayar) ? $kontrak_bayar : '' ?>" id="kontrak_bayar" class="form-control" />
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Disbursment Termin Ke</label>
  <div class="col-sm-9 text-right" id="content_bayar">
   <select class="form-control" id="kontrak_plan" error="Disbursment">
    <option value="">Pilih Disbursment</option>
    <?php if (!empty($list_disbursment)) { ?>
     <?php foreach ($list_disbursment as $value) { ?>
      <?php $selected = '' ?>
      <?php if (isset($kontrak_plan)) { ?>
       <?php $selected = $kontrak_plan == $value['id'] ? 'selected' : '' ?>
      <?php } ?>
      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jumlah_termin'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>
 </div>
 <!-- <div class="form-group row">
   <label for="" class="col-sm-3 col-form-label">Fisik (%)</label>
   <div class="col-sm-9">
    <input  type="number" min="0" class="form-control required text-right" 
           error="Fisik (%)" id="fisik" 
           placeholder="Fisik (%)" value="<?php echo isset($fisik) ? $fisik : '0' ?>">
   </div>
  </div>
  <div class="form-group row">
   <label for="" class="col-sm-3 col-form-label">Bayar (%)</label>
   <div class="col-sm-9">
    <input  type="number" min="0" class="form-control required text-right" 
           error="Bayar (%)" id="bayar" 
           placeholder="Bayar (%)" value="<?php echo isset($bayar) ? $bayar : '0' ?>">
   </div>
  </div>
  <div class="form-group row">
   <label for="" class="col-sm-3 col-form-label">Tanggal</label>
   <div class="col-sm-9">
    <input  type="text" readonly class="form-control" 
           error="Tanggal" id="tanggal" 
           placeholder="Tanggal" value="<?php echo isset($tanggal) ? $tanggal : '' ?>">
   </div>
  </div>
  <div class="form-group row">
   <label for="" class="col-sm-3 col-form-label">Jumlah</label>
   <div class="col-sm-9">
    <input  type="number" min="0" class="form-control required text-right" 
           error="Jumlah" id="jumlah" 
           placeholder="Jumlah" value="<?php echo isset($jumlah) ? $jumlah : '' ?>">
   </div>
  </div>-->
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal B.A</label>
  <div class="col-sm-9">
   <input  type="text" readonly class="form-control" 
           error="Tanggal B.A" id="tanggal_ba" 
           placeholder="Tanggal B.A" value="<?php echo isset($tanggal_ba) ? $tanggal_ba : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Nilai</label>
  <div class="col-sm-9">
   <input  type="text" min="0" class="form-control required text-right" 
           error="Nilai" id="nilai" 
           placeholder="Nilai" value="<?php echo isset($nilai) ? $nilai : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Persentase Bayar (%)</label>
  <div class="col-sm-9">
   <input  type="number" min="0" class="form-control required text-right" 
           error="Presentase Bayar (%)" id="persentase_bayar" 
           placeholder="Presentase Bayar (%)" value="<?php echo isset($persentase_bayar) ? $persentase_bayar : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Denda</label>
  <div class="col-sm-9">
   <input  type="text" min="0" class="form-control required text-right" 
           error="Denda" id="denda" 
           placeholder="Denda" value="<?php echo isset($denda) ? $denda : '' ?>">
  </div>
 </div>
</form>
