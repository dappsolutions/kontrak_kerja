<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <h4>Kontrak</h4>
    <hr/>
    <br/>
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">UPT</label>
      <div class="col-sm-9">
       <select <?php echo $upt_id == '' ? '' : 'disabled' ?> class="form-control required" error="UPT" id="upt">
        <option value="">Pilih UPT</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Vendor</label>
      <div class="col-sm-9">
       <select class="form-control required" error="Vendor" id="vendor">
        <option value="">Pilih Vendor</option>
        <?php if (!empty($list_vendor)) { ?>
         <?php foreach ($list_vendor as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($vendor)) { ?>
           <?php $selected = $vendor == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_vendor'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Jenis Pekerjaan</label>
      <div class="col-sm-9">
       <select class="form-control required" error="Jenis Pekerjaan" id="jenis">
        <option value="">Pilih Jenis Pekerjaan</option>
        <?php if (!empty($list_jenis)) { ?>
         <?php foreach ($list_jenis as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($jenis_pekerjaan)) { ?>
           <?php $selected = $jenis_pekerjaan == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Judul Pekerjaan</label>
      <div class="col-sm-9">
       <textarea placeholder="Pekerjaan" class="form-control required" id="pekerjaan" error="Pekerjaan"><?php echo isset($pekerjaan) ? $pekerjaan : '' ?></textarea>       
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">No Kontrak</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="No Kontrak" id="no_kontrak" 
              placeholder="No Kontrak" value="<?php echo isset($no_kontrak) ? $no_kontrak : '' ?>">
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Harga Kontrak</label>
      <div class="col-sm-9">
       <?php $disabled = "" ?>
       <?php if (isset($harga_kontrak)) { ?>
        <?php if (!empty($last_amandemen)) { ?>         
         <?php if ($last_amandemen['nilai_kontrak'] != 0) { ?>
          <?php $disabled = "disabled" ?>
          <?php $harga_kontrak = $last_amandemen['nilai_kontrak'] ?>
         <?php } ?>         
        <?php } ?>
        <input type="text" class="form-control required" 
               error="Harga Kontrak" id="harga_kontrak" 
               placeholder="Harga Kontrak" <?php echo $disabled ?> value="<?php echo isset($harga_kontrak) ? $harga_kontrak : '' ?>">             
        <i class="ik ik-archive" data-toggle="tooltip" title="History Harga Kontrak" onclick="Kontrak.showHistoryHarga(this)"></i>
       <?php } else { ?>
        <input type="text" class="form-control required" 
               error="Harga Kontrak" id="harga_kontrak" 
               placeholder="Harga Kontrak" value="<?php echo isset($harga_kontrak) ? $harga_kontrak : '' ?>">             
              <?php } ?>       
      </div>
     </div>     
     <!--     <div class="form-group row">
           <label for="" class="col-sm-3 col-form-label">Keterangan</label>
           <div class="col-sm-9">
            <textarea class="form-control required" 
                      error="Keterangan" id="keterangan" 
                      placeholder="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
           </div>
          </div>    -->

    </form>
   </div>

   <div class="col-md-6">
    <h4>&nbsp;</h4>
    <hr/>
    <br/>
    <?php echo $this->load->view('form_progress'); ?>
   </div>   
  </div>
  <hr/>


  <div class="row">
   <div class="col-md-12">
    <h4>Form Disbursment</h4>
    <?php echo $this->load->view('form_disbursment') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Form Kurva S</h4>
    <?php echo $this->load->view('form_kurva') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Form Kendala</h4>
    <?php echo $this->load->view('form_kendala') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Form Korlap</h4>
    <?php echo $this->load->view('form_korlap') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Form Dirlap</h4>
    <?php echo $this->load->view('form_dirlap') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Form Lokasi</h4>
    <?php echo $this->load->view('form_lokasi') ?>
   </div>
  </div>
  <hr/>
 </div>
 <div class="card-footer">
  <div class="text-right">
   <button type="submit" class="btn btn-primary mr-2" onclick="Kontrak.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
   <a href="<?php echo base_url() . $module ?>">Batal</a>
  </div>
 </div>
</div>
