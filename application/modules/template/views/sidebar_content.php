<div class="app-sidebar colored">
	<div class="sidebar-header color-pln">
		<a class="header-brand" href="<?php echo base_url() . 'dashboard' ?>">
			<div class="logo-img">
				<img width="25" height="30" src="<?php echo base_url() ?>assets/images/logo-pln.png" class="header-brand-img" alt="lavalite">
			</div>
			<span class="text">SILAKON</span>
		</a>
		<button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
		<button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
	</div>

	<div class="sidebar-content">
		<div class="nav-container">
			<nav id="main-menu-navigation" class="navigation-main">
				<div class="nav-lavel">Navigation</div>
				<div class="nav-item active">
					<a href="<?php echo base_url() . 'dashboard' ?>"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
				</div>
				<!--    <div class="nav-item">
         <a href="<?php echo base_url() ?>assets/themekit/pages/navbar.html"><i class="ik ik-menu"></i><span>Navigation</span> <span class="badge badge-success">New</span></a>
        </div>-->
				<div class="nav-item has-sub">
					<a href="javascript:void(0)"><i class="ik ik-folder"></i><span>Pekerjaan</span> </a>
					<div class="submenu-content">
						<a href="<?php echo base_url() . 'kontrak' ?>" class="menu-item">Kontrak</a>
						<a href="<?php echo base_url() . 'progress_kontrak' ?>" class="menu-item">Progress Pekerjaan</a>      
      <?php if ($akses == 'superadmin') { ?>
       <a href="<?php echo base_url() . 'jenis' ?>" class="menu-item">Jenis Pekerjaan</a>       
      <?php } ?>						
						<a href="<?php echo base_url() . 'dokumen' ?>" class="menu-item">Dokumen</a>
						<a href="<?php echo base_url() . 'surat_reminder' ?>" class="menu-item">Surat Reminder</a>
					</div>
				</div>
    <?php if ($akses == "superadmin") { ?>
     <div class="nav-item has-sub">
      <a href="javascript:void(0)"><i class="ik ik-folder"></i><span>Master</span> </a>
      <div class="submenu-content">
       <a href="<?php echo base_url() . 'upt' ?>" class="menu-item">Upt</a>
       <a href="<?php echo base_url() . 'vendor' ?>" class="menu-item">Vendor</a>
       <a href="<?php echo base_url() . 'pegawai' ?>" class="menu-item">Pegawai</a>
       <a href="<?php echo base_url() . 'probis' ?>" class="menu-item">Probis</a>
       <a href="<?php echo base_url() . 'nama_dokumen' ?>" class="menu-item">Nama Dokumen</a>
       <a href="<?php echo base_url() . 'library' ?>" class="menu-item">Library</a>
      </div>
     </div>
     <div class="nav-item has-sub">
      <a href="javascript:void(0)"><i class="ik ik-folder"></i><span>User</span> </a>
      <div class="submenu-content">
       <a href="<?php echo base_url() . 'usera' ?>" class="menu-item">Admin</a>
       <a href="<?php echo base_url() . 'userp' ?>" class="menu-item">Pegawai</a>
       <a href="<?php echo base_url() . 'userv' ?>" class="menu-item">Vendor</a>
      </div>
     </div>
    <?php } ?>

    <?php if ($akses == 'admin') { ?>
     <div class="nav-item has-sub">
      <a href="javascript:void(0)"><i class="ik ik-folder"></i><span>Master</span> </a>
      <div class="submenu-content">
       <a href="<?php echo base_url() . 'library' ?>" class="menu-item">Library</a>
      </div>
     </div>
     <div class="nav-item has-sub">
      <a href="javascript:void(0)"><i class="ik ik-folder"></i><span>User</span> </a>
      <div class="submenu-content">
       <a href="<?php echo base_url() . 'userp' ?>" class="menu-item">Pegawai</a>
       <a href="<?php echo base_url() . 'userv' ?>" class="menu-item">Vendor</a>
      </div>
     </div>
    <?php } ?>

    <?php if ($akses == 'superadmin' || $akses == 'admin') { ?>
     <div class="nav-item has-sub">
      <a href="javascript:void(0)"><i class="ik ik-folder"></i><span>Laporan</span> </a>
      <div class="submenu-content">
       <a href="<?php echo base_url() . 'lapkontrak' ?>" class="menu-item">Kontrak</a>
      </div>
     </div>
    <?php } ?>
				<div class="nav-item">
					<a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="ik ik-power"></i><span>Sign Out</span></a>
				</div>
			</nav>
		</div>
	</div>
</div>
