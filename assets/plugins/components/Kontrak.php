<?php

class Kontrak extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'kontrak';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/number-divider.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kontrak_v1-2.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function getTableName() {
  return 'kontrak';
 }

 public function getRootModule() {
  return "Pekerjaan";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Kontrak";
  $data['title_content'] = 'Kontrak';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataKontrak();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function getTotalDataKontrak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.no_kontrak', $keyword),
       array('u.nama_upt', $keyword),
       array('ks.status', $keyword),
   );
  }

  $where = "k.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'Superadmin') {
   $where = "k.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' k',
                'field' => array('k.*', 'u.nama_upt', 'ks.status'),
                'join' => array(
                    array('upt u', 'k.upt = u.id'),
                    array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                    array('kontrak_status ks', 'ks.id = kss.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' k',
                'field' => array('k.*', 'u.nama_upt', 'ks.status'),
                'join' => array(
                    array('upt u', 'k.upt = u.id'),
                    array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                    array('kontrak_status ks', 'ks.id = kss.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataKontrak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.no_kontrak', $keyword),
       array('u.nama_upt', $keyword),
       array('ks.status', $keyword),
   );
  }

  $where = "k.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'superadmin') {
   $where = "k.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' k',
                'field' => array('k.*', 'u.nama_upt', 'ks.status'),
                'join' => array(
                    array('upt u', 'k.upt = u.id'),
                    array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                    array('kontrak_status ks', 'ks.id = kss.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' k',
                'field' => array('k.*', 'u.nama_upt', 'ks.status'),
                'join' => array(
                    array('upt u', 'k.upt = u.id'),
                    array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                    array('kontrak_status ks', 'ks.id = kss.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKontrak($keyword)
  );
 }

 public function getDetailDataKontrak($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'ks.status', 'pk.lokasi', 'pk.diprek',
                  'pk.dirlap', 'pk.korlap',
                  'pk.kendala', 'pk.keterangan as ket_progress',
                  'v.email as email_vendor',
                  'v.nama_vendor',
                  'v.alamat as alamat_vendor', 'v.no_hp as no_vendor',
                  'ka.no_amandemen', 'ka.tanggal_mulai as tgl_amandemen',
                  'ka.tanggal_selesai as tgl_selesai_amandemen',
                  'kpf.realisasi_after'),
              'join' => array(
                  array('upt u', 'k.upt = u.id'),
                  array('vendor v', 'k.vendor = v.id'),
                  array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                  array('kontrak_status ks', 'ks.id = kss.id'),
                  array('progress_kontrak pk', 'pk.kontrak = k.id', 'left'),
                  array('(select max(id) id, progress_kontrak from progress group by progress_kontrak) pkss', 'pkss.progress_kontrak = pk.id', 'left'),
                  array('progress pgs', 'pgs.id = pkss.id', 'left'),
                  array('kontrak_amandemen ka', 'ka.progress = pgs.id', 'left'),
                  array('kontrak_progress_fisik kpf', 'kpf.progress = pgs.id', 'left'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

//   echo '<pre>';
//   echo $this->db->last_query();die;
  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt u',
              'where' => "u.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListVendor() {
  $data = Modules::run('database/get', array(
              'table' => 'vendor v',
              'where' => "v.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenis() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_pekerjaan jp',
              'where' => "jp.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'Kontrak ';
  $data['title'] = "Tambah Kontrak Pekerjaan";
  $data['title_content'] = 'Tambah Kontrak Pekerjaan';
  $data['list_upt'] = $this->getListUpt();
  $data['list_vendor'] = $this->getListVendor();
  $data['list_jenis'] = $this->getListJenis();
  $data['upt_id'] = $this->upt;
  $data['upt'] = $this->upt;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKontrak($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kontrak Pekerjaan";
  $data['root_module'] = 'Kontrak ';
  $data['title_content'] = 'Ubah Kontrak Pekerjaan';
  $data['list_upt'] = $this->getListUpt();
  $data['list_vendor'] = $this->getListVendor();
  $data['list_jenis'] = $this->getListJenis();
  $data['upt_id'] = $this->upt;

  $data['list_plan'] = $this->getListPlan($id);
  $data['list_kendala'] = $this->getListKendala($id);
  $data['list_korlap'] = $this->getListKorlap($id);
  $data['list_dirlap'] = $this->getListDirlap($id);
  $data['list_lokasi'] = $this->getListLokasi($id);
  $data['list_kurva'] = $this->getListKurva($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getListPlan($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_plan kp',
              'field' => array('kp.*'),
              'where' => "kp.deleted = 0 and kp.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKendala($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_kendala kk',
              'field' => array('kk.*'),
              'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKorlap($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_korlap kk',
              'field' => array('kk.*'),
              'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListDirlap($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_dirlap kk',
              'field' => array('kk.*'),
              'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListLokasi($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_lokasi kk',
              'field' => array('kk.*'),
              'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataKontrak($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kontrak Pekerjaan";
  $data['root_module'] = 'Kontrak';
  $data['title_content'] = "Detail Kontrak Pekerjaan";
  $data['list_upt'] = $this->getListUpt();
  $data['list_vendor'] = $this->getListVendor();
  $data['list_jenis'] = $this->getListJenis();

  $data['list_plan'] = $this->getListPlan($id);
  $data['list_kendala'] = $this->getListKendala($id);
  $data['list_korlap'] = $this->getListKorlap($id);
  $data['list_dirlap'] = $this->getListDirlap($id);
  $data['list_lokasi'] = $this->getListLokasi($id);
  $data['list_kurva'] = $this->getListKurva($id);

  $data['last_amandemen'] = $this->getLastAmandemen($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getLastAmandemen($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'progress_kontrak pk',
              'field' => array('pk.*', 'ka.nilai_kontrak', 'ka.no_amandemen'),
              'join' => array(
                  array('progress pg', 'pg.progress_kontrak = pk.id'),
                  array('kontrak_amandemen ka', 'ka.progress = pg.id'),
              ),
              'where' => "pk.deleted = 0 and pk.kontrak = '" . $kontrak . "'",
              'orderby' => 'ka.id desc'
  ));
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   $result['nilai_kontrak'] = $result['nilai_kontrak'] == '' ? 0 : $result['nilai_kontrak'];
  }


  return $result;
 }
 
 public function getListAmandemen($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'progress_kontrak pk',
              'field' => array('pk.*', 'ka.nilai_kontrak', 'ka.no_amandemen'),
              'join' => array(
                  array('progress pg', 'pg.progress_kontrak = pk.id'),
                  array('kontrak_amandemen ka', 'ka.progress = pg.id'),
              ),
              'where' => "pk.deleted = 0 and pk.kontrak = '" . $kontrak . "'",
              'orderby' => 'ka.id asc'
  ));
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKurva($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_kurva kk',
              'field' => array('kk.*'),
              'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataKontrak($value) {
  $data['upt'] = $value->upt;
  $data['vendor'] = $value->vendor;
  $data['no_kontrak'] = $value->no_kontrak;
  $data['jenis_pekerjaan'] = $value->jenis;
  $data['tanggal_kontrak'] = $value->tgl_kontrak;
  $data['tanggal_selesai_kontrak'] = $value->tgl_selesai_kontrak;
//  $data['keterangan'] = $value->keterangan;
  $data['reminder_date'] = $value->tgl_reminder;
  $data['leader'] = $value->leader;
  $data['pekerjaan'] = $value->pekerjaan;
  $data['harga_kontrak'] = $value->harga_kontrak;
  return $data;
 }

 public function getPostFormLokasi($form) {
//  $post_form['lokasi'] = $form->lokasi;
  $post_form['diprek'] = $form->diprek;
//  $post_form['dirlap'] = $form->dirlap;
//  $post_form['korlap'] = $form->korlap;
//  $post_form['kendala'] = $form->kendala;
//  $post_form['keterangan'] = $form->ket_progress;
  return $post_form;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

//  echo '<pre>';
//  print_r($data);
//  die;
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataKontrak($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);

    //status
    $pos_status['kontrak'] = $id;
    $pos_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'kontrak_status', $pos_status);

    //progress kontrak
    $form_diprek = $data->form_diprek;
    $post_progress = $this->getPostFormLokasi($form_diprek);
    $post_progress['kontrak'] = $id;
    Modules::run('database/_insert', 'progress_kontrak', $post_progress);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

    //progress kontrak
    $form_diprek = $data->form_diprek;
    $post_progress = $this->getPostFormLokasi($form_diprek);
    $post_progress['kontrak'] = $id;
    Modules::run('database/_update', 'progress_kontrak', $post_progress, array('kontrak' => $id));
   }


   //form plan
   $form_plan = $data->form_plan->item;
   if (!empty($form_plan)) {
    foreach ($form_plan as $value) {
     $post_item_plan['kontrak'] = $id;
     $post_item_plan['jumlah_termin'] = $value->termin;
     $post_item_plan['fisik'] = $value->fisik;
     $post_item_plan['bayar'] = $value->bayar;
     $post_item_plan['tanggal'] = $value->tanggal;
     $post_item_plan['jumlah'] = $value->jumlah;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_plan', $post_item_plan);
     } else {
      if ($value->deleted == '1') {
       $post_item_plan['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_plan', $post_item_plan, array('id' => $value->id));
     }
    }
   }

   //form kurva
   $form_kurva = $data->form_kurva->item;
   if (!empty($form_kurva)) {
    foreach ($form_kurva as $value) {
     $post_item_kurva['kontrak'] = $id;
     $post_item_kurva['bulan_ke'] = $value->bulan_ke;
     $post_item_kurva['master'] = $value->master;
     $post_item_kurva['rencana'] = $value->rencana;
     $post_item_kurva['realisasi'] = $value->realisasi;
     $post_item_kurva['is_amandemen'] = $value->is_amandemen;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_kurva', $post_item_kurva);
     } else {
      if ($value->deleted == '1') {
       $post_item_plan['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_kurva', $post_item_kurva, array('id' => $value->id));
     }
    }
   }

   //form kendala
   $form_kendala = $data->form_kendala->item;
   if (!empty($form_kendala)) {
    foreach ($form_kendala as $value) {
     $post_kendala['kontrak'] = $id;
     $post_kendala['kendala'] = $value->kendala;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_kendala', $post_kendala);
     } else {
      if ($value->deleted == '1') {
       $post_kendala['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_kendala', $post_kendala, array('id' => $value->id));
     }
    }
   }

   //form korlap
   $form_korlap = $data->form_korlap->item;
   if (!empty($form_korlap)) {
    foreach ($form_korlap as $value) {
     $post_korlap['kontrak'] = $id;
     $post_korlap['korlap'] = $value->korlap;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_korlap', $post_korlap);
     } else {
      if ($value->deleted == '1') {
       $post_kendala['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_korlap', $post_korlap, array('id' => $value->id));
     }
    }
   }

   //form dirlap
   $form_dirlap = $data->form_dirlap->item;
   if (!empty($form_dirlap)) {
    foreach ($form_dirlap as $value) {
     $post_dirlap['kontrak'] = $id;
     $post_dirlap['dirlap'] = $value->dirlap;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_dirlap', $post_dirlap);
     } else {
      if ($value->deleted == '1') {
       $post_kendala['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_dirlap', $post_dirlap, array('id' => $value->id));
     }
    }
   }

   //form lokasi
   $form_lokasi = $data->form_lokasi->item;
   if (!empty($form_lokasi)) {
    foreach ($form_lokasi as $value) {
     $post_lokasi['kontrak'] = $id;
     $post_lokasi['lokasi'] = $value->lokasi;
     if ($value->id == '') {
      //insert
      Modules::run('database/_insert', 'kontrak_lokasi', $post_lokasi);
     } else {
      if ($value->deleted == '1') {
       $post_kendala['deleted'] = 1;
      }

      Modules::run('database/_update', 'kontrak_lokasi', $post_lokasi, array('id' => $value->id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function selesai($id) {
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $pos_status['kontrak'] = $id;
   $pos_status['status'] = 'DONE';
   Modules::run('database/_insert', 'kontrak_status', $pos_status);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function setSessionKeyword() {
  $keyword = $_POST['keyWord'];

  $this->session->set_userdata(array(
      'keyword' => $keyword
  ));

  echo 1;
 }

 public function search() {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = $this->session->userdata('keyword');
  // $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kontrak";
  $data['title_content'] = 'Data Kontrak';
  $content = $this->getDataKontrak($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function showDokumenData() {
  $file = str_replace(' ', '_', $this->input->post('file'));
  $data['file'] = $file;
  echo $this->load->view('data_dokumen', $data, true);
 }

 public function showDokumen() {
  $kontrak = $_POST['kontrak'];
  $data = Modules::run('database/get', array(
              'table' => 'dokumen d',
              'where' => "d.deleted = 0 and d.kontrak = '" . $kontrak . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  $content['content'] = $result;
  echo $this->load->view('list_dokumen', $content, true);
 }

 public function showProgressKontrak() {
  $kontrak = $_POST['kontrak'];
//  echo '<pre>';
//  print_r($_POST);die;
  $data = Modules::run('database/get', array(
              'table' => 'progress p',
              'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
              'join' => array(
                  array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                  array('kontrak k', 'k.id = pk.kontrak'),
                  array('upt u', 'k.upt = u.id'),
              ),
              'where' => "p.deleted = 0 and pk.kontrak = '" . $kontrak . "'",
              'orderby' => 'p.id asc'
  ));

//   echo '<pre>';
//   echo $this->db->last_query();die;

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
//  echo '<pre>';
//  print_r($result);die;
  $content['content'] = $result;
  $content['akses'] = $this->akses;
  $content['kontrak'] = $kontrak;
	$content['data_kontrak'] = $this->getDetailDataKontrak($kontrak);
	$content['data_kurva'] = $this->getDataKurvaSKontrak($kontrak);
//  echo '<pre>';
//  print_r($content);die;
  echo $this->load->view('progress_kontrak', $content, true);
 }

 public function showProgressKontrakView($kontrak) {
//  echo '<pre>';
//  print_r($_POST);die;
  $content = Modules::run('database/get', array(
              'table' => 'progress p',
              'field' => array('p.*', 'u.nama_upt', 'k.no_kontrak'),
              'join' => array(
                  array('progress_kontrak pk', 'p.progress_kontrak = pk.id'),
                  array('kontrak k', 'k.id = pk.kontrak'),
                  array('upt u', 'k.upt = u.id'),
              ),
              'where' => "p.deleted = 0 and pk.kontrak = '" . $kontrak . "'",
              'orderby' => 'p.id asc'
  ));

//   echo '<pre>';
//   echo $this->db->last_query();die;

  $result = array();
  if (!empty($content)) {
   foreach ($content->result_array() as $value) {
    array_push($result, $value);
   }
  }
//  echo '<pre>';
//  print_r($result);die;
  $data['content'] = $result;
  $data['akses'] = $this->akses;
  $data['kontrak'] = $kontrak;
  $data['data_kontrak'] = $this->getDetailDataKontrak($kontrak);
//  echo '<pre>';
//  print_r($content);die;

  $data['view_file'] = 'progress_kontrak';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Kontrak";
  $data['title_content'] = 'Kontrak';
	$data['root_module'] = $this->getRootModule();	
  echo Modules::run('template', $data);
 }

 public function getDataKurvaSKontrak($kontrak){
		$data = Modules::run('database/get', array(
			'table' => 'kontrak_kurva kk',
			'field' => array('kk.bulan_ke', 'kk.master', 'kk.rencana', 'kk.realisasi'),
			'join' => array(
				array('kontrak k', 'k.id = kk.kontrak'),
			),
			'where' => "kk.deleted = 0 and kk.kontrak = '" . $kontrak . "'",
			'orderby' => 'kk.id asc'
		));

		$result = array();
		if(!empty($data)){
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}else{
			array_push($result, array(
				'bulan_ke'=> 0,
				'master' => 0,
				'rencana'=> 0,
				'realisasi'=> 0
			));
		}

		// echo '<pre>';
		// print_r($result);die;
		return json_encode($result);
 }

 public function kirimReminder() {
  $kontrak = $_POST['kontrak'];
  $data_kontrak = $this->getDetailDataKontrak($kontrak);
//   echo '<pre>';
//   print_r($data_kontrak);die;
  $message = $this->load->view('dashboard/reminder_mail', $data_kontrak, true);
  $send_email = Modules::run('email/send_email',
                  $data_kontrak['email_vendor'],
                  $message);

  $is_valid = false;
  if ($send_email) {
   $is_valid = true;
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addItemDisbursment() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_disbursment', $data, true);
 }

 public function addItemKurva() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_kurva', $data, true);
 }

 public function addItemKendala() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_kendala', $data, true);
 }

 public function addItemKorlap() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_korlap', $data, true);
 }

 public function addItemDirlap() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_dirlap', $data, true);
 }

 public function addItemLokasi() {
  $data['index'] = $_POST['index'];
  echo $this->load->view('content_lokasi', $data, true);
 }

 public function showHistoryHarga() {
  $id = $_POST['id'];
  $content['data'] = $this->getListAmandemen($id);
  echo $this->load->view('history_harga', $content, true);
 }

}
