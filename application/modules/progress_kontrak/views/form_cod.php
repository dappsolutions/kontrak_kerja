<form class="forms-sample" method="post">  
 <input type="hidden" value="<?php echo isset($kontrak_cod) ? $kontrak_cod : '' ?>" id="kontrak_cod" class="form-control" />
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Target</label>
  <div class="col-sm-9">
   <input type="text" readonly class="form-control text-right" 
          error="Tanggal Target" id="target" 
          placeholder="Tanggal Target" value="<?php echo isset($target) ? $target : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Realisasi</label>
  <div class="col-sm-9">
   <input  type="text" readonly class="form-control text-right" 
           error="Tanggal Realisasi" id="realisasi" 
           placeholder="Tanggal Realisasi" value="<?php echo isset($realisasi) ? $realisasi : '' ?>">
  </div>
 </div>
 
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Kendala / Keterangan</label>
  <div class="col-sm-9">
   <textarea class="form-control" 
             error="Keterangan" id="ket_cod" 
             placeholder="Keterangan"><?php echo isset($ket_cod) ? $ket_cod : '' ?></textarea>
  </div>
 </div>
</form>