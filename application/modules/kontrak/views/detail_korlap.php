<div class="table-responsive">
 <table class="table table-bordered" id="tb_korlap">
  <thead>
   <tr>
    <th>Korlap</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_korlap)) { ?>
    <?php foreach ($list_korlap as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['korlap'] ?>" placeholder="Korlap" id="korlap" class="form-control" />
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>