<div class="row">
	<div class="col-md-12">
		<div class="card">
   <div class="card-body">
    <div class="table-responsive">
     <table class="table table-bordered" id="tb_probis">
      <thead>
       <tr>
        <th>Nama Probis</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($probis_data)) { ?>
        <?php foreach ($probis_data as $value) { ?>
  							<tr>
  								<td><?php echo $value['nama'] ?></td>
  								<td class="text-center">
  									<div class="list-actions">
  										<a href="#" data-toggle="tooltip" title="Daftar Probis Kontrak Kerja" file="<?php echo $value['file'] ?>" onclick="Probis.showLogoFormTable(this, event)"><i class="ik ik-eye"></i></a>
  								</td>
  							</tr>
        <?php } ?>
       <?php } else { ?>
        <tr>
         <td colspan="4" class="text-center">Tidak ada data ditemukan</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>
  </div>
	</div>
</div>
