<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Pegawai</label>
      <div class="col-sm-9">
       <select disabled class="form-control required" error="Pegawai" id="pegawai">
        <option value="">Pilih Pegawai</option>
        <?php if (!empty($list_pegawai)) { ?>
         <?php foreach ($list_pegawai as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($pegawai)) { ?>
           <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama_pegawai'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Username</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Username" id="username" 
              placeholder="Username" value="<?php echo isset($username) ? $username : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Password</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Password" id="password" 
              placeholder="Password" value="<?php echo isset($password) ? $password : '' ?>">
      </div>
     </div>
     <div class="text-right">
      <a href="<?php echo base_url() . $module ?>">Kembali</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
