<?php echo $this->load->view('header'); ?>


<div class="card">
 <div class="card-header row">
  <div class="col col-sm-3">
   <input type="text" value="" readonly id="tgl_awal" class="form-control" placeholder="Tanggal Awal"/>
  </div>
  <div class="col col-sm-3">
   <input type="text" value="" readonly id="tgl_akhir" class="form-control" placeholder="Tanggal Akhir"/>
  </div>
  <div class="col col-sm-3">
   <button class="btn btn-warning" onclick="LapKontrak.tampilkan(this)">Tampilkan</button>
   &nbsp;
   <a href="#" class="btn btn-success" download="<?php echo 'Laporan Kontrak Kerja' ?>.xls" onclick="return ExcellentExport.excel(this, 'tb_content', 'Kontrak');"><i class="ik ik-file-text"></i> &nbsp;Export</a>
  </div>
 </div>
 <div class="card-body">
  <div class="table-responsive">
   <div class="sticky-table sticky-headers sticky-ltr-cells">
    <table class="table table-bordered" id="tb_content">
     <thead>
      <tr class="sticky-row">
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4" class="text-center sticky-cell">No</th>
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4" class="sticky-cell">No Kontrak</th>
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4" class="sticky-cell">Jenis Pekerjaan</th>
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4" class="sticky-cell">Lokasi</th>       
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="4" rowspan="2">Amandemen</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="2" rowspan="2">Progress Fisik</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="2" rowspan="2">Progress Fisik (Bulan Berjalan)</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="2" rowspan="2">Cod / Enegize</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="9">Monitoring Pembayaran</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="2" rowspan="2">BAST-1 / BASTB</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="2" rowspan="2">BAST-2</th>
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4">Diprek</th>   
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4">Dirlap</th>   
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4">Korlap / Waslap</th>   
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4">Kendala</th>   
       <th style="vertical-align: middle !important;border:1px solid black;" rowspan="4">Keterangan</th>   
      </tr>
      <tr class="sticky-row">
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="5">Rencana Disburse</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="4">Realisasi Bayar</th>
      </tr>
      <tr class="sticky-row">
       <!--Amandemen-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Nomor</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Uraian</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Tanggal Mulai</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Tanggal Selesai</th>

       <!--Progress Fisik-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Kurva S (%)</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Realisasi (%)</th>

       <!--Progress Fisik Bulan-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Kurva S (%)</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Realisasi (%)</th>

       <!--Cod / Energize-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Target</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Realisasi</th>

       <!--Rencana-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" colspan="3">Bayar Termin</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Bulan</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Jumlah (Rp)</th>

       <!--Realisasi-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Tgl BA Pembayaran</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Nilai (Rp)</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Prosentase Pembayaran (%)</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Pembayaran Denda (Rp)</th>

       <!--BAST 1-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Nomor</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Tanggal</th>

       <!--BAST 2-->
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Nomor</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center" rowspan="2">Tanggal</th>

      </tr>

      <tr class="sticky-row">
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center">Ke</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center">% Fisik</th>
       <th style="vertical-align: middle !important;border:1px solid black;" class="text-center">% Bayar</th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($content)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($content as $key => $v_head) { ?>
        <tr>
         <td colspan="33" class="bg-primary-light text-white" style="border:1px solid black;"><?php echo $key ?></td>
        </tr>

        <?php foreach ($v_head as $key_kontrak => $v_kontrak) { ?>
         <tr>
          <td style="vertical-align: top !important;border:1px solid black;" class="text-center sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $no++ ?></td>
          <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>">
           <table>
            <tbody>
             <tr>
              <td style="border:none"><?php echo $v_kontrak[0]['pekerjaan'] ?></td>
             </tr>
             <tr>
              <td style="border:none">&nbsp;</td>
             </tr>
             <tr>
              <td class="" style="font-weight: bold;border:none;">Administrasi</td>
              <td class="" style="border:none;">:</td>
             </tr>
             <tr>
              <td style="border:none;">- RAB </td>
              <td style="border:none;">:</td>
             </tr>
             <tr>
              <td style="border:none;">- RKS </td>
              <td style="border:none;">:</td>
             </tr>
             <tr>
              <td style="border:none;">- PRK </td>
              <td style="border:none;">:</td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Kontrak No</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['no_kontrak'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Tanggal Kontrak</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['tanggal_kontrak'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Tanggal Akhir Kontrak</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['tanggal_selesai_kontrak'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Nilai</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo number_format($v_kontrak[0]['harga_kontrak'], 0, ',', '.') ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Pelaksana</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['pelaksana'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Nama Direktur / Leader KSO</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['leader'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">Alamat</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['alamat'] ?></td>
             </tr>
             <tr>
              <td style="font-weight: bold;border:none;">No Telp</td>
              <td style="border:none;">:</td>
              <td style="border:none;"><?php echo $v_kontrak[0]['no_hp'] ?></td>
             </tr>
            </tbody>
           </table>           
          </td>
          <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['jenis'] ?></td>
          <td style="vertical-align: top !important;border:1px solid black;" class="sticky-cell" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['lokasi_pekerjaan'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['no_amandemen'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['uraian'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_mulai'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_selesai'] ?></td>


          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['kurva_before'] . ' %' ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi_before'] . ' %' ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['kurva_after'] . ' %' ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi_after'] . ' %' ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['target'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['realisasi'] ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['termin_ke'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['fisik'] . ' %' ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['bayar'] . ' %' ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal'] ?></td>
          <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['jumlah'], 0, ',', '.') ?></td>


          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_ba'] ?></td>
          <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['nilai'], 0, ',', '.') ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['persentase_bayar'] . ' %' ?></td>
          <td style="border:1px solid black;"><?php echo number_format($v_kontrak[0]['denda'], 0, ',', '.') ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['nomor_kb1'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_kb1'] ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['nomor_kb2'] ?></td>
          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['tanggal_kb2'] ?></td>

          <td style="border:1px solid black;"><?php echo $v_kontrak[0]['diprek'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['dirlap'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['korlap'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"><?php echo $v_kontrak[0]['kendala'] ?></td>
          <td style="border:1px solid black;" rowspan="<?php echo count($v_kontrak) ?>"></td>
         </tr>
         <?php if (count($v_kontrak) > 1) { ?>
          <?php foreach ($v_kontrak as $key_detail => $value) { ?>
           <?php if ($key_detail != 0) { ?>
            <tr>
             <td style="border:1px solid black;"><?php echo $value['no_amandemen'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['uraian'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['tanggal_mulai'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['tanggal_selesai'] ?></td>


             <td style="border:1px solid black;"><?php echo $value['kurva_before'] . ' %' ?></td>
             <td style="border:1px solid black;"><?php echo $value['realisasi_before'] . ' %' ?></td>

             <td style="border:1px solid black;"><?php echo $value['kurva_after'] . ' %' ?></td>
             <td style="border:1px solid black;"><?php echo $value['realisasi_after'] . ' %' ?></td>

             <td style="border:1px solid black;"><?php echo $value['target'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['realisasi'] ?></td>

             <td style="border:1px solid black;"><?php echo $value['termin_ke'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['fisik'] . ' %' ?></td>
             <td style="border:1px solid black;"><?php echo $value['bayar'] . ' %' ?></td>
             <td style="border:1px solid black;"><?php echo $value['tanggal'] ?></td>
             <td style="border:1px solid black;"><?php echo number_format($value['jumlah'], 0, ',', '.') ?></td>


             <td style="border:1px solid black;"><?php echo $value['tanggal_ba'] ?></td>
             <td style="border:1px solid black;"><?php echo number_format($value['nilai'], 0, ',', '.') ?></td>
             <td style="border:1px solid black;"><?php echo $value['persentase_bayar'] . ' %' ?></td>
             <td style="border:1px solid black;"><?php echo number_format($value['denda'], 0, ',', '.') ?></td>

             <td style="border:1px solid black;"><?php echo $value['nomor_kb1'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['tanggal_kb1'] ?></td>

             <td style="border:1px solid black;"><?php echo $value['nomor_kb2'] ?></td>
             <td style="border:1px solid black;"><?php echo $value['tanggal_kb2'] ?></td>

             <td style="border:1px solid black;"><?php echo $value['diprek'] ?></td>
            </tr>
           <?php } ?>           
          <?php } ?>  
         <?php } ?>                
        <?php } ?>
       <?php } ?>       
      <?php } else { ?>
       <tr>
        <td colspan="4" class="text-center">Tidak ada data ditemukan</td>
       </tr>
      <?php } ?> 
     </tbody>
    </table>
   </div>   
  </div>

  <div class="text-right">
   <nav aria-label="Page navigation example">
    <ul class="pagination mb-0 pull-right">

    </ul>
   </nav>
  </div>
 </div>
</div>
