<div class="table-responsive">
 <table class="table table-bordered" id="tb_kurva">
  <thead>
   <tr>
    <th>Bulan Ke</th>
    <th>Master (%)</th>
    <th>Rencana (%)</th>
    <th>Realisasi (Rp)</th>
    <th>Keterangan Realisasi</th>
    <th class="text-center">Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_kurva)) { ?>
    <?php $index = 0; ?>
    <?php foreach ($list_kurva as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input type="text" value="<?php echo $value['bulan_ke'] ?>" placeholder="Bulan Ke" id="bulan_ke<?php echo $index++ ?>" class="form-control bulan_ke" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['master'] ?>" placeholder="Master (%)" id="master" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['rencana'] ?>" placeholder="Rencana (%)" id="rencana" class="form-control" />
      </td>
      <td>
       <input type="text" value="<?php echo $value['realisasi'] ?>" placeholder="Realisasi" id="realisasi" class="form-control" />
      </td>
      <td>
       <textarea id="keterangan_realisasi" class="form-control"><?php echo $value['ket_realisasi'] ?></textarea>
      </td>
      <td class="text-center">
       <input type="checkbox" <?php echo $value['is_amandemen'] == '1' ? 'checked' : '' ?> value="" id="check_amandemen" class="" />
       Amandemen
       &nbsp;<br/>
       <i class="ik ik-trash ik-2x" onclick="Kontrak.removeItem(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
   <tr class="" data_id="">
    <td colspan="6"><a href="" onclick="Kontrak.addItemKurva(this, event)" class="btn btn-primary">Tambah Item</a></td>
   </tr>
  </tbody>
 </table>
</div>