<div class="app-sidebar colored">
	<div class="sidebar-header color-pln">
		<a class="header-brand" href="<?php echo base_url() . 'dashboard' ?>">
			<div class="logo-img">
				<img width="25" height="30" src="<?php echo base_url() ?>assets/images/logo-pln.png" class="header-brand-img" alt="lavalite">
			</div>
			<span class="text">SILAKON</span>
		</a>
		<button type="button" class="nav-toggle"><i data-toggle="collapsed" class="ik ik-toggle-right toggle-icon"></i></button>
		<button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
	</div>
</div>
