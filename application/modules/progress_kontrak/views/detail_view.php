<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <h4><?php echo $no_progress ?></h4>
    <hr/>
    <br/>
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Nomor Kontrak</label>
      <div class="col-sm-9">
       <select disabled class="form-control required" error="Nomor Kontrak" id="no_kontrak">
        <option value="">Pilih Kontrak</option>
        <?php if (!empty($list_kontrak)) { ?>
         <?php foreach ($list_kontrak as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($kontrak)) { ?>
           <?php $selected = $kontrak == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_kontrak'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

<!--     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Lokasi</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Lokasi" id="lokasi" 
              placeholder="Lokasi" value="<?php echo isset($lokasi) ? $lokasi : '' ?>">
      </div>
     </div>
     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Diprek</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Diprek" id="diprek" 
              placeholder="Diprek" value="<?php echo isset($diprek) ? $diprek : '' ?>">
      </div>
     </div>
     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Dirlap</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Dirlap" id="dirlap" 
              placeholder="Dirlap" value="<?php echo isset($dirlap) ? $dirlap : '' ?>">
      </div>
     </div>
     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Korlap</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Korlap" id="korlap" 
              placeholder="Korlap" value="<?php echo isset($korlap) ? $korlap : '' ?>">
      </div>
     </div>
     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Kendala</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Kendala" id="kendala" 
              placeholder="Kendala" value="<?php echo isset($kendala) ? $kendala : '' ?>">
      </div>
     </div>
     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Keterangan</label>
      <div class="col-sm-9">
       <textarea disabled class="form-control required" 
             error="Keterangan" id="keterangan" 
             placeholder="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>
     </div>-->
    </form>
   </div>
  </div>
  <hr/>
  <br/>

  <div class="row">
   <div class="col-md-12">
    <ul class="nav nav-pills nav-fill" id="nav_input">
     <li class="nav-item">
      <a class="nav-link active" action="form_amandemen" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">AMANDEMEN</a>
     </li>
     <li class="nav-item">
      <a class="nav-link font-bold" action="form_progress_fisik" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">PROGRESS FISIK</a>
     </li>
     <li class="nav-item">
      <a class="nav-link font-bold" action="form_cod" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">COD / ENERGIZE</a>
     </li>
     <li class="nav-item">
      <a class="nav-link font-bold " action="form_bayar" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">PEMBAYARAN</a>
     </li>
     <li class="nav-item">
      <a class="nav-link font-bold " action="form_bast1" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">BAST - 1</a>
     </li>
     <li class="nav-item">
      <a class="nav-link font-bold " action="form_bast2" onclick="ProgressKontrak.clickTabInput(this, event)" href="#">BAST - 2</a>
     </li>
    </ul>
   </div>
  </div>
  <br/>
  <hr/>
  <br/>
  <div class="row">
   <div class="col-md-6">
    <div class="form_input" id="form_amandemen">
     <?php echo $this->load->view('detail_amandemen'); ?>
    </div>

    <div class="form_input hidden" id="form_progress_fisik">
     <?php echo $this->load->view('detail_progress_fisik'); ?>
    </div>

    <div class="form_input hidden" id="form_cod">
     <?php echo $this->load->view('detail_cod'); ?>
    </div>

    <div class="form_input hidden" id="form_bayar">
     <?php echo $this->load->view('detail_bayar'); ?>
    </div>

    <div class="form_input hidden" id="form_bast1">
     <?php echo $this->load->view('detail_bast1'); ?>
    </div>

    <div class="form_input hidden" id="form_bast2">
     <?php echo $this->load->view('detail_bast2'); ?>
    </div>
    
    <div class="text-right">
     <a href="<?php echo base_url() . $module ?>">Kembali</a>
    </div>
   </div>
  </div>
 </div>
</div>
