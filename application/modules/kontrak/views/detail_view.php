<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <h4>Kontrak</h4>
    <hr/>
    <br/>
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">UPT</label>
      <div class="col-sm-9">
       <select disabled class="form-control required" error="UPT" id="upt">
        <option value="">Pilih UPT</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Vendor</label>
      <div class="col-sm-9">
       <select disabled class="form-control required" error="Vendor" id="vendor">
        <option value="">Pilih Vendor</option>
        <?php if (!empty($list_vendor)) { ?>
         <?php foreach ($list_vendor as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($vendor)) { ?>
           <?php $selected = $vendor == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_vendor'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Jenis Pekerjaan</label>
      <div class="col-sm-9">
       <select disabled class="form-control required" error="Jenis Pekerjaan" id="jenis">
        <option value="">Pilih Jenis Pekerjaan</option>
        <?php if (!empty($list_jenis)) { ?>
         <?php foreach ($list_jenis as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($jenis_pekerjaan)) { ?>
           <?php $selected = $jenis_pekerjaan == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Pekerjaan</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Pekerjaan" id="pekerjaan" 
              placeholder="Pekerjaan" value="<?php echo isset($pekerjaan) ? $pekerjaan : '' ?>">
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">No Kontrak</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="No Kontrak" id="no_kontrak" 
              placeholder="No Kontrak" value="<?php echo isset($no_kontrak) ? $no_kontrak : '' ?>">
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Harga Kontrak</label>
      <div class="col-sm-9">
       <?php if (isset($harga_kontrak)) { ?>
        <?php if (!empty($last_amandemen)) { ?>         
         <?php if ($last_amandemen['nilai_kontrak'] != 0) { ?>
          <?php $harga_kontrak = $last_amandemen['nilai_kontrak'] ?>
         <?php } ?>         
        <?php } ?>
        <input disabled type="text" class="form-control required" 
               error="Harga Kontrak" id="harga_kontrak" 
               placeholder="Harga Kontrak" value="<?php echo isset($harga_kontrak) ? $harga_kontrak : '' ?>">             
        <i class="ik ik-archive" data-toggle="tooltip" title="History Harga Kontrak" onclick="Kontrak.showHistoryHarga(this)"></i>
         
              <?php } else { ?>
        <input disabled type="text" class="form-control required" 
               error="Harga Kontrak" id="harga_kontrak" 
               placeholder="Harga Kontrak" value="<?php echo isset($harga_kontrak) ? $harga_kontrak : '' ?>">             
              <?php } ?> 
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Leader</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Leader" id="leader" 
              placeholder="Leader" value="<?php echo isset($leader) ? $leader : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Tanggal Kontrak</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Tanggal Kontrak" id="tgl_kontrak" 
              placeholder="Tanggal Kontrak" value="<?php echo isset($tanggal_kontrak) ? $tanggal_kontrak : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Tanggal Selesai Kontrak</label>
      <div class="col-sm-9">
       <input disabled type="text" class="form-control required" 
              error="Tanggal Selesai Kontrak" id="tgl_selesai_kontrak" 
              placeholder="Tanggal Selesai Kontrak" value="<?php echo isset($tanggal_selesai_kontrak) ? $tanggal_selesai_kontrak : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Tanggal Reminder</label>
      <div class="col-sm-9">
       <input disabled="" type="text" class="form-control required" 
              error="Tanggal Reminder" id="tgl_reminder" 
              placeholder="Tanggal Reminder" value="<?php echo isset($reminder_date) ? $reminder_date : '' ?>">
      </div>
     </div>
     <!-- <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Keterangan</label>
      <div class="col-sm-9">
       <textarea disabled class="form-control required" 
                 error="Keterangan" id="keterangan" 
                 placeholder="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>
     </div>      -->
    </form>
   </div>

   <div class="col-md-6">
    <h4>Lokasi</h4>
    <hr/>
    <br/>
    <?php echo $this->load->view('detail_progress'); ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Disbursment</h4>
    <?php echo $this->load->view('detail_disburstment') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Kurva S</h4>
    <?php echo $this->load->view('detail_kurva') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Kendala</h4>
    <?php echo $this->load->view('detail_kendala') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Korlap</h4>
    <?php echo $this->load->view('detail_korlap') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Dirlap</h4>
    <?php echo $this->load->view('detail_dirlap') ?>
   </div>
  </div>
  <hr/>

  <div class="row">
   <div class="col-md-12">
    <h4>Lokasi</h4>
    <?php echo $this->load->view('detail_lokasi') ?>
   </div>
  </div>
  <hr/>
 </div>

 <div class="card-footer">
  <div class="text-right">
   <?php if ($status == 'DRAFT') { ?>
    <button type="submit" class="btn btn-success mr-2" onclick="Kontrak.selesai('<?php echo isset($id) ? $id : '' ?>', event)">Kontrak Selesai</button>
    <button type="submit" class="btn btn-primary mr-2" onclick="Kontrak.addProgress('<?php echo isset($id) ? $id : '' ?>', event)">Progres Pekerjaan</button>
   <?php } ?>      
   <a href="<?php echo base_url() . $module ?>">Kembali</a>
  </div>
 </div>
</div>
