<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">UPT</label>
      <div class="col-sm-9">
       <select class="form-control required" error="UPT" id="upt">
        <option value="">Pilih Upt</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Nip</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Nip" id="nip" 
              placeholder="Nip" value="<?php echo isset($nip) ? $nip : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Pegawai</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Pegawai" id="nama" 
              placeholder="Pegawai" value="<?php echo isset($nama_pegawai) ? $nama_pegawai : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Posisi</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Posisi" id="posisi" 
              placeholder="Posisi" value="<?php echo isset($posisi) ? $posisi : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Email</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Email" id="email" 
              placeholder="Email" value="<?php echo isset($email) ? $email : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">No Hp</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="No Hp" id="no_hp" 
              placeholder="No Hp" value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
      </div>
     </div>
     <div class="text-right">
      <button type="submit" class="btn btn-primary mr-2" onclick="Pegawai.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
      <a href="<?php echo base_url() . $module ?>">Batal</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
