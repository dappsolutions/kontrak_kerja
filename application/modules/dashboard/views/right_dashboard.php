<div class="col-md-3 clearfix">
	<div class="card">
		<div class="card-body">
			<div class="col-lg-12 col-md-6 col-sm-12">
				<div class="widget bg-warning">
					<div class="widget-body">
						<div class="d-flex justify-content-between align-items-center">
							<div class="state">
								<h6>Kontrak Progress</h6>
								<h2 id="total_kontrak_progress"><?php echo $total_kontrak ?></h2>
							</div>
							<div class="icon">
								<i class="ik ik-file-text"></i>
							</div>
						</div>
						<small class="text-small mt-10 d-block">Total Kontrak</small>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-6 col-sm-12">
				<div class="widget bg-success">
					<div class="widget-body">
						<div class="d-flex justify-content-between align-items-center">
							<div class="state">
								<h6>Kontrak Selesai</h6>
								<h2 id="total_kontrak_selesai"><?php echo $total_kontrak_selesai ?></h2>
							</div>
							<div class="icon">
								<i class="ik ik-calendar"></i>
							</div>
						</div>
						<small class="text-small mt-10 d-block">Total Kontrak Selesai</small>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-6 col-sm-12">
				<h5>Grafik Presentase Kontrak</h5>
				<div id="grafik-presentase"></div>
				<canvas id="canvas_all" width="100" height="100"></canvas>
			</div>
		</div>
	</div>
</div>
