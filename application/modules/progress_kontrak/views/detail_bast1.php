<form class="forms-sample" method="post">  

 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Nomor</label>
  <div class="col-sm-9">
   <input disabled type="text" class="form-control required text-right" 
          error="Nomor" id="nomor_bast1" 
          placeholder="Nomor" value="<?php echo isset($nomor_bast1) ? $nomor_bast1 : '0' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal</label>
  <div class="col-sm-9">
   <input disabled  type="text" readonly="" class="form-control required text-right" 
          error="Tanggal" id="tanggal_bast1" 
          placeholder="Tanggal" value="<?php echo isset($tanggal_bast1) ? $tanggal_bast1: '' ?>">
  </div>
 </div>
</form>