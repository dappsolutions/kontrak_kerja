<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();

  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
//      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/speedochart/Chart.js"></script>',
      '<script src="' . base_url() . 'assets/js/speedochart/Gauge.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['total_user'] = $this->getTotalUser();
  $data['total_vendor'] = $this->getTotalVendor();
  $data['total_kontrak'] = $this->getTotalKontrak();
  $data['total_kontrak_selesai'] = $this->getTotalKontrakSelesai();
  $data['remember_kontrak'] = $this->getRememberDateKontrak();
  $data['hak_akses'] = $this->session->userdata('hak_akses');
  echo Modules::run('template', $data);
 }

 public function getTotalUser() {
  $total = Modules::run('database/count_all', array(
              'table' => 'user',
              'where' => "deleted = 0"
  ));
  return $total;
 }

 public function getTotalVendor() {
  $total = Modules::run('database/count_all', array(
              'table' => 'vendor',
              'where' => "deleted = 0"
  ));
  return $total;
 }

 public function getTotalKontrak() {
  $where = "k.deleted = 0 and (ks.status = 'DRAFT' or ks.status is null)";
  if ($this->hak_akses != 'superadmin') {
   $where = "k.deleted = 0 and (ks.status = 'DRAFT' or ks.status is null) and u.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'kontrak k',
              'field' => array('k.*', 'u.nama_upt', 'ks.status'),
              'join' => array(
                  array('upt u', 'k.upt = u.id'),
                  array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id', 'left'),
                  array('kontrak_status ks', 'ks.id = kss.id', 'left'),
              ),
              'where' => $where
  ));

//    echo "<pre>";
//    echo $this->db->last_query();
//    die;
  return $total;
 }

 public function getTotalKontrakSelesai() {
  $where = "k.deleted = 0 and ks.status = 'DONE'";
  if ($this->hak_akses != 'superadmin') {
   $where = "k.deleted = 0 and ks.status = 'DONE' and u.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'kontrak k',
              'field' => array('k.*', 'u.nama_upt', 'ks.status'),
              'join' => array(
                  array('upt u', 'k.upt = u.id'),
                  array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
                  array('kontrak_status ks', 'ks.id = kss.id'),
              ),
              'where' => $where
  ));
  return $total;
 }

 public function getRememberDateKontrak() {
  $where = "where pk.deleted = 0 or k.deleted = 0";
  if ($this->hak_akses != 'superadmin') {
   $where = "where (pk.deleted = 0 or k.deleted = 0) and ut.id = '" . $this->upt . "'";
  }

  $sql = "select 
	k.no_kontrak
	, v.nama_vendor
	, ut.nama_upt
	, jp.jenis as jenis_pekerjaan	
	, pk.lokasi
	, ka.no_amandemen
	, ka.uraian
	, ka.tanggal_mulai
	, ka.tanggal_selesai
	, kpf.kurva_before
	, kpf.realisasi_before
	, kpf.kurva_after
	, kpf.realisasi_after
	, kc.target
	, kc.realisasi
	, kb.jumlah_termin
	, kb.fisik
	, kb.bayar
	, kb.tanggal
	, kb.jumlah
	, kb.tanggal_ba
	, kb.nilai
	, kb.persentase_bayar
	, kb.denda
	, kb1.nomor as nomor_bast1
	, kb1.tanggal as tanggal_bast1
	, kb2.nomor as nomor_bast2
	, kb2.tanggal as tanggal_bast2
	, pk.diprek
	, pk.dirlap
	, pk.korlap
	, pk.kendala
	, pk.keterangan
	from kontrak k
	join progress_kontrak pk
		on k.id = pk.kontrak
	join jenis_pekerjaan jp
		on jp.id = k.jenis_pekerjaan
 left join (select max(id) id, progress_kontrak from progress group by progress_kontrak) ppg
  on ppg.progress_kontrak = pk.id
 left join progress pg
  on pg.id = ppg.id
	left join kontrak_amandemen ka
		on ka.progress = pg.id
	left join kontrak_progress_fisik kpf
		on kpf.progress = pg.id
	left join kontrak_cod kc
		on kc.progress = pg.id
	left join kontrak_bayar kb
		on kb.progress = pg.id
	left join kontrak_bast1 kb1
		on kb1.progress = pg.id
	left join kontrak_bast2 kb2
		on kb2.progress = pg.id
	left join vendor v
		on k.vendor = v.id
	join upt ut
		on ut.id = k.upt
	" . $where . " ";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

}
