<div class="page-header">
 <div class="row align-items-end">
  <div class="col-lg-8">
   <div class="page-header-title">
    <i class="ik ik-edit bg-blue"></i>
    <div class="d-inline">
     <h5><?php echo strtoupper($title) ?></h5>
     <span><?php echo $title_content ?></span>
    </div>
   </div>
  </div>
  <div class="col-lg-4">
   <nav class="breadcrumb-container" aria-label="breadcrumb">
    <ol class="breadcrumb">
     <li class="breadcrumb-item">
      <a href="../index.html"><i class="ik ik-home"></i></a>
     </li>
     <li class="breadcrumb-item"><a href="#"><?php echo $root_module ?></a></li>
     <li class="breadcrumb-item active" aria-current="page"><?php echo $title_content ?></li>
    </ol>
   </nav>
  </div>
 </div>
</div>