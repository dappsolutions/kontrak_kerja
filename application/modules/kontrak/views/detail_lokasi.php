<div class="table-responsive">
 <table class="table table-bordered" id="tb_lokasi">
  <thead>
   <tr>
    <th>Lokasi</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_lokasi)) { ?>
    <?php foreach ($list_lokasi as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['lokasi'] ?>" placeholder="Lokasi" id="lokasi" class="form-control" />
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>