<!--<script src="<?php echo base_url() ?>assets/plugins/flot-charts/jquery.flot.js"></script>-->
<label id="tick_data_kurva" class='hidden'><?php echo json_encode($data_kurva['tick_data']) ?></label>
<label id="master_data_kurva" class='hidden'><?php echo json_encode($data_kurva['master']) ?></label>
<label id="rencana_data_kurva" class='hidden'><?php echo json_encode($data_kurva['rencana']) ?></label>
<label id="realisasi_data_kurva" class='hidden'><?php echo json_encode($data_kurva['realisasi']) ?></label>
<label id="ket_realisasi_data_kurva" class='hidden'><?php echo json_encode($data_kurva['ket_realisasi']) ?></label>
<label id="max_kurva" class='hidden'><?php echo json_encode($data_kurva['max']) ?></label>

<div class="row">
 <div class="col-md-6">
  <div class="table-responsive">
   <table class="table table-bordered table-striped">
    <tbody> 
     <tr>
      <td style="font-weight: bold;border:none;">Kontrak No</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo $data_kontrak['no_kontrak'] ?></td>
     </tr>     
     <tr>
      <td style="font-weight: bold;border:none;">Pekerjaan</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo $data_kontrak['pekerjaan'] ?></td>
     </tr>
     <tr>
      <td style="font-weight: bold;border:none;">Tanggal Kontrak</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo $data_kontrak['tanggal_kontrak'] ?></td>
     </tr>
     <tr>
      <td style="font-weight: bold;border:none;">Tanggal Akhir Kontrak</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo $data_kontrak['tanggal_selesai_kontrak'] ?></td>
     </tr>
     <tr>
      <td style="font-weight: bold;border:none;">Nilai</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo number_format($data_kontrak['harga_kontrak'], 0, ',', '.') ?></td>
     </tr>
     <tr>
      <td style="font-weight: bold;border:none;">Nama Direktur / Leader KSO</td>
      <td style="border:none;">:</td>
      <td style="border:none;"><?php echo $data_kontrak['leader'] ?></td>
     </tr>
    </tbody>
   </table>  
  </div>
 </div>

 <div class="col-md-6">
  <div class="table-responsive">
   <table class="table table-bordered table-striped">
    <thead>
     <tr>
      <th>No</th>
      <th>No Amandemen</th>
      <th>Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_amandemen)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($data_amandemen as $value) { ?>
       <tr>
        <td><?php echo $no++ ?></td>
        <td><?php echo $value['no_amandemen'] ?></td>
        <td class="text-center">
         <div class="list-actions">
          <a href="#" data-toggle="tooltip" title="Detil Amandemen Kontrak" onclick="Dashboard.detailProgress('<?php echo $value['progress_id'] ?>')"><i class="ik ik-eye"></i></a>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="3">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?>
    </tbody>
   </table>  
  </div>
 </div>
</div>
<br/>
<div class="row">
 <div class="col-md-12">
  <div class="card">
   <div class="card-header">
    <i class="fa fa-bar-chart-o"></i>

    <h3 class="card-title">Kurva S</h3>
   </div>
   <div class="card-body">
    <div id="line-chart" style="max-width: none;height: 300px;width: 820px;"></div>
   </div>
   <!-- /.box-body-->
  </div>
 </div>
</div>
<br/>
<br/>
<br/>

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered table-striped" id="tb_content">
    <thead>
     <tr>
      <th class="text-center">No</th>
      <th>No Progress</th>
      <th>Progress</th>
      <th>No Kontrak</th>
      <th>Upt</th>
      <th class="text-center">Detail</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr>
        <td class="text-center"><?php echo $no++ ?></td>
        <td><?php echo $value['no_progress'] ?></td>
        <td><?php echo $value['createddate'] ?></td>
        <td><?php echo $value['no_kontrak'] ?></td>
        <td><?php echo $value['nama_upt'] ?></td>
        <td class="text-center">
         <div class="list-actions">
          <a href="#" data-toggle="tooltip" title="Detil Progress Kontrak" onclick="Dashboard.detailProgress('<?php echo $value['id'] ?>')"><i class="ik ik-eye"></i></a>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?> 
    </tbody>
   </table>
  </div>
 </div>
</div>

