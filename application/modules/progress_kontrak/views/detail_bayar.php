<form class="forms-sample" method="post">  

 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Disbursment Termin Ke</label>
  <div class="col-sm-9 text-right">
   <select disabled="" class="form-control required" id="kontrak_plan" error="Disbursment">
    <option value="">Pilih Disbursment</option>
    <?php if (!empty($list_disbursment)) { ?>
     <?php foreach ($list_disbursment as $value) { ?>
      <?php $selected = '' ?>
      <?php if (isset($kontrak_plan)) { ?>
       <?php $selected = $kontrak_plan == $value['id'] ? 'selected' : '' ?>
      <?php } ?>
      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jumlah_termin']  ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal B.A</label>
  <div class="col-sm-9">
   <input disabled  type="text" readonly class="form-control required" 
          error="Tanggal B.A" id="tanggal_ba" 
          placeholder="Tanggal B.A" value="<?php echo isset($tanggal_ba) ? $tanggal_ba: '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Nilai</label>
  <div class="col-sm-9">
   <input disabled  type="text" min="0" class="form-control required text-right" 
          error="Nilai" id="nilai" 
          placeholder="Nilai" value="<?php echo isset($nilai) ? $nilai: '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Persentase Bayar (%)</label>
  <div class="col-sm-9">
   <input disabled  type="number" min="0" class="form-control required text-right" 
          error="Presentase Bayar (%)" id="persentase_bayar" 
          placeholder="Presentase Bayar (%)" value="<?php echo isset($persentase_bayar) ? $persentase_bayar: '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Denda</label>
  <div class="col-sm-9">
   <input disabled  type="text" min="0" class="form-control required text-right" 
          error="Denda" id="denda" 
          placeholder="Denda" value="<?php echo isset($denda) ? $denda: '' ?>">
  </div>
 </div>
</form>