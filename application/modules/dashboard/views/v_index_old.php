<?php echo $this->load->view('top_dashboard'); ?>

<div class="row">
 <div class="col-md-8">
  <div class="card">
   <div class="card-header row">
    <div class="col col-sm-6">
     <h5>Remember Date Vendor</h5>
    </div>
    <div class="col col-sm-6">
     <div class="card-search with-adv-search dropdown">
      <input type="text" class="form-control" id="keyword" 
             placeholder="Pencarian" required="" onkeyup="Dashboard.searchInTableKontrak(this, event)">
      <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
     </div>
    </div>
   </div>
   <div class="card-body">
    <div class="text-right">
     <a href="#" class="btn btn-success" download="<?php echo 'Kontrak' ?>.xls" onclick="return ExcellentExport.excel(this, 'tb_kontrak', 'Kontrak');"><i class="ik ik-file-text"></i> &nbsp;Export</a>
    </div>
    <br/> 

    <div class="table-responsive">
     <table class="table table-bordered table-striped" id="tb_kontrak">
      <thead>
       <tr>
        <th rowspan="3" class="text-center">No</th>
        <th rowspan="3">No Kontrak</th>
        <th rowspan="3">Vendor</th>
        <th rowspan="3">Upt</th>
        <th rowspan="3">Jenis Pekerjaan</th>
        <th rowspan="3">Lokasi</th>
        <th colspan="4">Amandemen</th>
        <th colspan="2">Progress Fisik (Bulan Sebelumnya)</th>
        <th colspan="2">Progress Fisik (Bulan Berjalan)</th>
        <th colspan="2">COD / ENERGIZE</th>
        <th colspan="9">Monitoring Pembayaran</th>
        <th colspan="2">Bast-1 / Bastb</th>
        <th colspan="2">Bast-2 / Bastb</th>
        <th rowspan="3">Dirpek</th>
        <th rowspan="3">Dirlap</th>
        <th rowspan="3">Korlap / Waslap</th>
        <th rowspan="3">Kendala</th>
        <th rowspan="3">Keterangan</th>
       </tr>
       <tr>
        <th rowspan="2">Nomor</th>
        <th rowspan="2">Uraian</th>
        <th rowspan="2">Tanggal Mulai</th>
        <th rowspan="2">Tanggal Selesai</th>
        <th rowspan="2">Kurva S (%)</th>
        <th rowspan="2">Realisasi (%)</th>
        <th rowspan="2">Kurva S (%)</th>
        <th rowspan="2">Realisasi (%)</th>
        <th rowspan="2">Target</th>
        <th rowspan="2">Realisasi</th>
        <th colspan="3">Rencana Disburse</th>
        <th colspan="6">Realisasi Bayar</th>
        <th rowspan="2">Nomor</th>
        <th rowspan="2">Tanggal</th>
        <th rowspan="2">Nomor</th>
        <th rowspan="2">Tanggal</th>
       </tr>
       <tr>
        <th>Jumlah Termin Ke</th>
        <th>% Fisik</th>
        <th>% Bayar</th>
        <th>Bulan</th>
        <th>Jumlah (Rp)</th>
        <th>Tanggal Ba Pembayaran</th>
        <th>Nilai (Rp)</th>
        <th>Persentase Pembayaran (%)</th>
        <th>Pembayaran Denda (Rp)</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($remember_kontrak)) { ?>
        <?php $no = 1; ?>
        <?php foreach ($remember_kontrak as $value) { ?>
         <tr>
          <td class="text-center">        
           <?php echo $no++ ?>
          </td>
          <td><?php echo $value['no_kontrak'] ?></td>
          <td>
    <!--         <img src="<?php echo base_url() ?>assets/images/users/images.png" class="table-user-thumb" alt="">
           &nbsp;-->
           <?php echo $value['nama_vendor'] ?>
          </td>
          <td><?php echo $value['nama_upt'] ?></td>
          <td><?php echo $value['jenis_pekerjaan'] ?></td>
          <td><?php echo $value['lokasi'] ?></td>
          <td><?php echo $value['no_amandemen'] ?></td>
          <td><?php echo $value['uraian'] ?></td>
          <td><?php echo $value['tanggal_mulai'] ?></td>
          <td><?php echo $value['tanggal_selesai'] ?></td>
          <td><?php echo $value['kurva_before'] ?></td>
          <td><?php echo $value['realisasi_before'] ?></td>
          <td><?php echo $value['kurva_after'] ?></td>
          <td><?php echo $value['realisasi_after'] ?></td>
          <td><?php echo $value['target'] ?></td>
          <td><?php echo $value['realisasi'] ?></td>
          <td><?php echo $value['jumlah_termin'] ?></td>
          <td><?php echo $value['fisik'] ?></td>
          <td><?php echo $value['bayar'] ?></td>
          <td><?php echo $value['tanggal'] ?></td>
          <td><?php echo number_format($value['jumlah']) ?></td>
          <td><?php echo $value['tanggal_ba'] ?></td>
          <td><?php echo number_format($value['nilai']) ?></td>
          <td><?php echo $value['persentase_bayar'] ?></td>
          <td><?php echo number_format($value['denda']) ?></td>
          <td><?php echo $value['nomor_bast1'] ?></td>
          <td><?php echo $value['tanggal_bast1'] ?></td>
          <td><?php echo $value['nomor_bast2'] ?></td>
          <td><?php echo $value['tanggal_bast2'] ?></td>
          <td><?php echo $value['diprek'] ?></td>
          <td><?php echo $value['dirlap'] ?></td>
          <td><?php echo $value['korlap'] ?></td>
          <td><?php echo $value['kendala'] ?></td>
          <td><?php echo $value['keterangan'] ?></td>
         </tr>
        <?php } ?>
       <?php } else { ?>    
        <tr>
         <td class="text-center" colspan="40">Tidak ada data ditemukan</td>
        </tr>
       <?php } ?>    
      </tbody>
     </table>
    </div>
   </div>
  </div>
 </div>
 
 <div class="col-md-4">
 <div class="card" style="min-height: 422px;">
  <div class="card-header"><h3>Grafik Presentase</h3></div>
  <div class="card-body">
   <!--<div id="grafik-presentase"></div>-->
   <canvas id="canvas" width="100" height="100"></canvas>
  </div>
 </div>
</div>
</div>