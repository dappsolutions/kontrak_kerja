var Kontrak = {
 module: function () {
  return 'kontrak';
 },

 moduleApp: function () {
  return "kontrak_kerja";
 },

 add: function () {
  window.location.href = url.base_url(Kontrak.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(Kontrak.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Kontrak.module()) + "index";
 },

 setSessionKeyword: function () {
  var keyWord = $('#keyword').val();
  var search_by = $('#pencarian').val();
  $.ajax({
   type: 'POST',
   data: {
    keyWord: keyWord,
    search_by: search_by
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Kontrak.module()) + "setSessionKeyword",
   error: function () {
    showDangerToast("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    window.location.href = url.base_url(Kontrak.module()) + "search";
   }
  });
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    Kontrak.setSessionKeyword();
//    window.location.href = url.base_url(Kontrak.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Kontrak.module()) + "index";
   }
  }
 },

 searchByClick: function (elm) {
  var keyWord = $('#keyword').val();
  if (keyWord != '') {
   Kontrak.setSessionKeyword();
//    window.location.href = url.base_url(Kontrak.module()) + "search" + '/' + keyWord;
  } else {
   window.location.href = url.base_url(Kontrak.module()) + "index";
  }
 },

 getPostItemPlan: function () {
  var tb_plan = $('table#tb_plan').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {
   var td = $(this).find('td').length;
   if (td > 2) {
    data.push({
     'id': $(this).attr('data_id'),
     'termin': $(this).find('td:eq(0)').find('input').val(),
     'fisik': $(this).find('td:eq(1)').find('input').val(),
     'bayar': $(this).find('td:eq(2)').find('input').val(),
     'tanggal': $(this).find('td:eq(3)').find('input').val(),
     'jumlah': $(this).find('td:eq(4)').find('input').val(),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });

  return data;
 },

 getPostItemKendala: function () {
  var tb_plan = $('table#tb_kendala').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {
   var td = $(this).find('td').length;
   if (td > 1) {
    data.push({
     'id': $(this).attr('data_id'),
     'kendala': $(this).find('td:eq(0)').find('input').val(),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });
  return data;
 },

 getPostItemKorlap: function () {
  var tb_plan = $('table#tb_korlap').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {
   var td = $(this).find('td').length;
   if (td > 1) {
    data.push({
     'id': $(this).attr('data_id'),
     'korlap': $(this).find('td:eq(0)').find('input').val(),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });
  return data;
 },

 getPostItemDirlap: function () {
  var tb_plan = $('table#tb_dirlap').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {

   var td = $(this).find('td').length;
   if (td > 1) {
    data.push({
     'id': $(this).attr('data_id'),
     'dirlap': $(this).find('td:eq(0)').find('input').val(),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });
  return data;
 },

 getPostItemLokasi: function () {
  var tb_plan = $('table#tb_lokasi').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {
   var td = $(this).find('td').length;
   if (td > 1) {
    data.push({
     'id': $(this).attr('data_id'),
     'lokasi': $(this).find('td:eq(0)').find('input').val(),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });
  return data;
 },

 getPostItemKurva: function () {
  var tb_plan = $('table#tb_kurva').find('tbody').find('tr');
  var data = [];
  $.each(tb_plan, function () {
   var td = $(this).find('td').length;
   if (td > 1) {
    data.push({
     'id': $(this).attr('data_id'),
     'bulan_ke': $(this).find('td:eq(0)').find('input').val(),
     'master': $(this).find('td:eq(1)').find('input').val(),
     'rencana': $(this).find('td:eq(2)').find('input').val(),
     'realisasi': $(this).find('td:eq(3)').find('input').val(),
     'ket_realisasi': $(this).find('td:eq(4)').find('textarea').val(),
     'is_amandemen': $(this).find('td:eq(5)').find('input#check_amandemen').is(':checked') ? 1 : 0,
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });
  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'form': {
    'upt': $('#upt').val(),
    'vendor': $('#vendor').val(),
    'no_kontrak': $('#no_kontrak').val(),
    'jenis': $('#jenis').val(),
    'tgl_kontrak': $('#tgl_kontrak').val(),
    'tgl_selesai_kontrak': $('#tgl_selesai_kontrak').val(),
    'tgl_reminder': $('#tgl_reminder').val(),
    'pekerjaan': $('#pekerjaan').val(),
    'harga_kontrak': $('#harga_kontrak').val(),
//    'keterangan': $('#keterangan').val(),
    'leader': $('input#leader').val()
   },
   'form_diprek': {
    'diprek': $('input#diprek').val(),
//    'ket_progress': $('#ket_progress').val(),
   },
   'form_plan': {
    'item': Kontrak.getPostItemPlan()
   },
   'form_kendala': {
    'item': Kontrak.getPostItemKendala()
   },
   'form_korlap': {
    'item': Kontrak.getPostItemKorlap()
   },
   'form_dirlap': {
    'item': Kontrak.getPostItemDirlap()
   },
   'form_kurva': {
    'item': Kontrak.getPostItemKurva()
   },
   'form_lokasi': {
    'item': Kontrak.getPostItemLokasi()
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Kontrak.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Kontrak.module()) + "simpan",
    error: function () {
     showDangerToast("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Kontrak.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      showDangerToast("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 selesai: function (id, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Kontrak.module()) + "selesai/" + id,
   error: function () {
    showDangerToast("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Selesai...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil");
     var reload = function () {
      window.location.href = url.base_url(Kontrak.module()) + "detail" + '/' + resp.id;
     };

     setTimeout(reload(), 1000);
    } else {
     showDangerToast("Gagal");
    }
    message.closeLoading();
   }
  });
 },

 ubah: function (id) {
  window.location.href = url.base_url(Kontrak.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Kontrak.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Kontrak.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Kontrak.module()) + "delete/" + id,

   error: function () {
    showDangerToast("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Kontrak.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     showDangerToast("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Kontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Kontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Kontrak.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Kontrak.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     showDangerToast('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    showDangerToast('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   showDangerToast('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Kontrak.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Kontrak.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Kontrak.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Kontrak.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tgl_kontrak').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tgl_selesai_kontrak').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
  $('input#tgl_reminder').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': false,
   'lengthChange': true,
   'searching': false,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },

 setSelect2: function () {
  $('select#upt').select2();
  $('select#vendor').select2();
  $('select#jenis').select2();
 },

 showDokumen: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    kontrak: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Kontrak.module()) + "showDokumen",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data Dokumen...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showDokumenData: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).attr('file'))
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Kontrak.module()) + "showDokumenData",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showProgressKontrak: function (id, e) {

  e.preventDefault();
  var url_name = Kontrak.module();
  var url_host = window.location.host;
  var url_protocol = window.location.protocol;
  var url_next = "showProgressKontrak/" + id;
  var url = url_protocol + '//' + url_host + '/' + Kontrak.moduleApp() + '/' + url_name + '/' + url_next;
  var w = screen.width - 300,
          h = 500;
  var left = (screen.width / 2) - (w / 2);
  var top = (screen.height / 2) - (h / 2);

  window.open(
          url,
          "_blank",
          "toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
          " width=" + w + ", height=" + h
          );
  return false;
 },

 showDialogReminder: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Kontrak.execKirimReminder(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execKirimReminder: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    kontrak: id
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Kontrak.module()) + "kirimReminder",
   error: function () {
    showDangerToast("Gagal");
    message.closeLoading();
   },
   beforeSend: function () {
    message.loadingProses("Proses Mengirim Reminder via Email Vendor...");
   },
   success: function (resp) {
    message.closeLoading();
    if (resp.is_valid) {
     toastr.success("Reminder Telah Terkirim");
    } else {
     showDangerToast("Reminder Gagal Terkirim");
    }
   }
  });
 },

 kirimReminder: function (id) {
  Kontrak.showDialogReminder(id);
 },

 addProgress: function (id) {
  window.location.href = url.base_url("progress_kontrak") + "add/" + id;
 },

 setThousandSparator: function () {
  $('#harga_kontrak').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 addItemDisbursment: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemDisbursment",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 addItemKurva: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemKurva",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    tr.attr('data_id', '');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
    $('#bulan_ke' + index).datepicker({
     format: 'yyyy-mm',
     autoclose: true,
     todayHighlight: true,
     orientation: 'bottom left'
    });
   }
  });
 },

 addItemKendala: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemKendala",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 addItemKorlap: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemKorlap",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 addItemDirlap: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemDirlap",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 addItemLokasi: function (elm, e) {
  e.preventDefault();
  var index = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "addItemLokasi",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Item...");
   },

   success: function (resp) {
    message.closeLoading();
    var tr = $(elm).closest('tr');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 removeItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('hidden');
   $(elm).closest('tr').addClass('deleted');
  }
 },

 setGrafikKurva: function () {
  // sale start  
 },

 showHistoryHarga: function (elm) {
  var id = $('input#id').val();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    id: id
   },
   async: false,
   url: url.base_url(Kontrak.module()) + "showHistoryHarga",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 setGrafikKurva: function () {
  var data_kurva = $('label#data_kurva').text();
  console.log(JSON.parse(data_kurva));
  var line = new Morris.Line({
   element: 'line-chart',
   resize: true,
//    data: [
//     {y: '2011 Q1', item1: 2666, item2: 2666},
//     {y: '2011 Q2', item1: 2778, item2: 2294},
//     {y: '2011 Q3', item1: 4912, item2: 1969},
//     {y: '2011 Q4', item1: 3767, item2: 3597},
//     {y: '2012 Q1', item1: 6810, item2: 1914},
//     {y: '2012 Q2', item1: 5670, item2: 4293},
//     {y: '2012 Q3', item1: 4820, item2: 3795},
//     {y: '2012 Q4', item1: 15073, item2: 5967},
//     {y: '2013 Q1', item1: 10687, item2: 4460},
//     {y: '2013 Q2', item1: 8432, item2: 5713}
//    ],
   data: JSON.parse(data_kurva),
   xkey: 'bulan_ke',
   ykeys: ['master', 'rencana', 'realisasi'],
   labels: ['Master', 'Rencana', 'Realisasi'],
   lineColors: ['#f5365c', '#3c8dbc', '#28a745'],
   hideHover: 'auto'
  });
 },

 setDataGrafikKurvaPlot: function () {
  //flot options
//  var ticks_data = [[0, "Jan"], [1, "Feb"]];
  var tick_data = $.trim($('#tick_data_kurva').text());
  var ticks_data = JSON.parse(tick_data.toString());

  var master = $.trim($('#master_data_kurva').text());
  var master_data = JSON.parse(master.toString());

  var rencana = $.trim($('#rencana_data_kurva').text());
  var rencana_data = JSON.parse(rencana.toString());

  var realisasi = $.trim($('#realisasi_data_kurva').text());
  var realisasi_data = JSON.parse(realisasi.toString());

  var ket_realisasi = $.trim($('#ket_realisasi_data_kurva').text());
  var ket_realisasi_data = JSON.parse(ket_realisasi.toString());


//  console.log("master data",master);

  var charDataMaster = {
   data: master_data,
   label: "Master",
   color: "#f5365c",
   lines: {
    show: true,
    fill: false,
    lineWidth: 2
   },
   points: {
    show: true,
    radius: 5,
    fill: true,
    fillColor: '#f5365c'
   },
   curvedLines: {
    apply: false,
   },
   axis: ticks_data
  };

  var charDataRencana = {
   data: rencana_data,
   label: "Rencana",
   color: "#3c8dbc",
   lines: {
    show: true,
    fill: false,
    lineWidth: 2
   },
   points: {
    show: true,
    radius: 5,
    fill: true,
    fillColor: '#3c8dbc'
   },
   curvedLines: {
    apply: false,
   },
   axis: ticks_data
  };

  var charDataRealisasi = {
   data: realisasi_data,
   label: "Realisasi",
   color: "#28a745",
   lines: {
    show: true,
    fill: false,
    lineWidth: 2
   },
   points: {
    show: true,
    radius: 5,
    fill: true,
    fillColor: '#28a745'
   },
   curvedLines: {
    apply: false,
   },
   axis: ticks_data
  };


  var options = {
   legend: {
    show: true
   },
   series: {
    label: "",
    curvedLines: {
     active: true,
     nrSplinePoints: 20
    },
   },
   tooltip: {
    show: true,
    content: function (label, xval, yval, flotItem) {
     var master_value = charDataMaster.data[xval][1];
     var rencana_value = charDataRencana.data[xval][1];
     var realisasi_value = charDataRealisasi.data[xval][1];
     var ket_realisasi = ket_realisasi_data[xval];


     var word = "<label style='color:#f5365c;'>" + charDataMaster.label + "</label>" + " " + master_value + "<br/>";
     word += "<label style='color:#3c8dbc;'>" + charDataRencana.label + "</label>" + " " + rencana_value + "<br/>";
     word += "<label style='color:#28a745;'>" + charDataRealisasi.label + "</label>" + " " + realisasi_value + "</br>";
     word += "<label>Keterangan : </label><br/>";
     word += "<label><b>" + ket_realisasi + "</b></label><br/>";


     return word;
    }
   },
   grid: {
    hoverable: true,
    borderWidth: 0,
    labelMargin: 0,
    axisMargin: 0,
    minBorderMargin: 0,
   },
   yaxis: {
    min: 0,
    max: parseInt($.trim($('#max_kurva').text())) + 10,
    color: '#28a745',
    font: {
     size: 12,
    },
   },
   xaxis: {
    color: '#28a745',
    font: {
     size: 12,
    },
    ticks: ticks_data
   },
  };

  $.plot($("#line-chart"), [
   charDataMaster,
   charDataRencana,
   charDataRealisasi
  ], options);
 },

 setDataGrafikKurvaPlotRencana: function () {
  var d2 = [[0, 3], [1, 8], [2, 5], [3, 13]];
  var chartData = {data: d2, axis: [[0, "One"], [1, "Two"], [2, "Three"], [3, "Four"]]};

  var somePlot = $.plot("#line-chart", [{label: "Rain in Spain", data: chartData.data, bars: {show: true}}],
          {
           xaxis: {
            ticks: chartData.axis
           },
           grid: {
            hoverable: true
           },
           tooltip: true,
           tooltipOpts: {
            content: function (label, xval, yval, flotItem) {
             return "Orders <b>" + yval + "</b> for <span>" + chartData.axis[xval][1] + "</span>"
            },
//            shifts: {
//             x: -30,
//             y: -50
//            }
           }
          }
  );
 },

 setDatePickerBulanKe: function () {
  var bulan_ke = $('.bulan_ke');
  $.each(bulan_ke, function () {
   var id_bulan = $(this).attr('id');
   $('#' + id_bulan).datepicker({
    format: 'yyyy-mm',
    autoclose: true,
    todayHighlight: true,
    viewMode: "months",
    minViewMode: "months",
    orientation: 'bottom left'
   });
  });
 },

 caridBerdasarkan: function (elm) {
  var berdasarkan = $.trim($(elm).val()) == '' ? 'Pencarian' : $.trim($(elm).val());
  $('#keyword').attr('placeholder', berdasarkan);
  $('#keyword').removeAttr('readonly', '');
  $('#keyword').removeClass('hasDatepicker');
  $('#keyword').removeData('datepicker');
  $('#keyword').unbind();
  $('#keyword').val('');

  if (berdasarkan == 'TANGGAL KONTRAK') {
   $('#keyword').attr('readonly', '');
   $('#keyword').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true,
    orientation: 'bottom left'
   });
  }
 }
};

$(function () {

 var url_host = window.location.href;
 var data_url = url_host.split('/');
 console.log(data_url.length);
 if (data_url.length == 7) {
  var fungsi = $.trim(data_url[5]);
  if (fungsi.toString() == "showProgressKontrak") {
   Kontrak.setDataGrafikKurvaPlot();
  }
 }
 Kontrak.setDatePickerBulanKe();
 Kontrak.setThousandSparator();
 Kontrak.setDate();
 Kontrak.setSelect2();
// Kontrak.setDataTable();
});
