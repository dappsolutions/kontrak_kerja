<?php

class Lapkontrak extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'lapkontrak';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/stickyTable.min.css">',
      '<script src="' . base_url() . 'assets/js/stickyTable.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/lapkontrak.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kontrak';
 }

 public function getRootModule() {
  return "Laporan";
 }

 public function index() {
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Kontrak Kerja";
  $data['title_content'] = 'Kontrak Kerja';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataLapkontrak();
  $data['content'] = $content;
  echo Modules::run('template', $data);
 }

 public function getDataLapkontrak($keyword = '') {
  $where = "where k.deleted = 0";
  if ($this->akses == 'admin') {
   $where = "where k.deleted = 0 and ut.id = '" . $this->upt . "'";
  }
  $sql = "	select 
  k.id
		, k.no_kontrak
		, ut.nama_upt
		, k.tanggal_kontrak
  , k.pekerjaan
		, k.tanggal_selesai_kontrak
		, k.harga_kontrak
		, v.nama_vendor as pelaksana
		, k.leader as leader
		, v.alamat 
		, v.no_hp
		, jp.jenis
		, pk.lokasi as lokasi_pekerjaan
		, p.no_progress
		, ka.no_amandemen
		, ka.uraian
		, ka.tanggal_mulai
		, ka.tanggal_selesai
		, kpf.kurva_before
		, kpf.realisasi_before
		, kpf.kurva_after
		, kpf.realisasi_after
		, kc.target
		, kc.realisasi
		, kp.jumlah_termin as termin_ke
		, kp.fisik
		, kp.bayar
		, kp.tanggal
		, kp.jumlah
		, kb.tanggal_ba
		, kb.nilai
		, kb.persentase_bayar
		, kb.denda
		, kb1.nomor as nomor_kb1		
		, kb1.tanggal as tanggal_kb1
		, kb2.nomor as nomor_kb2		
		, kb2.tanggal as tanggal_kb2
  , pk.diprek
		, pk.dirlap
		, pk.korlap
		, pk.kendala
		from kontrak k
		join upt ut
			on ut.id = k.upt
		JOIN vendor v
			on v.id = k.vendor
		join jenis_pekerjaan jp
			on jp.id = k.jenis_pekerjaan
		join progress_kontrak pk
			on pk.kontrak = k.id
		left join progress p
			on p.progress_kontrak = pk.id and p.deleted = 0
		left join kontrak_amandemen ka
			on ka.progress = p.id
		left join kontrak_progress_fisik kpf
			on kpf.progress = p.id
		left join kontrak_cod kc
			on kc.progress = p.id
		left join kontrak_bayar kb
			on kb.progress = p.id
		left join kontrak_bast1 kb1
			on kb1.progress = p.id
		left join kontrak_bast2 kb2
			on kb2.progress = p.id
  left join kontrak_plan kp
   on kp.id = kb.kontrak_plan
		" . $where . "
  order by ut.nama_upt
	";

  $data = Modules::run('database/get_custom', $sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   $data_result = $data->result_array();

   $temp_upt = array();
   foreach ($data_result as $value) {
    $upt = $value['nama_upt'];
    if (!in_array($upt, $temp_upt)) {
     $detail = array();
     $temp_kontrak = array();
     foreach ($data_result as $v_detail) {
      $kontrak = $v_detail['no_kontrak'];
      if ($upt == $v_detail['nama_upt']) {
       $detail_kontrak = array();
       foreach ($data_result as $v_kontrak) {
        if ($kontrak == $v_kontrak['no_kontrak']) {

         $dirlap = $this->getDirlapData($v_kontrak['id']);
         $korlap = $this->getKorlapData($v_kontrak['id']);
         $kendala = $this->getKendalaData($v_kontrak['id']);

         $v_kontrak['dirlap'] = $dirlap;
         $v_kontrak['korlap'] = $korlap;
         $v_kontrak['kendala'] = $kendala;
         array_push($detail_kontrak, $v_kontrak);
        }
       }
       $detail[$kontrak] = $detail_kontrak;
      }
     }


     $result[$upt] = $detail;
     $temp_upt[] = $upt;
    }
   }
  }

  return $result;
 }

 public function getDirlapData($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_dirlap kd',
              'field' => array('kd.*'),
              'where' => "kd.deleted = 0 and kd.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value['dirlap']);
   }
  }


  return implode('<br/>', $result);
 }

 public function getKorlapData($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_korlap kd',
              'field' => array('kd.*'),
              'where' => "kd.deleted = 0 and kd.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value['korlap']);
   }
  }


  return implode('<br/>', $result);
 }

 public function getKendalaData($kontrak) {
  $data = Modules::run('database/get', array(
              'table' => 'kontrak_kendala kd',
              'field' => array('kd.*'),
              'where' => "kd.deleted = 0 and kd.kontrak = '" . $kontrak . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value['kendala']);
   }
  }


  return implode('<br/>', $result);
 }

 public function getDataLapkontrakSearch() {
  $tgl_awal = $_POST['tgl_awal'];
  $tgl_akhir = $_POST['tgl_akhir'];

  $where = "(k.tanggal_kontrak >= '" . $tgl_awal . "'  and k.tanggal_kontrak <= '" . $tgl_akhir . "')";

  if ($this->akses == "admin") {
   $where = "(k.tanggal_kontrak >= '" . $tgl_awal . "'  and k.tanggal_kontrak <= '" . $tgl_akhir . "') and ut.id = '" . $this->upt . "'";
  }
  $sql = "	select 
   k.id
		, k.no_kontrak
		, ut.nama_upt
		, k.tanggal_kontrak
  , k.pekerjaan
		, k.tanggal_selesai_kontrak
		, k.harga_kontrak
		, v.nama_vendor as pelaksana
		, k.leader as leader
		, v.alamat 
		, v.no_hp
		, jp.jenis
		, pk.lokasi as lokasi_pekerjaan
		, p.no_progress
		, ka.no_amandemen
		, ka.uraian
		, ka.tanggal_mulai
		, ka.tanggal_selesai
		, kpf.kurva_before
		, kpf.realisasi_before
		, kpf.kurva_after
		, kpf.realisasi_after
		, kc.target
		, kc.realisasi
		, kp.jumlah_termin as termin_ke
		, kp.fisik
		, kp.bayar
		, kp.tanggal
		, kp.jumlah
		, kb.tanggal_ba
		, kb.nilai
		, kb.persentase_bayar
		, kb.denda
		, kb1.nomor as nomor_kb1		
		, kb1.tanggal as tanggal_kb1
		, kb2.nomor as nomor_kb2		
		, kb2.tanggal as tanggal_kb2
  , pk.diprek
		, pk.dirlap
		, pk.korlap
		, pk.kendala
		from kontrak k
		join upt ut
			on ut.id = k.upt
		JOIN vendor v
			on v.id = k.vendor
		join jenis_pekerjaan jp
			on jp.id = k.jenis_pekerjaan
		join progress_kontrak pk
			on pk.kontrak = k.id
		left join progress p
			on p.progress_kontrak = pk.id and p.deleted = 0
		left join kontrak_amandemen ka
			on ka.progress = p.id
		left join kontrak_progress_fisik kpf
			on kpf.progress = p.id
		left join kontrak_cod kc
			on kc.progress = p.id
		left join kontrak_bayar kb
			on kb.progress = p.id
		left join kontrak_bast1 kb1
			on kb1.progress = p.id
		left join kontrak_bast2 kb2
			on kb2.progress = p.id
  left join kontrak_plan kp
   on kp.id = kb.kontrak_plan
		where k.deleted = 0 and " . $where . "
  order by ut.nama_upt
	";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   $data_result = $data->result_array();

   $temp_upt = array();
   foreach ($data_result as $value) {
    $upt = $value['nama_upt'];
    if (!in_array($upt, $temp_upt)) {
     $detail = array();
     $temp_kontrak = array();
     foreach ($data_result as $v_detail) {
      $kontrak = $v_detail['no_kontrak'];
      if ($upt == $v_detail['nama_upt']) {
       $detail_kontrak = array();
       foreach ($data_result as $v_kontrak) {
        if ($kontrak == $v_kontrak['no_kontrak']) {
         $dirlap = $this->getDirlapData($v_kontrak['id']);
         $korlap = $this->getKorlapData($v_kontrak['id']);
         $kendala = $this->getKendalaData($v_kontrak['id']);

         $v_kontrak['dirlap'] = $dirlap;
         $v_kontrak['korlap'] = $korlap;
         $v_kontrak['kendala'] = $kendala;
         array_push($detail_kontrak, $v_kontrak);
        }
       }
       $detail[$kontrak] = $detail_kontrak;
      }
     }


     $result[$upt] = $detail;
     $temp_upt[] = $upt;
    }
   }
  }

  return $result;
 }

 public function tampilkan() {
  $data_kontrak = $this->getDataLapkontrakSearch();
  $data['content'] = $data_kontrak;

  echo $this->load->view('table_laporan', $data, true);
 }

}
