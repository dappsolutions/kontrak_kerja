<?php

class Surat_reminder extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt');
 }

 public function getModuleName() {
  return 'surat_reminder';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/surat_reminder_v1.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'surat_reminder';
 }

 public function getRootModule() {
  return "Pekerjaan";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Reminder";
  $data['title_content'] = 'Reminder';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataReminder();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['akses'] = $this->akses;
  echo Modules::run('template', $data);
 }

 public function getTotalDataReminder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.tanggal_kirim', $keyword),
       array('k.no_kontrak', $keyword),
   );
  }


  $where = "t.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'k.no_kontrak'),
                'join' => array(
                    array('kontrak k', 'k.id = t.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'k.no_kontrak'),
                'join' => array(
                    array('kontrak k', 'k.id = t.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataReminder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.tanggal_kirim', $keyword),
       array('k.no_kontrak', $keyword),
   );
  }

  $where = "t.deleted = 0 and u.id = '" . $this->upt . "'";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'k.no_kontrak'),
                'join' => array(
                    array('kontrak k', 'k.id = t.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'k.no_kontrak'),
                'join' => array(
                    array('kontrak k', 'k.id = t.kontrak'),
                    array('upt u', 'k.upt = u.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataReminder($keyword)
  );
 }

 public function getDetailDataReminder($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'k.no_kontrak'),
              'join' => array(
                  array('kontrak k', 't.kontrak = k.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListKontrak() {
  $join = array(
      array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
      array('kontrak_status ks', 'kss.id = ks.id'),
  );
  $where = "k.deleted = 0";
  if ($this->akses != 'superadmin') {
   $join = array(
       array('(select max(id) id, kontrak from kontrak_status group by kontrak) kss', 'kss.kontrak = k.id'),
       array('kontrak_status ks', 'kss.id = ks.id'),
       array('upt u', 'k.upt = u.id'),
   );
   $where = "k.deleted = 0 and u.id = '" . $this->upt . "'";
  }

  $data = Modules::run('database/get', array(
              'table' => 'kontrak k',
              'field' => array('k.*'),
              'join' => $join,
//              'where' => "k.deleted = 0 and ks.status != 'DONE'"
              'where' => $where
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'Reminder ';
  $data['title'] = "Tambah Reminder";
  $data['title_content'] = 'Tambah Reminder';
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataReminder($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Reminder";
  $data['root_module'] = 'Reminder ';
  $data['title_content'] = 'Ubah Reminder';
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataReminder($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Reminder";
  $data['root_module'] = 'Reminder ';
  $data['title_content'] = "Detail Reminder";
  $data['list_kontrak'] = $this->getListKontrak();
  echo Modules::run('template', $data);
 }

 public function getPostDataReminder($value) {
  $data['kontrak'] = $value->kontrak;
  $data['tanggal_kirim'] = $value->tanggal_kirim;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $file = $_FILES;
  $is_save = true;
  $is_valid = false;
//  echo '<pre>';
//  print_r($file);die;
  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataReminder($data->form);
   if ($id == '') {
    if (!empty($file)) {
     $response_upload = $this->uploadData('file');
     if ($response_upload['is_valid']) {
      list($kategori, $tipe) = explode('/', $file['file']['type']);
      $post['file'] = $file['file']['name'];
      $post['tipe'] = $tipe;
     } else {
      $is_save = false;
      $message = $response_upload['response'];
     }
    }
    if ($is_save) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($file)) {
     if ($data->form->file_str != $file['file']['name']) {
      $response_upload = $this->uploadData('file');
      if ($response_upload['is_valid']) {
       list($kategori, $tipe) = explode('/', $file['file']['type']);
       $post['file'] = $file['file']['name'];
       $post['tipe'] = $tipe;
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }
    if ($is_save) {
     Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 
 public function setSessionKeyword(){
	$keyword = $_POST['keyWord'];

	$this->session->set_userdata(array(
		'keyword'=> $keyword
	));

	echo 1;
 }
 
 public function search() {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = $this->session->userdata('keyword');
//  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Reminder";
  $data['title_content'] = 'Data Reminder';
  $content = $this->getDataReminder($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $data['akses'] = $this->akses;
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/surat_reminder/';
  $config['allowed_types'] = 'png|jpg|jpeg|pdf';
  $config['max_size'] = '13000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }
 
 public function kirimSurat() {
  $kontrak = $_POST['kontrak'];
  $id = $_POST['id'];
  $data_kontrak = Modules::run('kontrak/getDetailDataKontrak', $kontrak);

  $surat_reminder = $this->getDetailDataReminder($id);
  
  $file = str_replace(' ', '_', $surat_reminder['file']);
  $message = "Surat Reminder Pekerjaan Vendor";
  $send_email = Modules::run('email/send_surat_reminder',
                  $data_kontrak['email_vendor'],
                  $message, $file);

  $is_valid = false;
  if ($send_email) {
   $is_valid = true;
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
