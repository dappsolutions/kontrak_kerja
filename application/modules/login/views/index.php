<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Login | Aplikasi - SILAKON</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="<?php echo base_url() ?>assets/images/logo-pln.png" type="image/x-icon" />

  <!--<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">-->

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/ionicons/dist/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/icon-kit/dist/css/iconkit.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/themekit/dist/css/theme.min.css">
  <script src="<?php echo base_url() ?>assets/themekit/src/js/vendor/modernizr-2.8.3.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
 </head>

 <body>
  <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <div class="auth-wrapper">
   <div class="container-fluid h-100">
    <div class="row flex-row h-100 bg-white">
     <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
      <div class="lavalite-bg" style="background-image: url('<?php echo base_url() ?>assets/themekit/img/auth/sutet.jpg')">
       <!--<div class="lavalite-overlay"></div>-->
      </div>
     </div>
     <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
      <div class="authentication-form mx-auto">
       <div class="logo-centered">
        <!--<a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/images/logo-pln.png" alt="" height="70" width="50"></a>-->        
        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/images/users/images.png" alt="" height="70" width="70"></a>        
       </div>       
       <h3>Sign In to Appication</h3>
       <p>Aplikasi Silakon (Sistem Informasi Laporan Konstruksi)</p>
       <form action="<?php echo base_url() ?>">
        <div class="form-group">
         <input type="text" class="form-control required" error="Username" placeholder="Username" value="" id="username">
         <i class="ik ik-user"></i>
        </div>
        <div class="form-group">
         <input type="password" class="form-control required" error="Password" placeholder="Password" value="" id="password">
         <i class="ik ik-lock"></i>
        </div>
        <div class="sign-btn text-center">
         <button class="btn" style="background-color: #00a2b9 !important;color:white;" onclick="Login.sign_in(this, event)">Sign In</button>
         &nbsp;
         <a class='btn btn-warning' href="<?php echo base_url().'files/berkas/manual_book.pdf' ?>">Manual Book</a>
        </div>
       </form>
<!--       <div class="register">
        <p>Don't have an account? <a href="register.html">Create an account</a></p>
       </div>-->
      </div>
     </div>
    </div>
   </div>
  </div>

  <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
  <script>window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/themekit/src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
  <script src="<?php echo base_url() ?>assets/themekit/plugins/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/themekit/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>assets/themekit/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <script src="<?php echo base_url() ?>assets/themekit/plugins/screenfull/dist/screenfull.js"></script>
  <script src="<?php echo base_url() ?>assets/themekit/dist/js/theme.js"></script>
  <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
  <script src="<?php echo base_url() ?>assets/js/url.js"></script>
  <script src="<?php echo base_url() ?>assets/js/message.js"></script>
  <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/controllers/login.js"></script>
  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--  <script>
   (function (b, o, i, l, e, r) {
    b.GoogleAnalyticsObject = l;
    b[l] || (b[l] =
            function () {
             (b[l].q = b[l].q || []).push(arguments)
            });
    b[l].l = +new Date;
    e = o.createElement(i);
    r = o.getElementsByTagName(i)[0];
    e.src = 'https://www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e, r)
   }(window, document, 'script', 'ga'));
   ga('create', 'UA-XXXXX-X', 'auto');
   ga('send', 'pageview');
  </script>-->
 </body>
</html>
