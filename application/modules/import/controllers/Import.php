<?php

class Import extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'import';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/import.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'probis';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Data";
  $data['title_content'] = 'Import Data';
  echo Modules::run('template', $data);
 }

 public function getDataUptId($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'upt ut',
              'field' => array('ut.*'),
              'where' => "ut.deleted = 0 and ut.nama_upt = '" . $upt . "'"
  ));

  $upt_id = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $upt_id = $data['id'];
  } else {
   $data_upt['nama_upt'] = trim($upt);
   $upt_id = Modules::run('database/_insert', 'upt', $data_upt);
  }


  return $upt_id;
 }

 public function importFileSd() {
  $data = json_decode($this->input->post('data'));

//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $params['nama_sekolah'] = trim($value[1]);
     $params['kategori'] = 1;
     if (strtoupper(trim($value[2])) == 'NEGERI') {
      $params['kategori'] = 2;
     }
     $params['akreditasi'] = trim($value[3]);
     $params['kecamatan'] = trim($value[4]);
     $params['alamat'] = trim($value[5]);
     $params['latitude'] = trim($value[6]);
     $params['longitude'] = trim($value[7]);
     $params['no_telp'] = trim($value[8]);
     $params['email'] = trim($value[9]);
     $params['website'] = trim($value[10]);
     $params['jam_operasional'] = trim($value[11]);
     $params['jumlah_murid'] = trim($value[12]);
     $params['jumlah_pengajar'] = trim($value[13]);

     $sekolah = Modules::run('database/_insert', 'sekolah', $params);

     if (trim($value[14]) != '') {
      $image = explode('&&', trim($value[14]));
      if (!empty($image)) {
       foreach ($image as $v_img) {
        if ($v_img != '') {
         $img_data['sekolah'] = $sekolah;
         $img_data['file'] = $v_img;
         Modules::run('database/_insert', 'sekolah_picture', $img_data);
        }
       }
      }
     }
     array_push($result, $value);
    }
   }
   
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFile() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $result = array();

//  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    $temp_jalur = "";
    $temp_alamat = "";
    foreach ($data as $value) {
//     if (trim($value[0]) != '' && is_numeric($value[0])) {
     $nama_jalur = $temp_jalur == trim($value[1]) ? $temp_jalur : trim($value[1]);
     if ($nama_jalur == '') {
      $nama_jalur = $temp_jalur;
     }

     $insert['jenis'] = trim($nama_jalur);
     $data = Modules::run('database/get', array(
                 'table' => 'jenis_angkot ja',
                 'field' => array('ja.*'),
                 'where' => "ja.deleted = 0 and ja.jenis = '" . trim($nama_jalur) . "'"
     ));

     $jenis = 0;
     if (!empty($data)) {
      $jenis = $data->row_array()['id'];
     } else {
      $jenis = Modules::run('database/_insert', 'jenis_angkot', $insert);
     }

     $insert_jalur['jenis_angkot'] = $jenis;
     $alamat = $temp_alamat == trim($value[2]) ? $temp_alamat : trim($value[2]);
     $insert_jalur['alamat'] = $alamat == '' ? $temp_alamat : $alamat;
     $insert_jalur['latitude'] = trim($value[4]);
     $insert_jalur['longitude'] = trim($value[3]);
     Modules::run('database/_insert', 'jalur_angkot', $insert_jalur);

     if (trim($value[1]) != '') {
      $temp_jalur = trim($value[1]);
     }

     if (trim($value[2]) != '') {
      $temp_alamat = trim($value[2]);
     }
     array_push($result, $value);
//     }
    }
   }
//   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileGedung() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     if (trim($value[0]) != '' && is_numeric($value[0])) {
      $insert_probis['nama'] = trim($value[1]);
      $insert_probis['kontak'] = trim($value[2]);
      $insert_probis['address'] = trim($value[3]);
      $insert_probis['kecamatan'] = trim($value[4]);
      $insert_probis['kelurahan'] = trim($value[5]);
      $insert_probis['harga_weekday'] = trim($value[6]);
      $insert_probis['kapasitas'] = trim($value[7]);
      $insert_probis['latitude'] = trim($value[8]);
      $insert_probis['longitude'] = trim($value[9]);
      $insert_probis['video'] = trim($value[10]);

      $gedung = Modules::run('database/_insert', "gedung", $insert_probis);

      if (trim($value[11]) != '') {
       $data_fasilitas = explode('[&&]', trim($value[11]));
       foreach ($data_fasilitas as $v_s) {
        $fas['gedung'] = $gedung;
        $fas['fasilitas'] = $v_s;
        Modules::run('database/_insert', 'gedung_fasilitas', $fas);
       }
      }

      if (trim($value[12]) != '') {
       $data_fasilitas = explode('[&&]', trim($value[12]));
       foreach ($data_fasilitas as $v_s) {
        $gd['gedung'] = $gedung;
        $gd['file'] = $v_s;
        Modules::run('database/_insert', 'gedung_picture', $gd);
       }
      }
      array_push($result, $value);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function importFileProbis() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $result = array();

  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     if (trim($value[0]) != '' && is_numeric($value[0])) {
//      echo '<pre>';
//      print_r($value);die;
      $insert_probis['upt'] = $this->getDataUptId(trim($value[3]));
      $insert_probis['nama_probis'] = trim($value[4]);
      if (trim($value[2] != '')) {
       $insert_probis['no_probis'] = trim($value[2]);
      } else {
       $insert_probis['no_probis'] = Modules::run('no_generator/generateNoProbis');
      }
      $insert_probis['user'] = $this->session->userdata('user_id');

      if (trim($value[7]) != '') {
       $insert_probis['tgl_update'] = date('Y-m-d', strtotime(trim($value[7])));
      }

      if (trim($value[6]) != '') {
       $insert_probis['tahun'] = trim($value[6]);
      }
      if (trim($value[8]) != '') {
       $insert_probis['edisi'] = trim($value[8]);
      }
      if (trim($value[9]) != '') {
       $insert_probis['revisi'] = trim($value[9]);
      }
      $probis = Modules::run('database/_insert', $this->getTableName(), $insert_probis);
      $this->insertKeywordProbis($probis, $value);

      $this->insertPathProbis($probis, $value);
      //insert status
      if (trim($value[10]) == '') {
       $value[10] = 'ONCOMING';
      }
      if (trim($value[10]) == 'proses') {
       $value[10] = 'PROGRESS';
      }
      $status['status'] = strtoupper(trim($value[10]));
      $status['probis'] = $probis;

      if (trim($value[12]) != '') {
       $status['keterangan'] = trim($value[12]);
      }
      Modules::run('database/_insert', 'probis_status', $status);
      array_push($result, $value);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $is_valid = false;
   $this->db->trans_rollback();
  }

  $content['result'] = $result;
  $view = $this->load->view('table_data', $content, true);
  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function insertPathProbis($probis, $value) {
  $data_path = explode('.', $value[1]);
  if (!empty($data_path)) {
   $insert['probis'] = $probis;
   $insert['parent'] = isset($data_path[0]) ? trim($data_path[0]) : 0;
   $insert['child_first'] = isset($data_path[1]) ? trim($data_path[1]) : 0;
   $insert['child_second'] = isset($data_path[2]) ? trim($data_path[2]) : 0;
   $insert['child_third'] = isset($data_path[3]) ? trim($data_path[3]) : 0;
   Modules::run('database/_insert', 'path_probis', $insert);
  }
 }

 public function insertKeywordProbis($probis, $value) {
  $data_keyword = explode('[&&]', $value[5]);
  if (!empty($data_keyword)) {
   foreach ($data_keyword as $v) {
    $insert['probis'] = $probis;
    $insert['keyword'] = trim($v);
    Modules::run('database/_insert', 'keyword_probis', $insert);
   }
  }
 }

}
