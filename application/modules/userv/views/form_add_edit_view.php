<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">     
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Vendor</label>
      <div class="col-sm-9">
       <select class="form-control required" error="Vendor" id="vendor">
        <option value="">Pilih Vendor</option>
        <?php if (!empty($list_vendor)) { ?>
         <?php foreach ($list_vendor as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($vendor)) { ?>
           <?php $selected = $vendor == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_vendor'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Username</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Username" id="username" 
              placeholder="Username" value="<?php echo isset($username) ? $username : '' ?>">
      </div>
     </div>
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Password</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Password" id="password" 
              placeholder="Password" value="<?php echo isset($password) ? $password : '' ?>">
      </div>
     </div>
     <div class="text-right">
      <button type="submit" class="btn btn-primary mr-2" onclick="Userv.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
      <a href="<?php echo base_url() . $module ?>">Batal</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
