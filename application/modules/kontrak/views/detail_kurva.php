<div class="table-responsive">
 <table class="table table-bordered" id="tb_kurva">
  <thead>
   <tr>
    <th>Bulan Ke</th>
    <th>Master (%)</th>
    <th>Rencana (%)</th>
    <th>Realisasi (Rp)</th>
    <th>Keterangan Realisasi</th>
    <th class="text-center">Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_kurva)) { ?>
    <?php foreach ($list_kurva as $value) { ?>
     <tr class="<?php echo $value['is_amandemen'] == '1' ? 'bg-success' : '' ?>" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['bulan_ke'] ?>" placeholder="Bulan Ke" id="bulan_ke" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['master'] ?>" placeholder="Master (%)" id="master" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['rencana'] ?>" placeholder="Rencana (%)" id="rencana" class="form-control" />
      </td>
      <td>
       <input disabled type="text" value="<?php echo $value['realisasi'] ?>" placeholder="Realisasi" id="realisasi" class="form-control" />
      </td>
      <td>
       <textarea disabled id="keterangan_realisasi" class="form-control"><?php echo $value['ket_realisasi'] ?></textarea>
      </td>
      <td class="text-center">
       <input disabled type="checkbox" <?php echo $value['is_amandemen'] == '1' ? 'checked' : '' ?> value="" id="check_amandemen" class="" />
       Amandemen
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>