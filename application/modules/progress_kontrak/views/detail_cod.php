<form class="forms-sample" method="post">  

 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Target</label>
  <div class="col-sm-9">
   <input disabled type="text" class="form-control required text-right" 
          error="Tanggal Target" id="target" 
          placeholder="Tanggal Target" value="<?php echo isset($target) ? $target : '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Tanggal Realisasi</label>
  <div class="col-sm-9">
   <input disabled  type="text" class="form-control required text-right" 
          error="Tanggal Realisasi" id="realisasi" 
          placeholder="Tanggal Realisasi" value="<?php echo isset($realisasi) ? $realisasi: '' ?>">
  </div>
 </div>
 <div class="form-group row">
  <label for="" class="col-sm-3 col-form-label">Kendala / Keterangan</label>
  <div class="col-sm-9">
   <textarea disabled class="form-control" 
             error="Keterangan" id="ket_cod" 
             placeholder="Keterangan"><?php echo isset($ket_cod) ? $ket_cod : '' ?></textarea>
  </div>
 </div>
</form>