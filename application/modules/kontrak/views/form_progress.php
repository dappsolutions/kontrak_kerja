<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Dirpek</label>
 <div class="col-sm-9">
  <input type="text" class="form-control required" 
         error="Diprek" id="diprek" 
         placeholder="Diprek" value="<?php echo isset($diprek) ? $diprek : '' ?>">
 </div>
</div>

<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Leader</label>
 <div class="col-sm-9">
  <input type="text" class="form-control required" 
         error="Leader" id="leader" 
         placeholder="Leader" value="<?php echo isset($leader) ? $leader : '' ?>">
 </div>
</div>
<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Tanggal Kontrak</label>
 <div class="col-sm-9">
  <input readonly="" type="text" class="form-control required" 
         error="Tanggal Kontrak" id="tgl_kontrak" 
         placeholder="Tanggal Kontrak" value="<?php echo isset($tanggal_kontrak) ? $tanggal_kontrak : '' ?>">
 </div>
</div>
<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Tanggal Selesai Kontrak</label>
 <div class="col-sm-9">
  <input readonly="" type="text" class="form-control required" 
         error="Tanggal Selesai Kontrak" id="tgl_selesai_kontrak" 
         placeholder="Tanggal Selesai Kontrak" value="<?php echo isset($tanggal_selesai_kontrak) ? $tanggal_selesai_kontrak : '' ?>">
 </div>
</div>
<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Tanggal Reminder</label>
 <div class="col-sm-9">
  <input readonly="" type="text" class="form-control required" 
         error="Tanggal Reminder" id="tgl_reminder" 
         placeholder="Tanggal Reminder" value="<?php echo isset($reminder_date) ? $reminder_date : '' ?>">
 </div>
</div>

<!--<div class="form-group row">
 <label for="" class="col-sm-3 col-form-label">Keterangan</label>
 <div class="col-sm-9">
  <textarea class="form-control required" 
            error="Keterangan" id="ket_progress" 
            placeholder="Keterangan"><?php echo isset($ket_progress) ? $ket_progress : '' ?></textarea>
 </div>
</div>-->
