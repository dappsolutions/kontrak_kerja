<header class="header-top color-pln" header-theme="light">
 <div class="container-fluid">
  <div class="d-flex justify-content-between">
   <div class="top-menu d-flex align-items-center">
    <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>   
    <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
   </div>
   <div class="top-menu d-flex align-items-center">
    <div class="dropdown">
     <a class="nav-link dropdown-toggle" href="#" id="notiDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-bell"></i><span class="badge bg-danger">0</span></a>
     <div class="dropdown-menu dropdown-menu-right notification-dropdown" aria-labelledby="notiDropdown">
      <h4 class="header">Pemberitahuan</h4>
      <div class="notifications-wrap">
       <a href="#" class="media">
        <span class="d-flex">
         <i class="ik ik-file-text"></i> 
        </span>
        <span class="media-body">
         <span class="heading-font-family media-heading">Kontrak Pekerjaan</span> 
         <span class="media-content">Tidak ada pemberitahuan</span>
        </span>
       </a>
<!--       <a href="#" class="media">
        <span class="d-flex">
         <img src="<?php echo base_url() ?>assets/images/image.png" class="rounded-circle" alt="">
        </span>
        <span class="media-body">
         <span class="heading-font-family media-heading">Steve Smith</span> 
         <span class="media-content">I slowly updated projects</span>
        </span>
       </a>
       <a href="#" class="media">
        <span class="d-flex">
         <i class="ik ik-calendar"></i> 
        </span>
        <span class="media-body">
         <span class="heading-font-family media-heading">To Do</span> 
         <span class="media-content">Meeting with Nathan on Friday 8 AM ...</span>
        </span>
       </a>-->
      </div>
<!--      <div class="footer"><a href="javascript:void(0);">See all activity</a></div>-->
     </div>
    </div>    
    <div class="dropdown">
     <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="<?php echo base_url() ?>assets/images/users/images.png" alt=""></a>
     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
      <a class="dropdown-item" href="#" onclick="Template.changePassword(this, event)"><i class="ik ik-settings dropdown-icon"></i> Ganti Password</a>
      <a class="dropdown-item" href="<?php echo base_url() . 'login/sign_out' ?>"><i class="ik ik-power dropdown-icon"></i> Logout</a>
     </div>
    </div>

   </div>
  </div>
 </div>
</header>