<?php echo $this->load->view('header'); ?>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="card">
 <div class="card-header"><h3>Form</h3></div>
 <div class="card-body">
  <div class="row">
   <div class="col-md-6">
    <form class="forms-sample" method="post">
     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Nomor Kontrak</label>
      <div class="col-sm-9">
       <select class="form-control required" error="Nomor Kontrak" id="no_kontrak">
        <option value="">Pilih Kontrak</option>
        <?php if (!empty($list_kontrak)) { ?>
         <?php foreach ($list_kontrak as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($kontrak)) { ?>
           <?php $selected = $kontrak == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_kontrak'] ?></option> 
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">Tanggal Kirim</label>
      <div class="col-sm-9">
       <input type="text" class="form-control required" 
              error="Tanggal Kirim" id="tanggal_kirim" 
              placeholder="Tanggal Kirim" value="<?php echo isset($tanggal_kirim) ? $tanggal_kirim : date('Y-m-d') ?>">
      </div>
     </div>

     <div class="form-group row">
      <label for="" class="col-sm-3 col-form-label">File Upload</label>
      <input type="file" name="file" class="file-upload-default" id="file" onchange="SuratReminder.getFilename(this)">
      <div class="col-sm-9">
       <div class="input-group">
        <input disabled type="text" class="form-control file-upload-info" id="file_str" 
               disabled="" placeholder="Upload Image" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-append">         
         <?php $hide = isset($file) ? 'hidden-content' : '' ?>
         <button class="file-upload-browse <?php echo $hide ?> btn btn-primary" type="button" onclick="SuratReminder.upload(this)" id="btn-upload">Upload</button>
         <?php if (isset($file)) { ?>
          <button class="file-upload-browse  btn btn-primary" type="button" onclick="SuratReminder.removeUpload(this)" id="btn-remove">
           <i class="ik ik-trash"></i>
          </button>
         <?php } ?>
        </span>
       </div>
      </div>
     </div>
     <div class="text-right">
      <button type="submit" class="btn btn-primary mr-2" onclick="SuratReminder.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Proses</button>
      <a href="<?php echo base_url() . $module ?>">Batal</a>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
