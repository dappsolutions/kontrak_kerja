<?php echo $this->load->view('header'); ?>


<div class="card">
 <div class="card-header row">
  <div class="col col-sm-3">
   <div class="card-options d-inline-block">
    <?php if (($akses == "superadmin" || $akses == "admin")) { ?>
     <button type="button" class="btn btn-success" onclick="Kontrak.add()">Tambah</button>
    <?php } ?>
   </div>
  </div>
  <div class="col col-sm-3">
   <select class="form-control" id="pencarian" onchange="Kontrak.caridBerdasarkan(this)">
    <option value="">Cari Berdasarkan</option>
    <option value="NO KONTRAK">NO KONTRAK</option>
    <option value="TANGGAL KONTRAK">TANGGAL KONTRAK</option>
   </select>
  </div>
  <div class="col col-sm-6">
   <div class="card-search with-adv-search dropdown">
    <input type="text" class="form-control" id="keyword" 
           placeholder="Pencarian" required="" onkeyup="Kontrak.search(this, event)">
    <button type="submit" class="btn btn-icon"><i class="ik ik-search" onclick="Kontrak.searchByClick(this)"></i></button>
   </div>
  </div>

  <?php if (isset($keyword)) { ?>
   <br/>
   <br/>
   <br/>
   <br/>
   <div class="col-sm-12">
    Cari : <label class="bold" style="font-size: 14px;"><b><?php echo $keyword ?></b></label>
   </div>
  <?php } ?> 
 </div>
 <div class="card-body">
  <div class="table-responsive">
   <table class="table table-bordered table-striped" id="tb_content">
    <thead>
     <tr>
      <th class="text-center">No</th>
      <th>Upt</th>
      <th>No Kontrak</th>
      <th>Tanggal Kontrak</th>
      <th>Tanggal Selesai Kontrak</th>
      <th class="text-center">Status</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = $pagination['last_no'] + 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr class="">
        <td class="text-center"><?php echo $no++ ?></td>
        <td><?php echo $value['nama_upt'] ?></td>
        <td><b><?php echo $value['no_kontrak'] ?></b></td>
        <td><?php echo date('d M Y', strtotime($value['tanggal_kontrak'])) ?></td>
        <td><?php echo date('d M Y', strtotime($value['tanggal_selesai_kontrak'])) ?></td>
        <?php $btn_color = $value['status'] == 'DRAFT' ? 'text-warning' : 'text-success' ?>
        <td class="<?php echo $btn_color ?> text-center">
         <?php echo $value['status'] == 'DRAFT' ? 'ON PROGRESS' : $value['status'] ?>
        </td>
        <td class="text-center">
         <div class="list-actions">
          <a href="#" data-toggle="tooltip" title="Detail Kontrak" onclick="Kontrak.detail('<?php echo $value['id'] ?>')"><i class="ik ik-eye"></i></a>
          <?php if ($value['status'] == 'DRAFT' && ($akses == "superadmin" || $akses == "admin")) { ?>
           <a href="#" data-toggle="tooltip" title="Ubah Kontrak" onclick="Kontrak.ubah('<?php echo $value['id'] ?>')"><i class="ik ik-edit-2"></i></a>	    
          <?php } ?>     	                  
          <?php if (($akses == "superadmin" || $akses == "admin")) { ?>
           <a href="#" data-toggle="tooltip" title="Hapus Kontrak" class="list-delete" onclick="Kontrak.delete('<?php echo $value['id'] ?>')"><i class="ik ik-trash-2"></i></a>
          <?php } ?>	   
          <a href="#" class="list-file" data-toggle="tooltip" title="Daftar Progress Kontrak" onclick="Kontrak.showProgressKontrak('<?php echo $value['id'] ?>', event)"><i class="ik ik-activity"></i></a>
          <a href="#" data-toggle="tooltip" title="Daftar Dokumen Kontrak" class="list-file" onclick="Kontrak.showDokumen('<?php echo $value['id'] ?>')"><i class="ik ik-file"></i></a>
        </td>                   
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?> 
    </tbody>
   </table>
  </div>

  <div class="text-right">
   <nav aria-label="Page navigation example">
    <ul class="pagination mb-0 pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </nav>
  </div>
 </div>
</div>
