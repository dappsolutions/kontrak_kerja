<!DOCTYPE html>
<html>

<head>
	<title>Selamat Datang!</title>
	<style>
		body {
			text-align: center;
			padding: 30px;
		}

		h1 {
			font-size: 50px;
		}

		body {
			font: 20px Helvetica, sans-serif;
			color: #333;
		}

		article {
			display: block;
			text-align: left;
			width: 650px;
			margin: 0 auto;
		}

		a {
			color: #dc8100;
			text-decoration: none;
		}

		a:hover {
			color: #333;
			text-decoration: none;
		}
	</style>
</head>

<body>
	<article>
		<h1>Selamat Datang!</h1>
		<div>
			<p>Sistem Kontrak Kerja&rsquo;</p>
			<p>&mdash; <a href="#">Reminder Vendor Via Email</a></p>
			<center><img src="<?php echo base_url() . 'assets/images/' ?>maintenance.gif" style="width:50%"></center>
		</div>
		<!--Div where the WhatsApp will be rendered-->
		<div id="WAButton"></div>
	</article>
</body>

</html>
<script>window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/themekit/src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/message.js"></script>
<script src="<?php echo base_url() ?>assets/js/controllers/reminder.js"></script>
