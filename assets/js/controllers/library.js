var Library = {
 module: function () {
  return 'library';
 },

 add: function () {
  window.location.href = url.base_url(Library.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(Library.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Library.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Library.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Library.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'form': {
    'kontrak': $('select#no_kontrak').val(),
    'nama': $('#nama').val(),
    'file_str': $('#file_str').val()
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Library.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Library.module()) + "simpan",
    error: function () {
     showDangerToast("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Library.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      showDangerToast("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Library.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Library.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Library.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Library.module()) + "delete/" + id,

   error: function () {
    showDangerToast("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Library.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     showDangerToast("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Library.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Library.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Library.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {  
  Library.checkFile(elm);
 },

 checkFile: function (elm) {
//  console.log("masukkk");
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   
   type_file = type_file.toLowerCase();
   if (type_file == 'pdf' || type_file == 'png' || type_file == 'jpg' 
           || type_file == 'jpeg') {
    if (data_file.size <= 13432846) {
     $(elm).closest('div').find('input.file-upload-info').val($(elm).val());
    } else {
     showDangerToast('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {    
    showDangerToast('File Harus Berformat Pdf, Png, Jpg, Jpeg');
//    showDangerToast('File Harus Berformat Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   showDangerToast('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $('input#file_str').val()
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Library.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },
 
 showLogoFormTable: function (elm, e) {
  e.preventDefault();
  var file_str = $(elm).attr('file');
  $.ajax({
   type: 'POST',
   data: {
    foto: file_str
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Library.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Library.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Library.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Library.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': false,
   'lengthChange': true,
   'searching': false,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },
 
 removeUpload: function(elm){
  $('#btn-upload').removeClass('hidden-content');
  $('#btn-remove').addClass('hidden-content');
  $('input#file_str').val('');
 },
 
 setSelect2: function () {
  $('select#no_kontrak').select2();
 },
};

$(function () {
// Library.setDate();
 Library.setDataTable();
 Library.setSelect2();
});
