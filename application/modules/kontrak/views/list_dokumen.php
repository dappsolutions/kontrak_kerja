<div class="row">
 <div class="col-md-12">
  <h5><u>Daftar Dokumen Kontrak</u></h5>
 </div>
</div>
<hr/>
<br>
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_content">
    <thead>
     <tr>
      <th class="text-center">No</th>
      <th>Dokumen</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($content)) { ?>
      <?php $no = 1; ?>
      <?php foreach ($content as $value) { ?>
       <tr>
        <td class="text-center"><?php echo $no++ ?></td>
        <td><?php echo $value['nama'] ?></td>
        <td class="text-center">
         <a href="#" file="<?php echo $value['file'] ?>" onclick="Kontrak.showDokumenData(this,'<?php echo $value['id'] ?>')"><i class="ik ik-file"></i></a>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="4" class="text-center">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?> 
    </tbody>
   </table>
  </div>
 </div>
</div>