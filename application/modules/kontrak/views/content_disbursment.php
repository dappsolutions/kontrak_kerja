<td>
 <input type="text" value="" placeholder="Termin Ke" id="termin_ke" class="form-control" />
</td>
<td>
 <input type="text" value="" placeholder="Fisik (%)" id="fisik" class="form-control" />
</td>
<td>
 <input type="text" value="" placeholder="Bayar (%)" id="bayar" class="form-control" />
</td>
<td>
 <input type="text" value="" placeholder="Tanggal" id="tanggal_disbursment_<?php echo $index ?>" class="form-control" />
</td>
<td>
 <input type="text" value="" placeholder="Jumlah" id="jumlah_bayar_plan_<?php echo $index ?>" class="form-control" />
</td>
<td class="text-center">
 <i class="ik ik-trash ik-2x" onclick="Kontrak.removeItem(this)"></i>
</td>


<script>
 $(function () {
  $('#tanggal_disbursment_<?php echo $index ?>').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
   orientation: 'bottom left'
  });


  $('#jumlah_bayar_plan_<?php echo $index ?>').divide({
   delimiter: '.',
   divideThousand: true
  });
 });
</script>