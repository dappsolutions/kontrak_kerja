<?php

class Usera extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'usera';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/usera.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'user';
 }

 public function getRootModule() {
  return "User";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Admin";
  $data['title_content'] = 'Admin';
  $data['root_module'] = $this->getRootModule();
  $content = $this->getDataUsera();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataUsera($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('t.password', $keyword),
       array('p.nip', $keyword),
       array('p.nama_pegawai', $keyword),
       array('p.posisi', $keyword),
       array('u.nama_upt', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
	$where = "t.deleted = 0 and t.priveledge = '2'";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'p.nip', 
                    'p.nama_pegawai', 'p.posisi', 'u.nama_upt'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id'),
                    array('upt u', 'p.upt = u.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'p.nip', 
                    'p.nama_pegawai', 'p.posisi', 'u.nama_upt'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id'),
                    array('upt u', 'p.upt = u.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataUsera($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('t.password', $keyword),
       array('p.nip', $keyword),
       array('p.nama_pegawai', $keyword),
       array('p.posisi', $keyword),
       array('u.nama_upt', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0 and t.priveledge = '2'";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip',
                    'p.nama_pegawai', 'p.posisi', 'u.nama_upt'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id'),
                    array('upt u', 'p.upt = u.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'p.nip',
                    'p.nama_pegawai', 'p.posisi', 'u.nama_upt'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id'),
                    array('upt u', 'p.upt = u.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataUsera($keyword)
  );
 }

 public function getDetailDataUsera($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListAdmin() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'where' => "p.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['root_module'] = 'User ';
  $data['title'] = "Tambah User Admin";
  $data['title_content'] = 'Tambah User Admin';
  $data['list_pegawai'] = $this->getListAdmin();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataUsera($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah User Admin";
  $data['root_module'] = 'User ';
  $data['title_content'] = 'Ubah User Admin';
  $data['list_pegawai'] = $this->getListAdmin();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataUsera($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail User Admin";
  $data['root_module'] = 'User';
  $data['title_content'] = "Detail User Admin";
  $data['list_pegawai'] = $this->getListAdmin();
  echo Modules::run('template', $data);
 }

 public function getPostDataUsera($value) {
  $data['pegawai'] = $value->pegawai;
  $data['username'] = $value->username;
  $data['password'] = $value->password;
  $data['priveledge'] = 2;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataUsera($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Usera";
  $data['title_content'] = 'Data Usera';
  $content = $this->getDataUsera($keyword);
  $data['root_module'] = $this->getRootModule();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
