-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 05:49 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kontrak`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokumen`
--

CREATE TABLE `dokumen` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `file` text,
  `tipe` varchar(50) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen`
--

INSERT INTO `dokumen` (`id`, `kontrak`, `nama`, `file`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Dokumen A', 'blank.png', 'png', '2019-11-12', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `url` text,
  `description` varchar(255) DEFAULT NULL,
  `visible` int(11) DEFAULT '1',
  `icon` varchar(255) DEFAULT NULL,
  `row` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feature_set`
--

CREATE TABLE `feature_set` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pekerjaan`
--

CREATE TABLE `jenis_pekerjaan` (
  `id` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pekerjaan`
--

INSERT INTO `jenis_pekerjaan` (`id`, `jenis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', '2019-11-02', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak`
--

CREATE TABLE `kontrak` (
  `id` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `pekerjaan` text,
  `jenis_pekerjaan` int(11) NOT NULL,
  `no_kontrak` varchar(150) DEFAULT NULL,
  `leader` varchar(255) DEFAULT NULL,
  `tanggal_kontrak` date DEFAULT NULL,
  `tanggal_selesai_kontrak` date DEFAULT NULL,
  `reminder_date` date DEFAULT NULL,
  `harga_kontrak` int(11) DEFAULT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak`
--

INSERT INTO `kontrak` (`id`, `vendor`, `upt`, `pekerjaan`, `jenis_pekerjaan`, `no_kontrak`, `leader`, `tanggal_kontrak`, `tanggal_selesai_kontrak`, `reminder_date`, `harga_kontrak`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 1, 1, 'tes', 1, '12/2019/122', 'Dodik Rismawan Affrudin', '2019-11-02', '2019-11-22', '2019-11-06', 15000000, 'Kontrak Aplikasi', '2019-11-02', 1, '2019-11-20 06:51:45', 1, 0),
(3, 1, 1, 'Install Aplikasi', 1, 'KON/201', 'Dodik Rismawan Affrudin', '2019-11-19', '2019-11-30', '2019-11-22', 12000000, NULL, '2019-11-19', 1, '2019-11-27 09:37:35', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_amandemen`
--

CREATE TABLE `kontrak_amandemen` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `no_amandemen` varchar(155) DEFAULT NULL,
  `uraian` text,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_amandemen`
--

INSERT INTO `kontrak_amandemen` (`id`, `progress`, `no_amandemen`, `uraian`, `tanggal_mulai`, `tanggal_selesai`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '2019/AM', 'tesokk', '2019-11-02', '2019-11-04', '2019-11-02', 1, '2019-11-27 04:11:13', 1, 0),
(2, 2, '2019/AM/001', 'tes', '2019-11-04', '2019-11-06', '2019-11-02', 1, NULL, NULL, 0),
(3, 3, '2019/wewe', '-', '2019-11-12', '2019-11-05', '2019-11-12', 1, NULL, NULL, 0),
(4, 4, 'AMANKON19NOV001', '-', '2019-11-12', '2019-11-12', '2019-11-12', 1, NULL, NULL, 0),
(5, 5, 'AMANKON19NOV001', '-', '2019-11-12', '2019-11-12', '2019-11-12', 1, NULL, NULL, 0),
(6, 6, 'AMANKON19NOV002', '-', '2019-11-12', '2019-11-27', '2019-11-12', 1, NULL, NULL, 0),
(7, 7, 'AMANKON19NOV003', '-', '2019-11-12', '2019-11-21', '2019-11-12', 1, NULL, NULL, 0),
(8, 8, 'AMANKON19NOV004', 'oke', '2019-11-12', '2019-11-19', '2019-11-12', 1, NULL, NULL, 0),
(11, 11, 'AMANKON19NOV005', '-', NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(12, 12, 'AMANKON19NOV006', '-', NULL, NULL, '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_bast1`
--

CREATE TABLE `kontrak_bast1` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_bast1`
--

INSERT INTO `kontrak_bast1` (`id`, `progress`, `nomor`, `tanggal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '2', '2019-11-02', '2019-11-02', 1, '2019-11-27 04:11:13', 1, 0),
(2, 2, '01', '2019-11-07', '2019-11-02', 1, NULL, NULL, 0),
(3, 3, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(4, 4, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(5, 5, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(6, 6, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(7, 7, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(8, 8, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(9, 11, '0', NULL, '2019-11-27', 1, NULL, NULL, 0),
(10, 12, '0', NULL, '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_bast2`
--

CREATE TABLE `kontrak_bast2` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_bast2`
--

INSERT INTO `kontrak_bast2` (`id`, `progress`, `nomor`, `tanggal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '1', '2019-11-03', '2019-11-02', 1, '2019-11-27 04:11:13', 1, 0),
(2, 2, '02', '2019-11-07', '2019-11-02', 1, NULL, NULL, 0),
(3, 3, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(4, 4, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(5, 5, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(6, 6, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(7, 7, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(8, 8, '0', NULL, '2019-11-12', 1, NULL, NULL, 0),
(9, 11, '0', NULL, '2019-11-27', 1, NULL, NULL, 0),
(10, 12, '0', NULL, '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_bayar`
--

CREATE TABLE `kontrak_bayar` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `kontrak_plan` int(11) DEFAULT NULL,
  `tanggal_ba` date DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `persentase_bayar` int(11) DEFAULT NULL,
  `denda` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_bayar`
--

INSERT INTO `kontrak_bayar` (`id`, `progress`, `kontrak_plan`, `tanggal_ba`, `nilai`, `persentase_bayar`, `denda`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 11, 1, NULL, 30, 20, 3000000, '2019-11-27', 1, NULL, NULL, 0),
(2, 12, 2, '2019-11-28', 3000000, 20, 0, '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_cod`
--

CREATE TABLE `kontrak_cod` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `target` date DEFAULT NULL,
  `realisasi` date DEFAULT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_cod`
--

INSERT INTO `kontrak_cod` (`id`, `progress`, `target`, `realisasi`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, '2019-11-02', '2019-11-03', NULL, '2019-11-02', 1, '2019-11-27 04:11:13', 1, NULL),
(2, 2, '2019-11-05', '2019-11-06', NULL, '2019-11-02', 1, NULL, NULL, NULL),
(3, 3, '2019-11-12', '2019-11-21', NULL, '2019-11-12', 1, NULL, NULL, NULL),
(4, 4, NULL, NULL, NULL, '2019-11-12', 1, NULL, NULL, NULL),
(5, 5, NULL, NULL, NULL, '2019-11-12', 1, NULL, NULL, NULL),
(6, 6, NULL, NULL, NULL, '2019-11-12', 1, NULL, NULL, NULL),
(7, 7, NULL, NULL, NULL, '2019-11-12', 1, NULL, NULL, NULL),
(8, 8, NULL, NULL, 'siap', '2019-11-12', 1, NULL, NULL, NULL),
(11, 11, NULL, NULL, NULL, '2019-11-27', 1, NULL, NULL, NULL),
(12, 12, NULL, NULL, NULL, '2019-11-29', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_dirlap`
--

CREATE TABLE `kontrak_dirlap` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `dirlap` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_dirlap`
--

INSERT INTO `kontrak_dirlap` (`id`, `kontrak`, `dirlap`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, 'Deni', '2019-11-27', 1, '2019-11-27 09:31:22', 1, 0),
(2, 3, 'Dono', '2019-11-27', 1, '2019-11-27 09:31:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_forecast`
--

CREATE TABLE `kontrak_forecast` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `tanggal_rencana_bayar` date DEFAULT NULL,
  `tanggal_berita_acara` date DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_kendala`
--

CREATE TABLE `kontrak_kendala` (
  `id` int(11) NOT NULL,
  `kendala` varchar(255) DEFAULT NULL,
  `kontrak` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_kendala`
--

INSERT INTO `kontrak_kendala` (`id`, `kendala`, `kontrak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'KENDALA ', 3, '2019-11-27', 1, '2019-11-27 09:31:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_korlap`
--

CREATE TABLE `kontrak_korlap` (
  `id` int(11) NOT NULL,
  `korlap` varchar(255) DEFAULT NULL,
  `kontrak` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_korlap`
--

INSERT INTO `kontrak_korlap` (`id`, `korlap`, `kontrak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Tedi', 3, '2019-11-27', 1, '2019-11-27 09:31:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_lokasi`
--

CREATE TABLE `kontrak_lokasi` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_lokasi`
--

INSERT INTO `kontrak_lokasi` (`id`, `kontrak`, `lokasi`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 3, '', '2019-11-27', 1, '2019-11-27 09:31:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_plan`
--

CREATE TABLE `kontrak_plan` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `jumlah_termin` int(11) DEFAULT NULL,
  `fisik` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_plan`
--

INSERT INTO `kontrak_plan` (`id`, `kontrak`, `jumlah_termin`, `fisik`, `bayar`, `tanggal`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, 1, 20, 10, '2019-11-27', 2000000, '2019-11-27', 1, '2019-11-27 09:37:35', 1, 0),
(2, 3, 2, 20, 10, '2019-11-30', 3000000, '2019-11-27', 1, '2019-11-27 09:37:35', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_progress_fisik`
--

CREATE TABLE `kontrak_progress_fisik` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `kurva_before` int(11) DEFAULT NULL,
  `realisasi_before` int(11) DEFAULT NULL,
  `kurva_after` int(11) DEFAULT NULL,
  `realisasi_after` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_progress_fisik`
--

INSERT INTO `kontrak_progress_fisik` (`id`, `progress`, `kurva_before`, `realisasi_before`, `kurva_after`, `realisasi_after`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 0, 0, 0, 0, '2019-11-02', 1, '2019-11-27 04:11:13', 1, 0),
(2, 2, 1, 1, 1, 20, '2019-11-02', 1, NULL, NULL, 0),
(3, 3, 0, 0, 0, 0, '2019-11-12', 1, NULL, NULL, 0),
(4, 4, 0, 0, 0, 0, '2019-11-12', 1, NULL, NULL, 0),
(5, 5, 0, 0, 0, 0, '2019-11-12', 1, NULL, NULL, 0),
(6, 6, 0, 0, 0, 0, '2019-11-12', 1, NULL, NULL, 0),
(7, 7, 0, 0, 0, 0, '2019-11-12', 1, NULL, NULL, 0),
(8, 8, 0, 0, 0, 12, '2019-11-12', 1, NULL, NULL, 0),
(11, 11, 0, 0, 0, 0, '2019-11-27', 1, NULL, NULL, 0),
(12, 12, 0, 0, 0, 0, '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_status`
--

CREATE TABLE `kontrak_status` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'CLOSE\nON PROGRESS\nDRAFT',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_status`
--

INSERT INTO `kontrak_status` (`id`, `kontrak`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(2, 2, 'DRAFT', '2019-11-02', 1, NULL, NULL),
(3, 3, 'DRAFT', '2019-11-19', 1, NULL, NULL),
(4, 2, 'DONE', '2019-11-27', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `nama_pegawai` varchar(150) DEFAULT NULL,
  `nip` varchar(150) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `upt`, `nama_pegawai`, `nip`, `email`, `no_hp`, `posisi`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'dodik', '1318033', 'dodikitn@gmail.com', '6285738233712', 'Staff', '2019-11-02', 1, NULL, NULL, 0),
(2, 1, 'beni', '1318032', 'beni@gmail.com', '085748233712', 'Staff', '2019-11-12', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'pegawai'),
(4, 'vendor');

-- --------------------------------------------------------

--
-- Table structure for table `probis`
--

CREATE TABLE `probis` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `probis`
--

INSERT INTO `probis` (`id`, `nama`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Probis Kontrak', 'blank.png', '2019-11-12', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `progress_kontrak` int(11) NOT NULL,
  `no_progress` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress`
--

INSERT INTO `progress` (`id`, `progress_kontrak`, `no_progress`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'PROG19NOV001', '2019-11-02', 1, NULL, NULL, 0),
(2, 1, 'PROG19NOV002', '2019-11-02', 1, NULL, NULL, 0),
(3, 1, 'PROG19NOV003', '2019-11-12', 1, NULL, NULL, 0),
(4, 1, 'PROG19NOV004', '2019-11-12', 1, NULL, NULL, 0),
(5, 1, 'PROG19NOV005', '2019-11-12', 1, NULL, NULL, 0),
(6, 1, 'PROG19NOV006', '2019-11-12', 1, NULL, NULL, 0),
(7, 1, 'PROG19NOV007', '2019-11-12', 1, NULL, NULL, 0),
(8, 1, 'PROG19NOV008', '2019-11-12', 1, '2019-11-18 05:38:06', 1, 1),
(11, 2, 'PROG19NOV009', '2019-11-27', 1, NULL, NULL, 0),
(12, 2, 'PROG19NOV010', '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `progress_kontrak`
--

CREATE TABLE `progress_kontrak` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `diprek` varchar(255) DEFAULT NULL,
  `dirlap` varchar(255) DEFAULT NULL,
  `korlap` varchar(255) DEFAULT NULL,
  `kendala` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress_kontrak`
--

INSERT INTO `progress_kontrak` (`id`, `kontrak`, `lokasi`, `diprek`, `dirlap`, `korlap`, `kendala`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Paiton', 'DIPREK 1', 'DIRLAP 1', 'KORLAP 1', 'KENDALA ', 'Tes di lokasi unit induk', '2019-11-02', 1, '2019-11-20 06:51:45', 1, 0),
(2, 3, 'Kantor Induk', '-', '-', '-', '-', NULL, '2019-11-19', 1, '2019-11-27 09:37:35', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `surat_reminder`
--

CREATE TABLE `surat_reminder` (
  `id` int(11) NOT NULL,
  `kontrak` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `file` text,
  `tipe` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_reminder`
--

INSERT INTO `surat_reminder` (`id`, `kontrak`, `tanggal_kirim`, `file`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, '2019-11-29', '3505090512940001_kartuDaftar.pdf', 'pdf', '2019-11-29', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `upt`
--

CREATE TABLE `upt` (
  `id` int(11) NOT NULL,
  `nama_upt` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upt`
--

INSERT INTO `upt` (`id`, `nama_upt`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UPT Surabaya', '2019-11-02', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `pegawai` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `pegawai`, `vendor`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'admin', 'admin', NULL, NULL, 1, NULL, NULL, NULL, 0, 0),
(2, 'dodik', 'sasuke', 1, NULL, 3, '2019-11-02', 1, NULL, NULL, 0),
(3, 'beni', '1234', 2, NULL, 2, '2019-11-12', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(255) DEFAULT NULL,
  `alamat` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `nama_vendor`, `alamat`, `no_hp`, `email`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Dappsolutions', 'Surabaya', '6285738233712', 'dodikitn@gmail.com', '2019-11-02', 1, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_dokumen_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_set`
--
ALTER TABLE `feature_set`
  ADD PRIMARY KEY (`id`,`user`,`feature`),
  ADD KEY `fk_feature_set_user_idx` (`user`),
  ADD KEY `fk_feature_set_feature1_idx` (`feature`);

--
-- Indexes for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontrak`
--
ALTER TABLE `kontrak`
  ADD PRIMARY KEY (`id`,`vendor`,`upt`,`jenis_pekerjaan`),
  ADD KEY `fk_kontrak_vendor1_idx` (`vendor`),
  ADD KEY `fk_kontrak_upt1_idx` (`upt`),
  ADD KEY `fk_kontrak_jenis_pekerjaan1_idx` (`jenis_pekerjaan`);

--
-- Indexes for table `kontrak_amandemen`
--
ALTER TABLE `kontrak_amandemen`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_amandemen_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_bast1`
--
ALTER TABLE `kontrak_bast1`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_bast1_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_bast2`
--
ALTER TABLE `kontrak_bast2`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_bast2_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_bayar`
--
ALTER TABLE `kontrak_bayar`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_bayar_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_cod`
--
ALTER TABLE `kontrak_cod`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_cod_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_dirlap`
--
ALTER TABLE `kontrak_dirlap`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_dirlap_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_forecast`
--
ALTER TABLE `kontrak_forecast`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_forecast_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_kendala`
--
ALTER TABLE `kontrak_kendala`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_kendala_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_korlap`
--
ALTER TABLE `kontrak_korlap`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_korlap_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_lokasi`
--
ALTER TABLE `kontrak_lokasi`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_lokasi_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_plan`
--
ALTER TABLE `kontrak_plan`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_plan_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `kontrak_progress_fisik`
--
ALTER TABLE `kontrak_progress_fisik`
  ADD PRIMARY KEY (`id`,`progress`),
  ADD KEY `fk_kontrak_progress_fisik_progress1_idx` (`progress`);

--
-- Indexes for table `kontrak_status`
--
ALTER TABLE `kontrak_status`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_kontrak_status_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`,`upt`),
  ADD KEY `fk_pegawai_upt1_idx` (`upt`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `probis`
--
ALTER TABLE `probis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`,`progress_kontrak`),
  ADD KEY `fk_progress_progress_kontrak1_idx` (`progress_kontrak`);

--
-- Indexes for table `progress_kontrak`
--
ALTER TABLE `progress_kontrak`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_progress_kontrak_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `surat_reminder`
--
ALTER TABLE `surat_reminder`
  ADD PRIMARY KEY (`id`,`kontrak`),
  ADD KEY `fk_surat_reminder_kontrak1_idx` (`kontrak`);

--
-- Indexes for table `upt`
--
ALTER TABLE `upt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge1_idx` (`priveledge`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature_set`
--
ALTER TABLE `feature_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontrak`
--
ALTER TABLE `kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontrak_amandemen`
--
ALTER TABLE `kontrak_amandemen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kontrak_bast1`
--
ALTER TABLE `kontrak_bast1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kontrak_bast2`
--
ALTER TABLE `kontrak_bast2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kontrak_bayar`
--
ALTER TABLE `kontrak_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kontrak_cod`
--
ALTER TABLE `kontrak_cod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kontrak_dirlap`
--
ALTER TABLE `kontrak_dirlap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kontrak_forecast`
--
ALTER TABLE `kontrak_forecast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kontrak_kendala`
--
ALTER TABLE `kontrak_kendala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontrak_korlap`
--
ALTER TABLE `kontrak_korlap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kontrak_lokasi`
--
ALTER TABLE `kontrak_lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontrak_plan`
--
ALTER TABLE `kontrak_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kontrak_progress_fisik`
--
ALTER TABLE `kontrak_progress_fisik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kontrak_status`
--
ALTER TABLE `kontrak_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `probis`
--
ALTER TABLE `probis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `progress_kontrak`
--
ALTER TABLE `progress_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `surat_reminder`
--
ALTER TABLE `surat_reminder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `upt`
--
ALTER TABLE `upt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
