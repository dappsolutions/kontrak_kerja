<div class="table-responsive">
 <table class="table table-bordered" id="tb_dirlap">
  <thead>
   <tr>
    <th>Dirlap</th>
   </tr>
  </thead>
  <tbody>
   <?php if (isset($list_dirlap)) { ?>
    <?php foreach ($list_dirlap as $value) { ?>
     <tr class="" data_id="<?php echo $value['id'] ?>">
      <td>
       <input disabled type="text" value="<?php echo $value['dirlap'] ?>" placeholder="Dirlap" id="dirlap" class="form-control" />
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  </tbody>
 </table>
</div>