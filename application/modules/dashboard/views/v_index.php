<?php echo $this->load->view('probis_view'); ?>

<div class="row">
	<div class="col-md-9">
		<div class="card">
			<div class="card-header row">
				<div class="col col-sm-6">
					<h5>Grafik Realisasi Kontrak Kerja</h5>
				</div>
			</div>
			<div class="card-body">
				<div class="text-right">
					<!--<a href="#" class="btn btn-success" download="<?php echo 'Kontrak' ?>.xls" onclick="return ExcellentExport.excel(this, 'tb_kontrak', 'Kontrak');"><i class="ik ik-file-text"></i> &nbsp;Export</a>-->
				</div>
				<br />
    
    <label class='hidden' id="max_bar"><?php echo $all_persentase_kontrak['max']; ?></label>
    <label class='hidden' id="0_30_bar"><?php echo $all_persentase_kontrak['0_30']; ?></label>
    <label class='hidden' id="31_50_bar"><?php echo $all_persentase_kontrak['31_50']; ?></label>
    <label class='hidden' id="51_70_bar"><?php echo $all_persentase_kontrak['51_70']; ?></label>
    <label class='hidden' id="71_100_bar"><?php echo $all_persentase_kontrak['71_100']; ?></label>
    <div id="bar-chart" style="max-width: none;height: 300px;width: 600px;">
     
    </div>
			</div>
		</div>
	</div>	
 <?php echo $this->load->view('right_dashboard'); ?>
</div>


<input type="hidden" value="<?php echo $persentase_kontrak ?>" id="persentase_kontrak" class="form-control" />